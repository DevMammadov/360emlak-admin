import { AppBar, CssBaseline, IconButton, Toolbar, Typography, withWidth } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import { Aside } from "components/layout";
import { Header } from "components/layout/header";
import React, { FC } from "react";
import AppRouter from "routes/app-router";
import { useStyles } from "./app-layout.style";

interface IApp {
  width: any;
}

const App: FC<IApp> = ({ width }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header open={open} onClose={() => setOpen(!open)} />
      <Aside open={open} onClose={() => setOpen(!open)} />
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        <AppRouter />
      </main>
    </div>
  );
};

export default withWidth()(App);
