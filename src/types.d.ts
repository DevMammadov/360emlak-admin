export interface ILimitOfs {
  limit: number;
  offset: number;
}
