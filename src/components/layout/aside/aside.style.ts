import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

export const useStyles = makeStyles((theme) => {
  return {
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      zIndex: 999,
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: "flex-end",
    },
    menu: {
      "& .MuiListItemIcon-root": {
        minWidth: 40,
      },
    },
    collapsedItem: {
      background: theme.palette.action.hover,
    },
    activeItem: {
      background: `${theme.palette.color.main} !important`,
      color: theme.palette.color.white,
      "& .MuiListItemIcon-root": {
        color: theme.palette.color.white,
      },
      "&:hover": {
        background: theme.palette.primary.light,
      },
    },
    subItem: {
      background: theme.palette.action.hover,
      padding: theme.spacing(0.5, 1, 1, 2),
      "& .MuiListItemIcon-root": {
        minWidth: 30,
        "& svg": {
          fontSize: 20,
        },
      },
    },
    arrow: {
      color: theme.palette.color.main,
      transition: "all ease .3s",
    },
    collapsedArrow: {
      transform: "rotate(180deg)",
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
