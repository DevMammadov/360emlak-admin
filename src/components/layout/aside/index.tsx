import { Collapse, Divider, Drawer, IconButton, List, ListItem, ListItemText } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import React, { FC, Fragment, useState } from "react";
import { useHistory, useLocation } from "react-router";
import { useStyles } from "./aside.style";
import DashboardIcon from "@material-ui/icons/Dashboard";
import { useTranslator } from "localization";
import ApartmentIcon from "@material-ui/icons/Apartment";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import HomeIcon from "@material-ui/icons/Home";
import InfoIcon from "@material-ui/icons/Info";
import WallpaperIcon from "@material-ui/icons/Wallpaper";
import PageviewIcon from "@material-ui/icons/Pageview";
import GavelIcon from "@material-ui/icons/Gavel";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import NoteAddIcon from "@material-ui/icons/NoteAdd";
import AssignmentIcon from "@material-ui/icons/Assignment";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import PhotoLibraryIcon from "@material-ui/icons/PhotoLibrary";
import clsx from "clsx";

interface ISubMenu {
  title: string;
  icon: any;
  link: string;
}

interface IMenu {
  title: string;
  icon: any;
  link: string;
  menu?: ISubMenu[];
}

export interface IAside {
  open: boolean;
  onClose(): void;
}

export const Aside: FC<IAside> = ({ open, onClose }) => {
  const classes = useStyles();
  const history = useHistory();
  const lang = useTranslator("menu");
  const [collapsedItems, setCollapsedItem] = useState<string[]>([]);
  const location = useLocation();

  const menu: IMenu[] = [
    {
      title: lang.dashboard,
      icon: <DashboardIcon />,
      link: "/",
    },
    {
      title: lang.announces,
      icon: <HomeIcon />,
      link: "/announces",
    },
    {
      title: lang.agencies,
      icon: <BusinessCenterIcon />,
      link: "/agencies",
    },
    {
      title: lang.complexes,
      icon: <ApartmentIcon />,
      link: "/complexes",
    },
    {
      title: lang.users,
      icon: <PeopleAltIcon />,
      link: "/users",
    },
    {
      title: lang.homeSearchers,
      icon: <PageviewIcon />,
      link: "/home-requests",
    },
    {
      title: lang.info,
      icon: <InfoIcon />,
      link: "/information",
    },
    {
      title: lang.laws,
      icon: <GavelIcon />,
      link: "/laws",
      menu: [
        {
          title: lang.increaseBalance,
          icon: <AttachMoneyIcon />,
          link: "/laws/increase-balance",
        },
        {
          title: lang.announceAdding,
          icon: <NoteAddIcon />,
          link: "/laws/announce-law",
        },
        {
          title: lang.addAdding,
          icon: <LibraryAddIcon />,
          link: "/laws/advertisment-law",
        },
        {
          title: lang.userAgreement,
          icon: <AssignmentIcon />,
          link: "/laws/user-agreement",
        },
      ],
    },
    {
      title: lang.addBanner,
      icon: <WallpaperIcon />,
      link: "/baners",
    },
    {
      title: lang.album,
      icon: <PhotoLibraryIcon />,
      link: "/album",
    },
  ];

  const handleCollapse = (title: string) => {
    if (collapsedItems.includes(title)) {
      setCollapsedItem(collapsedItems.filter((c) => c !== title));
    } else {
      setCollapsedItem([...collapsedItems, title]);
    }
  };

  const iActive = (link: string) => {
    if (link.length > 1) {
      return location.pathname.startsWith(link);
    } else {
      return location.pathname === "/";
    }
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={onClose}>{<ChevronLeftIcon />}</IconButton>
      </div>
      <Divider />
      <List className={classes.menu}>
        {menu.map((item, index) => (
          <Fragment key={item.title}>
            <ListItem
              button
              className={clsx(
                collapsedItems.includes(item.title) && classes.collapsedItem,
                iActive(item.link) && classes.activeItem
              )}
              onClick={() => (item.menu ? handleCollapse(item.title) : history.push(item.link))}
            >
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.title} />
              {item.menu && (
                <KeyboardArrowDownIcon
                  className={clsx(classes.arrow, collapsedItems.includes(item.title) && classes.collapsedArrow)}
                />
              )}
            </ListItem>
            {item.menu && (
              <Collapse in={collapsedItems.includes(item.title)}>
                {item.menu?.map((subItem, subIndex) => (
                  <ListItem
                    button
                    key={subItem.title}
                    className={clsx(iActive(subItem.link) && classes.activeItem, classes.subItem)}
                    onClick={() => history.push(subItem.link)}
                  >
                    <ListItemIcon>{subItem.icon}</ListItemIcon>
                    <ListItemText primary={subItem.title} />
                  </ListItem>
                ))}
              </Collapse>
            )}
          </Fragment>
        ))}
      </List>
    </Drawer>
  );
};
