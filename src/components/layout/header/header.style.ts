import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

export const useStyles = makeStyles((theme) => {
  return {
    appBar: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: "none",
    },
    toolbar: {
      display: "flex",
      justifyContent: "flex-start",
    },
    header: {
      display: "flex",
      justifyContent: "space-between",
      width: "100%",
    },
    avatar: {
      position: "relative",
      display: "flex",
      alignItems: "center",
      "& .MuiAvatar-root": {
        marginRight: theme.spacing(1),
        background: theme.palette.success.main,
      },
      "& span": {
        fontSize: 17,
      },
    },
    menuPaper: {
      width: 200,
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
