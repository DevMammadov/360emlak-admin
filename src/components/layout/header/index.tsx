import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./header.style";
import { useTranslator } from "localization";
import MenuIcon from "@material-ui/icons/Menu";
import {
  AppBar,
  Avatar,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";
import clsx from "clsx";
import { useUser } from "hooks/useUser";
import UserApi from "api/users.api";
import { IUserBasic } from "views/users/types";
import { Button } from "components/shared";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { removeStorage } from "helpers/storage";

export interface IHeader {
  open: boolean;
  onClose(): void;
}

export const Header: FC<IHeader> = ({ open, onClose }) => {
  const lang = useTranslator();
  const classes = useStyles();
  const currentUser = useUser();
  const [user, setUser] = useState<IUserBasic>({} as IUserBasic);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  useEffect(() => {
    if (currentUser.userId) {
      UserApi.getUseById(currentUser.userId).then((payload) => setUser(payload?.data));
    }
  }, []);

  const handleLogout = () => {
    removeStorage();
    window.location.reload();
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: open,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={onClose}
          edge="start"
          className={clsx(classes.menuButton, open && classes.hide)}
        >
          <MenuIcon />
        </IconButton>
        <div className={classes.header}>
          <div></div>
          <Button
            className={classes.avatar}
            variant="text"
            text="white"
            aria-controls="customized-menu"
            aria-haspopup="true"
            color="primary"
            onClick={(e) => setAnchorEl(e.currentTarget)}
          >
            <Avatar> {user?.fullName && user.fullName[0].toUpperCase()} </Avatar>
            <span> {user?.fullName} </span>
          </Button>
          <Menu
            elevation={0}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={() => setAnchorEl(null)}
            classes={{ paper: classes.menuPaper }}
          >
            <MenuItem onClick={() => handleLogout()}>
              <ListItemIcon>
                <ExitToAppIcon fontSize="small" />
              </ListItemIcon>
              <ListItemText primary="Çıxış" />
            </MenuItem>
          </Menu>
        </div>
      </Toolbar>
    </AppBar>
  );
};
