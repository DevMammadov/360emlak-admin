import { SvgIconTypeMap, useTheme } from "@material-ui/core";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import clsx from "clsx";
import { renderIcon } from "helpers/for-priject";
import React, { CSSProperties, FC } from "react";
import { IColor } from "theme/types";
import { useStyles } from "./list.style";
//import { useTranslator } from "localization";

export interface ITableWordpair {
  label?: string | number;
  value?: string | number | JSX.Element;
  icon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
}

export interface ITableClasses {
  values?: string;
  labels?: string;
  icons?: string;
  header?: string;
  headerRow?: string;
  tbody?: string;
}

export interface ITable {
  title?: string;
  data: string[] | number[] | ITableWordpair[];
  icon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
  checkValue?: boolean;
  checkLabel?: boolean;
  className?: string;
  classes?: ITableClasses;
  valueAlign?: "left" | "center" | "right";
  paperRow?: boolean;
  style?: CSSProperties;
  fullWidth?: boolean;
  iconColor?: keyof IColor;
}

export const List: FC<ITable> = ({
  data,
  icon,
  title,
  checkValue,
  checkLabel,
  className,
  valueAlign,
  classes,
  paperRow,
  style,
  fullWidth,
  iconColor,
}) => {
  const classNames = useStyles();
  const theme = useTheme();

  const renderItemContent = (item: string | number | ITableWordpair, key: any) => {
    if (typeof item === "object") {
      const valueCond = checkValue ? !!item.value : true;
      const labelCond = checkLabel ? !!item.label : true;
      return (
        valueCond &&
        labelCond && (
          <tr key={key} className={clsx(classNames.tr, "pair-list-row")}>
            {(!!item.icon || !!icon) && (
              <td
                className={clsx(classNames.icons, classes?.icons, "pair-list-icon-cell")}
                style={{ color: iconColor ? theme.palette.color[iconColor] : "inherit" }}
              >
                {renderIcon(item.icon, clsx(classNames.icon, "pair-list-icon")) ||
                  renderIcon(icon, clsx(classNames.icon, "pair-list-icon"))}
              </td>
            )}
            {item?.label && (
              <>
                <td className={clsx(classes?.labels, "pair-list-label-row")}>{item.label}</td>
                {item?.value && (
                  <td
                    style={{ textAlign: valueAlign || "right" }}
                    className={clsx(classes?.values, "pair-list-value-row")}
                  >
                    {item.value}
                  </td>
                )}
              </>
            )}
          </tr>
        )
      );
    } else {
      return <td> {item} </td>;
    }
  };

  return (
    <table
      className={clsx(
        classNames.root,
        "pair-list",
        paperRow && classNames.paperTr,
        fullWidth && classNames.fullWidth,
        className
      )}
      style={style}
    >
      {title && (
        <thead>
          <tr>
            <th colSpan={3} className={clsx(classNames.headerRow, "pair-list-header-row", classes?.headerRow)}>
              <h3 className={classes?.header}>{title}</h3>
            </th>
          </tr>
        </thead>
      )}
      <tbody className={clsx("pair-list-body", classes?.tbody)}>
        {(data as Array<any>).map((item: any, key: number) => renderItemContent(item, key))}
      </tbody>
    </table>
  );
};
