import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    root: {
      borderCollapse: "collapse",
      position: "relative",
      left: -5,
    },
    fullWidth: {
      width: "100%",
    },
    paperTr: {
      borderCollapse: "separate",
      borderSpacing: theme.spacing(0, 1),
      "& tr": {
        "& td": {
          background: theme.palette.color.white,
          padding: theme.spacing(1, 0.6),
          boxShadow: "0 1px 3px rgb(43 45 55 / 10%)",
        },
        "& td:first-child": {
          borderTopLeftRadius: theme.spacing(1),
          borderBottomLeftRadius: theme.spacing(1),
        },
        "& td:last-child": {
          borderTopRightRadius: theme.spacing(1),
          borderBottomRightRadius: theme.spacing(1),
        },
      },
    },
    tr: {
      fontSize: 16,
    },
    labels: {
      width: "100%",
    },
    headerRow: {
      textAlign: "left",
      "& h3": {
        margin: 0,
        marginBottom: 5,
      },
    },
    icons: {
      paddingRight: 5,
    },
    icon: {
      position: "relative",
      top: 2,
      fontSize: 22,
    },
    info: {},
    leftAligned: {},
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {
      icons: {
        paddingRight: theme.spacing(0.1),
      },
    },
    [theme.breakpoints.down("xs")]: {},
  };
});
