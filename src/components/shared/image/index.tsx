import { CSSProperties } from "@material-ui/core/styles/withStyles";
import React, { FC } from "react";
import ImageZoom, { ImageZoomProps } from "react-medium-image-zoom";

interface IImage extends Omit<ImageZoomProps, "image"> {
  src: string;
  alt?: string;
  className?: string;
  style?: CSSProperties;
}

export const Image: FC<IImage> = ({ src, alt, className, style, ...rest }) => (
  <ImageZoom
    image={{ src, className, alt, style }}
    defaultStyles={{
      image: { objectFit: "cover" },
      zoomImage: { objectFit: "cover" },
    }}
    {...rest}
  />
);
