import { red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    button: {
      position: "absolute",
      top: 0,
      left: 0,
      background: theme.palette.action.hover,
      fontWeight: "bold",
      padding: theme.spacing(0.7, 2),
      color: theme.palette.common.black,
      "& .MuiSvgIcon-root": {
        fontSize: 16,
      },
      boxShadow: "none",
    },
    icon: {
      fontSize: ".8rem",
    },
    iconBack: {
      marginRight: theme.spacing(1),
    },
    right: {
      top: 0,
      padding: theme.spacing(1, 2),
      borderBottomLeftRadius: 6,
      left: "auto",
      right: 0,
      borderRadius: 0,
      "&:hover": {
        background: red[400],
        color: theme.palette.common.white,
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
