import React, { FC } from "react";
import { useStyles } from "./action-button.style";
import { useTranslator } from "localization";
import { Button } from "../final-form/button";
import { useHistory } from "react-router-dom";
import clsx from "clsx";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import CloseIcon from "@material-ui/icons/Close";

interface IActionButton {
  onClick?(): void;
  variant?: "close" | "back";
}

export const ActionButton: FC<IActionButton> = ({ onClick = () => {}, variant = "back" }) => {
  const lang = useTranslator("main");
  const history = useHistory();
  const classes = useStyles();

  const handleClick = () => {
    if (variant === "close") {
      onClick();
    } else {
      history.goBack();
    }
  };

  return (
    <Button onClick={handleClick} className={clsx(classes.button, variant === "close" && classes.right)}>
      {variant === "back" ? <ArrowBackIosIcon /> : <CloseIcon />}
      {variant === "back" && lang.back}
    </Button>
  );
};
