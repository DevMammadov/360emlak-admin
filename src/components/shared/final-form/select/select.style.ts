import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    selectRoot: {
      width: "100%",
      padding: "0 !important",
      "& .MuiFormHelperText-root": {
        margin: 0,
        position: "absolute",
        bottom: -20,
        whiteSpace: "nowrap",
      },
      "& .MuiInputBase-root": {
        display: "flex",
        margin: 0,
        background: theme.palette.color.white,
        border: `1px solid ${theme.palette.color.borderColor}`,
        "&:hover": {
          borderColor: theme.palette.common.black,
        },
        "&:focus": {
          borderColor: theme.palette.primary.main,
        },
        "& .MuiInputBase-input": {
          padding: theme.spacing(1),
        },
      },
      "& .MuiInputBase-root.Mui-focused": {
        borderColor: `${theme.palette.primary.main} !important`,
      },
      "& .MuiInputBase-root.Mui-error": {
        borderColor: `red !important`,
      },
      "& .MuiInputBase-root.Mui-disabled": {
        color: "#4B506D",
        background: theme.palette.color.gray,
        "&:hover": {
          borderColor: theme.palette.color.borderColor,
        },
      },
      "& .MuiInputBase-multiline": {
        padding: 0,
      },
      "& .MuiOutlinedInput-notchedOutline": {
        borderWidth: "1px !important",
        top: 0,
      },
      "& fieldset": {
        display: "none",
      },
      "& .MuiInputLabel-root.MuiInputLabel-formControl": {
        display: "none",
      },
    },
    selectLabelContainer: {
      display: "flex",
      justifyContent: "space-between",
    },
    selectLabel: {
      marginBottom: 5,
    },
    selectLabelDisabled: {
      color: "rgba(0, 0, 0, 0.38)",
    },
    selectQuestionMark: {
      color: theme.palette.primary.main,
    },
    selectTooltip: {
      whiteSpace: "pre-line",
    },
    menu: {
      maxHeight: 250,
    },
    native: {
      "& select": {
        minHeight: 35,
        padding: `${theme.spacing(0, 1)} !important`,
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {
      menu: {
        maxHeight: 170,
      },
    },
    [theme.breakpoints.down("xs")]: {},
  };
});
