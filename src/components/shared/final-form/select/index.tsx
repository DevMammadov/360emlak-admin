import React, { FC } from "react";
import { useStyles } from "./select.style";
import { Select as MuiRffSelect, SelectProps } from "mui-rff";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import IconButton from "@material-ui/core/IconButton/IconButton";
import HelpIcon from "@material-ui/icons/Help";
import clsx from "clsx";
import { FieldValidator } from "final-form";
import { getValidator } from "../helper";
import { OnChange, OnBlur } from "react-final-form-listeners";
import { MenuItem } from "@material-ui/core";

export interface ISelect extends SelectProps {
  questionMarkTitle?: string;
  validate?: FieldValidator<any> | FieldValidator<any>[];
  onChange?(value: any, previous: any): void;
  emptyOption?: boolean;
  emptyLabel?: string;
}

export const Select: FC<ISelect> = ({
  questionMarkTitle,
  className,
  validate,
  onChange,
  native,
  emptyLabel,
  data,
  onSelect = () => {},
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={clsx("MuiSelect-root", "MuiSelect-select", classes.selectRoot, className)}>
      <label>
        {rest.label && (
          <div className={classes.selectLabelContainer}>
            <div className={clsx(classes.selectLabel, rest.disabled && classes.selectLabelDisabled)}>{rest.label}</div>
            {questionMarkTitle && (
              <Tooltip title={questionMarkTitle} classes={{ tooltip: classes.selectTooltip }}>
                <IconButton edge="end" size="small" style={{ cursor: "default" }}>
                  <HelpIcon className={classes.selectQuestionMark} />
                </IconButton>
              </Tooltip>
            )}
          </div>
        )}

        {native ? (
          <>
            <MuiRffSelect
              variant="outlined"
              native
              {...rest}
              fieldProps={{ validate: getValidator(validate) }}
              label=""
              className={classes.native}
              MenuProps={{ classes: { paper: classes.menu } }}
            >
              {emptyLabel && <option value=""> {emptyLabel} </option>}
              {data?.map((type) => (
                <option key={type.label} value={type.value}>
                  {type.label}
                </option>
              ))}
            </MuiRffSelect>
            {onChange && (
              <OnChange name={rest.name}>
                {(value, previous) => {
                  onChange(value, previous);
                }}
              </OnChange>
            )}
          </>
        ) : (
          <>
            <MuiRffSelect
              variant="outlined"
              displayEmpty={!!emptyLabel}
              {...rest}
              fieldProps={{ validate: getValidator(validate) }}
              MenuProps={{ classes: { paper: classes.menu } }}
              label=""
            >
              {emptyLabel && <MenuItem value=""> {emptyLabel} </MenuItem>}
              {data?.map((type) => (
                <MenuItem key={type.label} value={type.value}>
                  {type.label}
                </MenuItem>
              ))}
            </MuiRffSelect>
            {onChange && (
              <OnChange name={rest.name}>
                {(value, previous) => {
                  onChange(value, previous);
                }}
              </OnChange>
            )}
          </>
        )}
      </label>
    </div>
  );
};
