import { Button, CircularProgress, IconButton } from "@material-ui/core";
import PhotoLibraryIcon from "@material-ui/icons/PhotoLibrary";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import clsx from "clsx";
import { FieldValidator } from "final-form";
import { getPhoto } from "helpers/functions";
import { useTranslator } from "localization";
import { ShowErrorFunc, showErrorOnChange } from "mui-rff";
import React, { FC, useEffect, useState } from "react";
import { Field, FieldRenderProps } from "react-final-form";
import { getValidator } from "../helper";
import { useStyles } from "./file.style";

interface Classes {
  image?: string;
}

export interface IFileField {
  name: string;
  label?: string;
  icon?: string;
  multiple?: boolean;
  errorText?: any;
  helperText?: string;
  accept?: string;
  priview?: boolean;
  error?: any;
  initialPhotos?: string[];
  showError?: ShowErrorFunc;
  onInitialPhotoRemove?(img: string): void;
  validate?: FieldValidator<any> | FieldValidator<any>[];
  loaderFor?: string;
  minRemoveCount?: number;
  classes?: Classes;
}

interface IFileUrl {
  url: string;
  file: File;
}

export const FileField: FC<IFileField> = ({ name, validate, ...rest }) => {
  return (
    <Field
      name={name}
      validate={getValidator(validate)}
      type="file"
      render={(props) => <FileFieldWrapper name={name} {...props} {...rest} />}
    />
  );
};

const FileFieldWrapper: FC<IFileField & FieldRenderProps<HTMLInputElement, HTMLElement>> = (props) => {
  const classNames = useStyles();
  const lang = useTranslator("main");
  const [fileArr, setFileArr] = useState<IFileUrl[]>([]);
  const [_initialPhotos, setInitialPhotos] = useState<string[]>([]);
  const {
    helperText,
    multiple = false,
    label,
    priview,
    errorText,
    initialPhotos = [],
    onInitialPhotoRemove = () => {},
    input: { name, onChange, value },
    meta,
    showError = showErrorOnChange,
    loaderFor,
    accept,
    minRemoveCount = 0,
    classes,
  } = props;

  const { error, submitError } = meta;
  const isError = showError({ meta });

  useEffect(() => {
    setFileArr([]);
  }, []);

  useEffect(() => {
    setInitialPhotos(initialPhotos);
  }, [initialPhotos.length]);

  useEffect(() => {
    if (initialPhotos.length === 0 && fileArr.length === 0) {
      onChange(undefined); // for required validation
    }
  }, [_initialPhotos, fileArr]);

  const handleRemove = (url: IFileUrl) => {
    setFileArr([...fileArr.filter((u) => u !== url)]);
    onChange(multiple ? [...fileArr.filter((u) => u !== url).map((u) => u.file)] : undefined);
  };

  const handleInitialRemove = (item: string) => {
    onInitialPhotoRemove(item);
  };

  const handleChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    let fileList = target.files;
    if (fileList) {
      if (multiple) {
        let files = [];
        for (let i = 0; i < fileList.length; i++) {
          //@ts-ignore
          setFileArr((prev) => [...prev, { url: URL.createObjectURL(fileList[i]), file: fileList[i] }]);
          files.push(fileList[i]);
        }
        onChange([...fileArr.map((u) => u.file), ...files]);
      } else {
        setFileArr([{ url: URL.createObjectURL(fileList[0]), file: fileList[0] }]);
        onChange(fileList[0]);
      }
    }
  };

  const renderLabel = () => {
    if (isError) {
      return error;
    } else if (!multiple) {
      const name = fileArr[0]?.file.name;
      return fileArr.length > 0 ? `${name.substr(0, 5)} ... ${name.substr(name.length - 5, name.length)}` : label;
    } else {
      return `${label} (${fileArr.length + _initialPhotos.length})`;
    }
  };

  return (
    <div>
      <label>
        {!multiple && priview && (_initialPhotos.length > 0 || fileArr.length > 0) ? (
          <label>{label}</label>
        ) : (
          <>
            <Button component="span" className={clsx(isError && classNames.redFont)}>
              <PhotoLibraryIcon className={clsx(classNames.fileIcon, isError && classNames.redFont)} />
              {renderLabel()}
            </Button>
          </>
        )}
        <input className={classNames.input} type="file" accept={accept} multiple={multiple} onChange={handleChange} />
      </label>
      <div className={clsx(classNames.imgContainer, "imgContainer")}>
        {priview &&
          _initialPhotos?.map((item, i) => (
            <div key={i}>
              {loaderFor === item && (
                <div className={classNames.loader}>
                  <CircularProgress />
                </div>
              )}
              <img
                src={item ? (typeof item === "object" ? URL.createObjectURL(item) : getPhoto(item)) : ""}
                alt={item}
                className={clsx(classes?.image)}
              />
              {loaderFor !== item && _initialPhotos.length > minRemoveCount && (
                <IconButton onClick={() => loaderFor !== item && handleInitialRemove(item)}>
                  <RemoveCircleIcon />
                </IconButton>
              )}
            </div>
          ))}
        {priview &&
          fileArr.map((item, i) => (
            <div key={i}>
              <img src={item.url} alt="..." className={clsx(classes?.image)} />
              <IconButton onClick={() => handleRemove(item)}>
                <RemoveCircleIcon />
              </IconButton>
            </div>
          ))}
      </div>
    </div>
  );
};
