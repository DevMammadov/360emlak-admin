import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    input: {
      display: "none",
    },
    fileIcon: {
      marginRight: theme.spacing(1),
      color: theme.palette.primary.main,
    },
    fileName: {
      fontSize: "1rem",
      color: theme.palette.primary.main,
    },
    errorText: {
      color: "#c62828",
      fontSize: "0.75rem",
      margin: 0,
    },
    redFont: {
      color: "#c62828",
    },
    imgContainer: {
      display: "flex",
      flexWrap: "wrap",
      marginTop: theme.spacing(1),
      "& img": {
        height: 100,
        width: 100,
        borderRadius: 6,
        objectFit: "cover",
        marginRight: theme.spacing(1),
        marginTop: theme.spacing(1),
      },
      "& > div": {
        position: "relative",
        "& .MuiIconButton-root": {
          position: "absolute",
          right: 10,
          top: -2,
          padding: 1,
          background: theme.palette.color.red,
          color: theme.palette.color.white,
          "& .MuiSvgIcon-root": {
            fontSize: 26,
          },
        },
      },
    },
    loader: {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
      "& .MuiCircularProgress-root": {
        color: theme.palette.color.orange,
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
