import HelpIcon from "@material-ui/icons/Help";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { IconButton, InputAdornment, SvgIconTypeMap, Tooltip } from "@material-ui/core";
import clsx from "clsx";
import { TextField as MuiRffTextField, TextFieldProps } from "mui-rff";
import React, { FC, useState } from "react";
import { useStyles } from "./text-field.style";
import { FieldState, FieldValidator } from "final-form";
import { getValidator } from "../helper";
import { useValidators } from "hooks/useValidators";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import { OnChange } from "react-final-form-listeners";

export interface ITextField extends TextFieldProps {
  questionMarkTitle?: string;
  variant?: "outlined" | "filled" | "standard";
  validate?: FieldValidator<any> | FieldValidator<any>[];
  adornmentPos?: "end" | "start";
  adornmentIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
  onChange?(value: string): void;
}

export const TextField: FC<ITextField> = ({
  questionMarkTitle,
  variant = "outlined",
  adornmentPos = "start",
  onChange = () => {},
  adornmentIcon,
  validate,
  type,
  ...rest
}) => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const ElementIcon: FC = adornmentIcon || (() => <></>);
  const { email, number } = useValidators();

  const passwordAdornment = () => {
    return (
      <InputAdornment position="end">
        <IconButton
          onClick={() => setShowPassword(!showPassword)}
          onMouseDown={(e) => e.preventDefault()}
          edge="end"
          size="small"
        >
          {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
        </IconButton>
      </InputAdornment>
    );
  };

  const defaultValidators = () => {
    switch (type) {
      case "email":
        return [email];
      case "number":
        return [number];
      default:
        return [];
    }
  };

  return (
    <div className={`MuiFormControl-root MuiTextField-root ${classes.textFieldRoot}`}>
      <label>
        {rest.label && (
          <div className={classes.textFieldLabelContainer}>
            <div className={clsx(classes.textFieldLabel, rest.disabled && classes.textFieldLabelDisabled)}>
              {rest.label}
            </div>
            {questionMarkTitle && (
              <Tooltip title={questionMarkTitle} classes={{ tooltip: classes.textFieldTooltip }}>
                <IconButton edge="end" size="small" style={{ cursor: "default" }}>
                  <HelpIcon />
                </IconButton>
              </Tooltip>
            )}
          </div>
        )}

        <MuiRffTextField
          variant={variant}
          hiddenLabel
          InputProps={{
            autoComplete: type === "password" ? "off" : "on",
            endAdornment:
              type === "password"
                ? passwordAdornment()
                : adornmentPos === "end" &&
                  adornmentIcon && (
                    <InputAdornment position={adornmentPos}>
                      <ElementIcon />
                    </InputAdornment>
                  ),
            startAdornment: adornmentPos === "start" && adornmentIcon && (
              <InputAdornment position={adornmentPos}>
                <ElementIcon />
              </InputAdornment>
            ),
          }}
          type={type === "password" ? (showPassword ? "text" : "password") : type}
          fieldProps={{ validate: getValidator(validate, defaultValidators()) }}
          label=""
          {...rest}
        />
        {onChange && (
          <OnChange name={rest.name}>
            {(value) => {
              onChange(value);
            }}
          </OnChange>
        )}
      </label>
    </div>
  );
};
