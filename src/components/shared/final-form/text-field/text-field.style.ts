import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    textFieldRoot: {
      width: "100%",
      "& .MuiFormHelperText-root": {
        margin: 0,
        position: "absolute",
        bottom: -20,
        whiteSpace: "nowrap",
      },
      "& .MuiInputBase-root": {
        display: "flex",
        margin: 0,
        background: theme.palette.color.white,
        border: `1px solid ${theme.palette.color.borderColor}`,
        "&:hover": {
          borderColor: theme.palette.common.black,
        },
        "&:focus": {
          borderColor: theme.palette.primary.main,
        },
        "& .MuiInputBase-input": {
          padding: theme.spacing(1),
        },
      },
      "& .MuiInputBase-root.Mui-focused": {
        borderColor: `${theme.palette.primary.main} !important`,
      },
      "& .MuiInputBase-root.Mui-error": {
        borderColor: `red !important`,
      },
      "& .MuiInputBase-root.Mui-disabled": {
        color: "#4B506D",
        background: theme.palette.color.gray,
        "&:hover": {
          borderColor: theme.palette.color.borderColor,
        },
      },
      "& .MuiInputBase-multiline": {
        padding: 0,
      },
      "& .MuiOutlinedInput-notchedOutline": {
        borderWidth: "1px !important",
        top: 0,
      },
      "& fieldset": {
        display: "none",
      },
      "& .MuiInputLabel-root.MuiInputLabel-formControl": {
        display: "none",
      },
    },
    textFieldLabelContainer: {
      display: "flex",
      justifyContent: "space-between",
    },
    textFieldLabel: {
      marginBottom: 5,
      //lineHeight: 1.3,
    },
    textFieldLabelDisabled: {
      color: "rgba(0, 0, 0, 0.38)",
    },
    textFieldQuestionMark: {
      color: theme.palette.primary.main,
    },
    textFieldTooltip: {
      whiteSpace: "pre-line",
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
