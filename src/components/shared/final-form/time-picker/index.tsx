import DateFnsUtils from "@date-io/date-fns";
import { KeyboardTimePicker, KeyboardTimePickerProps } from "mui-rff";
import React, { FC } from "react";
import { TextField } from "../simple-text-field";
import AccessTimeIcon from "@material-ui/icons/AccessTime";

export interface ITimePicker {}

export const TimePicker: FC<ITimePicker & KeyboardTimePickerProps> = ({ ...rest }) => {
  return (
    <KeyboardTimePicker
      {...rest}
      dateFunsUtils={DateFnsUtils}
      keyboardIcon={<AccessTimeIcon />}
      TextFieldComponent={(props) => <TextField type={props.type as any} name={props.name as string} {...props} />}
    />
  );
};
