import { FieldState, FieldValidator } from "final-form";

export const getValidator = (
  validators: FieldValidator<any> | FieldValidator<any>[] | undefined,
  builtInValidators = [] as FieldValidator<any>[]
) => {
  const validatorArr =
    typeof validators === "object" ? [...validators, ...builtInValidators] : [validators, ...builtInValidators];

  return (value: any, allValues: object, meta?: FieldState<any> | undefined) =>
    validatorArr.reduce((error, validator) => error || (validator && validator(value, allValues, meta)), undefined);
};
