import { grey } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    root: {
      width: "100%",
      "& .MuiFormHelperText-root": {
        margin: 0,
        position: "absolute",
        bottom: -20,
        whiteSpace: "nowrap",
      },
      "& .MuiInputBase-root": {
        display: "flex",
        margin: 0,
        background: "white",
        border: `1px solid ${grey[400]}`,
        "&:hover": {
          borderColor: "black",
        },
        "&:focus": {
          borderColor: "#3f51b5",
        },
        "& .MuiInputBase-input": {
          padding: theme.spacing(1),
        },
      },
      "& .MuiInputBase-root.Mui-focused": {
        borderColor: `#3f51b5 !important`,
      },
      "& .MuiInputBase-root.Mui-error": {
        borderColor: `red !important`,
      },
      "& .MuiInputBase-root.Mui-disabled": {
        color: theme?.palette?.primary?.light,
        background: theme?.palette?.color?.gray,
        "&:hover": {
          borderColor: theme?.palette?.color?.borderColor,
        },
      },
      "& .MuiInputBase-multiline": {
        padding: 0,
      },
      "& .MuiOutlinedInput-notchedOutline": {
        borderWidth: "1px !important",
        top: 0,
      },
      "& fieldset": {
        display: "none",
      },
      "& .MuiInputLabel-root.MuiInputLabel-formControl": {
        display: "none",
      },
    },
    labelContainer: {
      display: "flex",
      justifyContent: "space-between",
    },
    label: {
      marginBottom: 5,
      lineHeight: 1.3,
    },
    labelDisabled: {
      color: "rgba(0, 0, 0, 0.38)",
    },
    questionMark: {
      color: theme?.palette?.primary?.main,
    },
    tooltip: {
      whiteSpace: "pre-line",
    },
    adornImg: {
      height: 25,
      width: 25,
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
