import React, { FC, useState } from "react";
import {
  TextField as MuiTextField,
  TextFieldProps,
  InputAdornment,
  Tooltip,
  IconButton,
  SvgIconTypeMap,
} from "@material-ui/core";
import { useStyles } from "./simple-text-field.style";
import clsx from "clsx";
import HelpIcon from "@material-ui/icons/Help";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";

export interface ITextField {
  errorText?: any;
  showQuestionMark?: boolean;
  questionMarkTitle?: string;
  adornmentPos?: "end" | "start";
  adornmentIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">> | string;
}

export const TextField: FC<ITextField & TextFieldProps> = ({
  errorText,
  label,
  adornmentPos,
  adornmentIcon,
  showQuestionMark = false,
  questionMarkTitle = "",
  type,
  className,
  ...rest
}) => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const ElementIcon: FC = (adornmentIcon as OverridableComponent<SvgIconTypeMap<{}, "svg">>) || (() => <></>);

  const passwordAdornment = () => {
    return (
      <InputAdornment position="end">
        <IconButton
          onClick={() => setShowPassword(!showPassword)}
          onMouseDown={(e) => e.preventDefault()}
          edge="end"
          size="small"
        >
          {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
        </IconButton>
      </InputAdornment>
    );
  };

  return (
    <div className={`MuiFormControl-root MuiTextField-root ${classes.root}`}>
      <label>
        <div className={classes.labelContainer}>
          <div className={clsx(classes.label, rest.disabled && classes.labelDisabled)}>{label}</div>
          {showQuestionMark && questionMarkTitle && (
            <Tooltip title={questionMarkTitle} classes={{ tooltip: classes.tooltip }}>
              <IconButton edge="end" size="small" style={{ cursor: "default" }}>
                <HelpIcon className={classes.questionMark} />
              </IconButton>
            </Tooltip>
          )}
        </div>

        <MuiTextField
          className={clsx(classes.root, className)}
          InputProps={{
            endAdornment:
              type === "password"
                ? passwordAdornment()
                : adornmentPos === "end" &&
                  adornmentIcon && (
                    <InputAdornment position={adornmentPos}>
                      {typeof adornmentIcon === "string" ? (
                        <img src={adornmentIcon} className={classes.adornImg} />
                      ) : (
                        <ElementIcon />
                      )}
                    </InputAdornment>
                  ),
            startAdornment: adornmentPos === "start" && adornmentIcon && (
              <InputAdornment position={adornmentPos}>
                {typeof adornmentIcon === "string" ? (
                  <img src={adornmentIcon} className={classes.adornImg} />
                ) : (
                  <ElementIcon />
                )}
              </InputAdornment>
            ),
          }}
          {...rest}
          hiddenLabel
          variant="outlined"
        />
      </label>
    </div>
  );
};
