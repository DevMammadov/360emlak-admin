import { SvgIconTypeMap, TextFieldProps } from "@material-ui/core";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import {
  AutocompleteChangeDetails,
  AutocompleteChangeReason,
  AutocompleteProps as MuiAutocompleteProps,
  default as MuiAutocomplete,
} from "@material-ui/lab/Autocomplete";
import { UseAutocompleteProps as MuiUseAutocompleteProps, Value } from "@material-ui/lab/useAutocomplete";
import clsx from "clsx";
import { FieldValidator } from "final-form";
import { ShowErrorFunc, showErrorOnChange } from "mui-rff";
import React, { ReactNode } from "react";
import { Field, FieldProps, FieldRenderProps } from "react-final-form";
//import { ITextField as MuiTextFieldProps, TextField } from "../";
import { getValidator } from "../helper";
import { ITextField, TextField } from "../simple-text-field";
import { useStyles } from "./auto-complete.style";

type MuiTextFieldProps = ITextField & TextFieldProps;

export type AutocompleteData = {
  [key: string]: any | null;
};

export interface AutocompleteProps<
  T,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> extends Omit<
    MuiAutocompleteProps<T, Multiple, DisableClearable, FreeSolo> &
      MuiUseAutocompleteProps<T, Multiple, DisableClearable, FreeSolo>,
    "renderInput"
  > {
  name: string;
  label: ReactNode;
  helperText?: string;
  required?: boolean;
  getOptionValue?: (option: T) => any;
  options: T[];
  validate?: FieldValidator<any> | FieldValidator<any>[];
  fieldProps?: Partial<FieldProps<any, any>>;
  textFieldProps?: Partial<MuiTextFieldProps>;
  questionMarkTitle?: string;
  showError?: ShowErrorFunc;
  adornmentPos?: "end" | "start";
  adornmentIcon?: OverridableComponent<SvgIconTypeMap<{}, "svg">>;
}

export function Autocomplete<
  T,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(props: AutocompleteProps<T, Multiple, DisableClearable, FreeSolo>): JSX.Element {
  const { name, fieldProps, validate, ...rest } = props;

  return (
    <Field
      name={name}
      render={(fieldRenderProps) => <AutocompleteWrapper {...fieldRenderProps} {...rest} />}
      {...fieldProps}
      validate={getValidator(validate)}
    />
  );
}

interface AutocompleteWrapperProps<
  T,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
> extends AutocompleteProps<T, Multiple, DisableClearable, FreeSolo> {
  label: ReactNode;
  required?: boolean;
  textFieldProps?: Partial<MuiTextFieldProps>;
  getOptionValue?: (option: T) => any;
}

function AutocompleteWrapper<
  T,
  Multiple extends boolean | undefined,
  DisableClearable extends boolean | undefined,
  FreeSolo extends boolean | undefined
>(
  props: AutocompleteWrapperProps<T, Multiple, DisableClearable, FreeSolo> &
    FieldRenderProps<MuiTextFieldProps, HTMLElement>
): JSX.Element {
  const {
    input: { name, onChange, value },
    meta,
    options,
    label,
    required,
    multiple,
    textFieldProps,
    getOptionValue,
    showError = showErrorOnChange,
    placeholder,
    onChange: onChangeCallback,
    ...rest
  } = props;

  function getValue(values: any) {
    if (!getOptionValue) {
      return values;
    }

    // ternary hell...
    return multiple ? (values ? values.map(getOptionValue) : null) : values ? getOptionValue(values) : null;
  }

  const { helperText, adornmentPos, questionMarkTitle, adornmentIcon, ...lessrest } = rest;
  const { variant, ...restTextFieldProps } = textFieldProps || {};

  // yuck...
  let defaultValue: Value<T, Multiple, DisableClearable, FreeSolo> | undefined;

  if (!getOptionValue) {
    //defaultValue = value as Value<T, Multiple, DisableClearable, FreeSolo> | undefined;
    defaultValue = value as any;
  } else if (value) {
    options?.forEach((option) => {
      const optionValue = getOptionValue(option);
      if (multiple) {
        if (!defaultValue) {
          defaultValue = [] as any;
        }
        (value as any).forEach((v: any) => {
          if (v === optionValue) {
            (defaultValue as any).push(option);
          }
        });
      } else {
        if (value === optionValue) {
          defaultValue = option as any;
        }
      }
    });
  }

  const onChangeFunc = (
    // eslint-disable-next-line @typescript-eslint/ban-types
    event: React.ChangeEvent<{}>,
    value: Value<T, Multiple, DisableClearable, FreeSolo>,
    reason: AutocompleteChangeReason,
    details?: AutocompleteChangeDetails<any>
  ) => {
    const gotValue = getValue(value);
    onChange(gotValue);

    if (onChangeCallback) {
      onChangeCallback(event, value, reason, details);
    }
  };

  const { error, submitError } = meta;
  const isError = showError({ meta });
  const classes = useStyles();

  return (
    <MuiAutocomplete
      multiple={multiple}
      onChange={onChangeFunc}
      options={options || []}
      //@ts-ignore
      value={defaultValue || null}
      renderInput={(params) => (
        <TextField
          label={label}
          required={required}
          helperText={isError ? error || submitError : helperText}
          error={isError}
          placeholder={placeholder}
          variant={variant}
          adornmentPos={adornmentPos}
          questionMarkTitle={questionMarkTitle}
          adornmentIcon={adornmentIcon}
          {...params}
          {...restTextFieldProps}
          fullWidth={true}
          name=""
        />
      )}
      className={clsx(classes.root, lessrest.className)}
      {...lessrest}
    />
  );
}
