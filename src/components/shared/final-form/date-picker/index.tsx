import MomentUtils from "@date-io/date-fns";
import {
  KeyboardDatePicker as MuiKeyboardDatePicker,
  KeyboardDatePickerProps as MuiKeyboardDatePickerProps,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { TextField } from "../simple-text-field";
import { FieldValidator } from "final-form";
import { useValidators } from "hooks/useValidators";
import { useTranslator } from "localization/useTranslator";
import { ShowErrorFunc, showErrorOnChange } from "mui-rff";
import React, { FC } from "react";
import { Field, FieldProps, FieldRenderProps } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import { getValidator } from "../helper";
import { useStyles } from "./date-picker.style";

export interface KeyboardDatePickerProps extends Partial<MuiKeyboardDatePickerProps> {
  name: string;
  fieldProps?: Partial<FieldProps<any, any>>;
  validate?: FieldValidator<any> | FieldValidator<any>[];
  showError?: ShowErrorFunc;
  iconTextPoisition?: "end" | "start";
  questionMarkTitle?: string;
  iconText?: string | Element;
}

export const DatePicker: FC<KeyboardDatePickerProps> = (props) => {
  const { name, fieldProps, validate, ...rest } = props;
  const { validDate } = useValidators();

  return (
    <Field
      name={name}
      render={(fieldRenderProps: any) => <KeyboardDatePickerWrapper {...fieldRenderProps} {...rest} />}
      {...fieldProps}
      validate={getValidator(validate, [validDate])}
    />
  );
};

interface DatePickerWrapperProps extends FieldRenderProps<MuiKeyboardDatePickerProps, HTMLElement> {
  iconTextPoisition?: "end" | "start";
  questionMarkTitle?: string;
  iconText?: string | Element;
}

const KeyboardDatePickerWrapper: FC<DatePickerWrapperProps & MuiKeyboardDatePickerProps> = (props) => {
  const {
    input: { name, onChange, value, ...restInput },
    meta,
    showError = showErrorOnChange,
    ...rest
  } = props;

  const classes = useStyles();
  const lang = useTranslator();
  const { error, submitError } = meta;
  const isError = showError({ meta });

  const { helperText, ...lessrest } = rest;

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <MuiKeyboardDatePicker
        disableToolbar
        helperText={isError ? error || submitError : helperText}
        format="yyyy-MM-dd"
        TextFieldComponent={(props: any) => (
          <TextField {...props} placeholder={`${lang.day} ${lang.mounth} ${lang.year}`} />
        )}
        error={isError}
        name={name}
        className={classes.root}
        variant="inline"
        value={(value as any) === "" ? null : value}
        inputProps={restInput}
        invalidDateMessage={lang.invalidDateMessage}
        maxDateMessage={lang.maxDateMessage}
        minDateMessage={lang.minDateMessage}
        fullWidth
        autoOk
        {...lessrest}
        onChange={onChange}
      />
      <OnChange name={name}>
        {(value, previous) => {
          props.onChange && props.onChange(value);
        }}
      </OnChange>
    </MuiPickersUtilsProvider>
  );
};
