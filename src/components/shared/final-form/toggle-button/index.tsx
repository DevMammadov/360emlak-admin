import {
  ToggleButton as MuiToggleButton,
  ToggleButtonGroup,
  ToggleButtonGroupProps,
  ToggleButtonProps,
} from "@material-ui/lab";
import { useTranslator } from "localization";
import React, { FC } from "react";
import { Field } from "react-final-form";
import { OnChange } from "react-final-form-listeners";

export interface IToggleButtonData {
  label: string;
  value: string;
}

export interface IToggleButton {
  name: string;
  buttonProps?: ToggleButtonProps;
  groupProps?: ToggleButtonGroupProps;
  className?: string;
  selected?: string;
  data: IToggleButtonData[];
  onChange?(value: string): void;
  type?: "radio" | "checkbox";
}

export const ToggleButton: FC<IToggleButton> = ({
  name,
  buttonProps,
  groupProps,
  className,
  selected,
  data,
  type = "checkbox",
  onChange = () => {},
}) => {
  const lang = useTranslator();

  const [v, sv] = React.useState([]);

  return (
    <>
      <Field
        name={name}
        render={({ input, meta }) => (
          <ToggleButtonGroup
            size="small"
            className={className}
            value={v}
            //value={input.value}
            exclusive={type === "radio"}
            onChange={(e, v) => {
              sv(v);
              console.log(v);
              //input.onChange(v);
            }}
            {...groupProps}
          >
            {data.map((d, i) => (
              <MuiToggleButton key={i} classes={{ selected: selected }} value={d.value} {...buttonProps}>
                <label>{d.label}</label>
              </MuiToggleButton>
            ))}
          </ToggleButtonGroup>
        )}
      />
      <OnChange name={name}>
        {(value, previous) => {
          onChange(value);
        }}
      </OnChange>
    </>
  );
};
