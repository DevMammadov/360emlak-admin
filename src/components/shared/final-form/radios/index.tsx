import React, { FC } from "react";
import { useStyles } from "./radios.style";
import { Radios as MuiRffRadios, RadiosProps } from "mui-rff";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import IconButton from "@material-ui/core/IconButton/IconButton";
import HelpIcon from "@material-ui/icons/Help";
import clsx from "clsx";
import { FieldValidator } from "final-form";
import { getValidator } from "../helper";

export interface IRadios {
  questionMarkTitle?: string;
  validate?: FieldValidator<any> | FieldValidator<any>[];
}

export const Radios: FC<IRadios & RadiosProps> = ({ questionMarkTitle, className, validate, ...rest }) => {
  const classes = useStyles();

  return (
    <div className={clsx("MuiSelect-root", "MuiSelect-select", classes.selectRoot, className)}>
      <label>
        {rest.label && (
          <div className={classes.selectLabelContainer}>
            <div className={clsx(classes.selectLabel, rest.disabled && classes.selectLabelDisabled)}>{rest.label}</div>
            {questionMarkTitle && (
              <Tooltip title={questionMarkTitle} classes={{ tooltip: classes.selectTooltip }}>
                <IconButton edge="end" size="small" style={{ cursor: "default" }}>
                  <HelpIcon className={classes.selectQuestionMark} />
                </IconButton>
              </Tooltip>
            )}
          </div>
        )}
        <MuiRffRadios {...rest} fieldProps={{ validate: getValidator(validate) }} label="" />
      </label>
    </div>
  );
};
