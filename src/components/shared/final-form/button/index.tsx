import React, { FC } from "react";
import { useStyles } from "./button.style";
import { useTranslator } from "localization";
import { Button as MuiButton, ButtonProps, SvgIconTypeMap, useTheme } from "@material-ui/core";
import { IColor } from "theme/types";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import clsx from "clsx";
import { useButtonStyle } from "./helper";

type MaterialIcon = OverridableComponent<SvgIconTypeMap<{}, "svg">>;
export type IFor = "search" | "add" | "approve" | "deny";

export interface IButton extends ButtonProps {
  text?: keyof IColor;
  background?: keyof IColor;
  icon?: MaterialIcon;
  for?: IFor;
}

export const Button: FC<IButton> = ({
  text,
  variant = "contained",
  background,
  for: forWhat,
  icon,
  children,
  ...rest
}) => {
  const lang = useTranslator("main");
  const classes = useStyles();
  const theme = useTheme();
  const IconElement: MaterialIcon = icon as MaterialIcon;
  const btnStyle = useButtonStyle(forWhat);

  return (
    <MuiButton
      variant={variant}
      color={background && text ? "inherit" : "primary"}
      className={clsx(classes.button, rest.className)}
      style={{
        color: text ? theme.palette.color[text] : "",
        backgroundColor: background && variant === "contained" ? theme.palette.color[background] : "",
        borderColor: variant === "outlined" && background ? theme.palette.color[background] : "",
      }}
      {...rest}
    >
      {icon ? <IconElement className={classes.icon} /> : btnStyle.icon}

      {btnStyle.title || children}
    </MuiButton>
  );
};
