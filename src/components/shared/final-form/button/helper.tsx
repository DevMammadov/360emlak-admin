import { IFor } from "components/shared";
import SearchIcon from "@material-ui/icons/Search";
import AddIcon from "@material-ui/icons/Add";
import CheckIcon from "@material-ui/icons/Check";
import { useTranslator } from "localization";
import ClearIcon from "@material-ui/icons/Clear";
import { useStyles } from "./button.style";

export const useButtonStyle = (forWhat?: IFor) => {
  const lang = useTranslator("main");
  const classes = useStyles();

  switch (forWhat) {
    case "search":
      return { icon: <SearchIcon className={classes.icon} />, title: lang.search };
    case "add":
      return { icon: <AddIcon className={classes.icon} />, title: lang.add };
    case "approve":
      return { icon: <CheckIcon className={classes.icon} />, title: lang.approve };
    case "deny":
      return { icon: <ClearIcon className={classes.icon} />, title: lang.deny };
    default:
      return { icon: <></>, title: undefined };
  }
};
