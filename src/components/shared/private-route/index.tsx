import React, { FC } from "react";
import { useTranslator } from "localization";
import { useUser } from "hooks/useUser";
import { Redirect, Route, RouteProps, useHistory } from "react-router-dom";
import { useStyles } from "./private-route.style";
import icon from "assets/folder2.svg";
import { Button } from "@material-ui/core";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import { links } from "routes/links";

export interface IPrivateRoute {}

export const PrivateRoute: FC<IPrivateRoute & RouteProps> = ({ ...rest }) => {
  const lang = useTranslator("login");
  const currentUser = useUser();
  const classes = useStyles();
  const history = useHistory();

  return currentUser.online ? <Route {...rest} /> : <Redirect to={links.login.login} />;
};
