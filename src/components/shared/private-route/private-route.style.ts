import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    section: {
      height: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      fontSize: 18,
      fontWeight: "bold",
      "& img": {
        height: 140,
        width: 140,
        marginBottom: theme.spacing(2),
      },
      "& > div": {
        textAlign: "center",
        "& .MuiButton-root": {
          marginTop: theme.spacing(2),
          padding: theme.spacing(1, 6),
          "& .MuiSvgIcon-root": {
            marginRight: 5,
          },
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
