import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    alertMessage: {
      width: "100%",
    },
    alertAction: {
      alignItems: "center !important",
      paddingRight: theme.spacing(2),
    },
    title: {
      fontWeight: "bold",
    },
    alertIcon: {
      fontSize: "25px",
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {
      alertIcon: {
        display: "none",
      },
      alertAction: {
        alignItems: "start !important",
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(0.5),
      },
    },
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
