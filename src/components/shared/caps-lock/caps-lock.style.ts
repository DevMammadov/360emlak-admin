import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    capslockWarn: {
      display: "flex",
      alignItems: "center",
      fontSize: 13,
      fontWeight: "bold",
      "& .MuiSvgIcon-root": {
        color: theme.palette.color.orange,
        fontSize: 22,
        marginRight: theme.spacing(0.5),
        top: -2,
        position: "relative",
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
