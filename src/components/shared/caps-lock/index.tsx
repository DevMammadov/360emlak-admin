import React, { FC } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./caps-lock.style";
//@ts-ignore
import ReactIsCapsLockActive from "@matsun/reactiscapslockactive";
import WarningIcon from "@material-ui/icons/Warning";

export interface ICapsLock {}

export const CapsLock: FC<ICapsLock> = ({}) => {
  const lang = useTranslator("main");
  const classes = useStyles();

  return (
    <ReactIsCapsLockActive>
      {(active: boolean) =>
        active && (
          <div className={classes.capslockWarn}>
            <WarningIcon /> {lang.capslockIsActive}
          </div>
        )
      }
    </ReactIsCapsLockActive>
  );
};
