import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { TextField as SimpleTextField } from "components/shared/final-form/simple-text-field";
import { ClickAwayListener, List, ListItem, ListItemIcon, ListItemText, Paper } from "@material-ui/core";
import FolderIcon from "@material-ui/icons/Folder";
import { useStyles } from "./search-route.style";
import userNoImage from "assets/userNoImage.png";
import { getPhoto, isEmpty } from "helpers/functions";
import { Field } from "react-final-form";

export interface IUser {
  id: string;
  fullName: string;
  iamAgent: boolean;
  email: string;
  photo: string;
}

interface ISreachInput {
  data: IUser[];
  onChange(name: string): void;
  onItemClick(user: IUser): void;
  value?: string;
  name: string;
}

export const SearchInput: FC<ISreachInput> = ({ onChange, data, value = "", onItemClick, name }) => {
  const lang = useTranslator("main");
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState({} as IUser);
  const [val, setVal] = useState("");
  const classes = useStyles();

  const handleItemClick = (user: IUser) => {
    onItemClick(user);
    setSelected(user);
    setVal(user.email);
    setOpen(false);
  };

  const handleChange = (e: any) => {
    setSelected({} as IUser);
    onChange(e.target.value);
    setVal(e.target.value);
  };

  return (
    <div className={classes.root}>
      <SimpleTextField
        name="name"
        label={lang.selectAddingUser}
        placeholder={lang.emailOfUser}
        fullWidth
        onChange={handleChange}
        value={val}
        onClick={() => setOpen(true)}
        autoComplete="off"
        adornmentIcon={!isEmpty(selected) ? (selected.iamAgent ? getPhoto(selected.photo) : userNoImage) : undefined}
        adornmentPos="start"
        InputProps={{ autoComplete: "off" }}
      />
      {data?.length > 0 && open && (
        <ClickAwayListener onClickAway={() => setOpen(false)}>
          <List dense className={classes.list} component={Paper}>
            {data.map((user: IUser) => (
              <ListItem key={user.id} button onClick={() => handleItemClick(user)}>
                <ListItemIcon>
                  <img src={user.iamAgent ? getPhoto(user.photo) : userNoImage} />
                </ListItemIcon>
                <ListItemText
                  primary={user.fullName}
                  secondary={`${user.email} - ${user.iamAgent ? "agent" : "user"}`}
                />
              </ListItem>
            ))}
          </List>
        </ClickAwayListener>
      )}
      <Field name={name} value={selected ? selected.id : undefined} component="input" type="hidden" />
    </div>
  );
};
