import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    root: {
      position: "relative",
      width: "100%",
    },
    list: {
      position: "absolute",
      zIndex: 5,
      maxHeight: 250,
      overflowY: "scroll",
      width: "100%",
      "& .MuiListItemIcon-root": {
        "& img": {
          height: 40,
          width: 40,
          borderRadius: 5,
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
