import { Fade, IconButton, Modal, Paper } from "@material-ui/core";
import { Autocomplete, GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import clsx from "clsx";
import { Button } from "components/shared";
import { useTranslator } from "localization";
import React, { FC, useState } from "react";
import { useStyles } from "./google.map";
import { ILngLat } from "./types";
import CloseIcon from "@material-ui/icons/Close";

// This could be a data-driven prop.
const myPlace = { lat: 39.09366509575983, lng: -94.58751660204751 };

export interface IMap {
  open: boolean;
  onClose(): void;
  onSubmit?(data?: ILngLat): void;
  initial?: ILngLat;
  readonly?: boolean;
}

const Map: FC<IMap> = ({ onClose, open, initial, onSubmit = () => {}, readonly = false }) => {
  const classes = useStyles();
  // The things we need to track in state
  const [markerMap, setMarkerMap] = useState({});
  const [autocomplete, setAutocomplete] = useState<any>(null);
  const [markerPos, setMarkerPos] = useState(initial);
  const [center, setCenter] = useState({ lat: 40.4240778034108, lng: 49.83529604462531 });
  const lang = useTranslator("main");

  // Iterate myPlaces to size, center, and zoom map to contain all markers
  const fitBounds = (map: any) => {
    //@ts-ignore
    const bounds = new window.google.maps.LatLngBounds();
    //bounds.extend(myPlace);
    map.fitBounds(bounds);
  };

  const loadHandler = (map: any) => {
    // Fit map bounds to contain all markers
    //fitBounds(map);
  };

  const markerLoadHandler = (marker: any, place: any) => {
    return setMarkerMap((prevState) => {
      return { ...prevState, [place.id]: marker };
    });
  };

  const onLoad = (autocomplete: any) => {
    setAutocomplete(autocomplete);
  };

  const onPlaceChanged = () => {
    if (autocomplete !== null) {
      console.log(autocomplete?.getPlace());
    } else {
      console.log("Autocomplete is not loaded yet!");
    }
  };

  const handleMapClick = (e: any) => {
    if (!readonly) {
      setMarkerPos(e.latLng.toJSON());
    }
  };

  const renderMap = () => {
    return (
      <Modal open={open} disableScrollLock onClose={onClose} className={classes.modal}>
        <Fade in={open}>
          <Paper className={classes.paper}>
            <IconButton className={classes.closeButton} onClick={() => onClose()}>
              <CloseIcon />
            </IconButton>
            <LoadScript googleMapsApiKey={process.env.REACT_APP_GOOGLE_KEY as string} libraries={["places"]}>
              <GoogleMap
                onLoad={loadHandler}
                onClick={handleMapClick}
                center={center}
                zoom={12}
                mapContainerClassName={clsx(classes.mapContainer, readonly && classes.fullHeight)}
              >
                <Marker position={markerPos} onLoad={(marker) => markerLoadHandler(marker, myPlace)} />
                <Autocomplete
                  bounds={{ east: 180, west: -180, north: 90, south: -90 }}
                  onLoad={onLoad}
                  onPlaceChanged={onPlaceChanged}
                >
                  <input
                    type="text"
                    placeholder="Customized your placeholder"
                    style={{
                      boxSizing: `border-box`,
                      border: `1px solid transparent`,
                      width: `240px`,
                      height: `32px`,
                      padding: `0 12px`,
                      borderRadius: `3px`,
                      boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                      fontSize: `14px`,
                      outline: `none`,
                      textOverflow: `ellipses`,
                      position: "absolute",
                      left: "50%",
                      marginLeft: "-120px",
                    }}
                  />
                </Autocomplete>
              </GoogleMap>
            </LoadScript>
            {!readonly && (
              <div className={classes.buttonContainer}>
                <Button
                  onClick={() => {
                    onSubmit(markerPos);
                    onClose();
                  }}
                >
                  {lang.approve}
                </Button>
              </div>
            )}
          </Paper>
        </Fade>
      </Modal>
    );
  };

  return renderMap();
};

export default Map;
