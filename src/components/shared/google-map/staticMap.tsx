import React, { FC } from "react";
import { ILngLat } from "./types";

export interface IStaticMap {
  position?: ILngLat;
  height?: number;
  width?: number;
  zoom?: number;
  className?: string;
}

export const StaticMap: FC<IStaticMap> = ({ position, height, width, zoom, className }) => {
  return (
    <img
      className={className}
      draggable={false}
      src={`https://maps.googleapis.com/maps/api/staticmap?center=${position?.lat},${position?.lng}&zoom=${
        zoom || 11
      }&size=${width || 378}x${height || 160}&maptype=roadmap&markers=${position?.lat},${position?.lng}&key=${
        process.env.REACT_APP_GOOGLE_KEY
      }`}
    />
  );
};
