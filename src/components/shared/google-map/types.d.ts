export interface ILngLat {
  lat: number;
  lng: number;
}
