import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    modal: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    paper: {
      width: 1000,
      height: 500,
      padding: theme.spacing(2),
      position: "relative",
    },
    mapContainer: {
      height: "90%",
      width: "100%",
      borderRadius: 13,
    },
    fullHeight: {
      height: "100%",
    },
    buttonContainer: {
      display: "flex",
      justifyContent: "center",
      padding: theme.spacing(1.5),
      "& .MuiButton-root": {
        padding: theme.spacing(1, 10),
      },
    },
    closeButton: {
      position: "absolute",
      top: -20,
      right: -20,
      zIndex: 55,
      background: theme.palette.color.white,
      boxShadow: "0 1px 3px rgb(43 45 55 / 10%)",
      padding: theme.spacing(1),
      "&:hover": {
        background: theme.palette.color.white,
        boxShadow: "0 1px 3px rgb(43 45 55 / 10%)",
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {
      paper: {
        position: "static",
        width: "100%",
        height: "100%",
        padding: theme.spacing(5, 0.5, 0.5, 0.5),
      },
      closeButton: {
        top: 0,
        right: 0,
      },
    },
    [theme.breakpoints.down("xs")]: {},
  };
});
