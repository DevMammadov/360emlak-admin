import React, { FC } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./section-header.style";
import { AppBar, Paper } from "@material-ui/core";
import { Button, Select } from "../final-form";
import { SelectData } from "mui-rff";
import { Form } from "react-final-form";

export interface ISectionHeader {
  title?: string | JSX.Element;
  marginBottom?: number;
  renderContent?(): JSX.Element;
  addButton?: boolean;
  onAddClick?(): void;
  filterData?: SelectData[];
  onFilter?(data: any): void;
  initial?: any;
}

export const SectionHeader: FC<ISectionHeader> = ({
  title,
  marginBottom = 16,
  renderContent = () => <></>,
  addButton = false,
  onAddClick = () => {},
  onFilter = () => {},
  initial,
  filterData,
}) => {
  const lang = useTranslator("main");
  const classes = useStyles();

  const renderAddButton = () => {
    return (
      <Button for="add" onClick={() => onAddClick()}>
        {lang.add}
      </Button>
    );
  };

  return (
    <Paper className={classes.root} style={{ marginBottom }}>
      {addButton ? renderAddButton() : <h2>{title}</h2>}
      <div>{renderContent()}</div>
      <div>
        {filterData && (
          <Form
            onSubmit={(data) => onFilter(data.filter)}
            initialValues={initial || { filter: 0 }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Select name="filter" data={filterData} onChange={handleSubmit} className={classes.filterSelect} />
              </form>
            )}
          />
        )}
      </div>
    </Paper>
  );
};
