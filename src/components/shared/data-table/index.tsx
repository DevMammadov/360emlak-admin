import React, { FC, ExoticComponent, forwardRef } from "react";
import { useStyles } from "./data-table.style";
import { useTranslator } from "localization";
import MaterialTable, { Icons, MaterialTableProps } from "material-table";
import { Button } from "../final-form";
import SearchIcon from "@material-ui/icons/Search";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import ClearIcon from "@material-ui/icons/Clear";

export interface IDataTable extends MaterialTableProps<any> {
  addButton?: boolean;
  onAddClick?(): void;
}

export const DataTable: FC<IDataTable> = ({ addButton = true, onAddClick = () => {}, ...props }) => {
  const lang = useTranslator("main");
  const classes = useStyles();

  const renderAddButton = () => {
    return <Button onClick={() => onAddClick()}>{lang.add}</Button>;
  };

  const tableIcons: Icons = {
    PreviousPage: forwardRef((props, ref) => <ChevronLeftIcon {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRightIcon {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <SearchIcon {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpwardIcon {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <ClearIcon {...props} ref={ref} />),
  };

  return (
    <div className={classes.root}>
      <MaterialTable icons={tableIcons} title={addButton ? renderAddButton() : props.title} {...props} />
    </div>
  );
};
