import React, { FC } from "react";
import { Box, Typography, BoxProps } from "@material-ui/core";

interface ITabPanel {
  value: any;
  index: any;
  boxProps?: BoxProps;
}

export const TabPanel: FC<ITabPanel> = ({ children, value, index, boxProps, ...other }: any) => {
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box py={3} {...boxProps}>
          {children}
        </Box>
      )}
    </Typography>
  );
};
