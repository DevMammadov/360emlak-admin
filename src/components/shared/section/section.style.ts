import { grey } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    section: {},
    fullHeight: {
      height: "100%",
    },
    imgContainer: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    compact: {
      width: 1078,
    },
    privateSection: {
      height: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      fontSize: 18,
      fontWeight: "bold",
      "& > div": {
        textAlign: "center",
        "& .MuiButton-root": {
          marginTop: theme.spacing(2),
          padding: theme.spacing(1, 6),
          "& .MuiSvgIcon-root": {
            marginRight: 5,
          },
        },
      },
    },
    notFountContainer: {
      minHeight: 300,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      fontSize: 18,
      fontWeight: "bold",
      "& .MuiSvgIcon-root": {
        fontSize: 90,
        marginBottom: 20,
        color: grey[500],
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {
      compact: {
        width: "auto",
      },
    },
    [theme.breakpoints.down("xs")]: {},
  };
});
