import clsx from "clsx";
import { useTranslator } from "localization";
import React, { FC } from "react";
import { useStyles } from "./section.style";
import SentimentDissatisfiedIcon from "@material-ui/icons/SentimentDissatisfied";
import { CircularProgress } from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";

export interface ISection {
  loading: boolean;
  className?: string;
  isPrivate?: boolean;
  privateText?: string;
  compact?: boolean;
  notFound?: boolean;
  notFoundText?: string;
  fullHeight?: boolean;
}

export const Section: FC<ISection> = ({
  loading,
  children,
  className,
  isPrivate,
  privateText,
  compact = false,
  notFound = false,
  fullHeight = true,
  notFoundText,
}) => {
  const lang = useTranslator("alerts");
  const classes = useStyles();

  const renderPrivate = () => {
    if (notFound) {
      return (
        <div className={classes.notFountContainer}>
          <SentimentDissatisfiedIcon />
          <div> {notFoundText} </div>
        </div>
      );
    } else if (isPrivate) {
      return (
        <section className={classes.privateSection}>
          <LockIcon />
          <div>
            <div>{privateText}</div>
          </div>
        </section>
      );
    } else {
      return <>{children}</>;
    }
  };

  return (
    <section className={clsx(classes.section, fullHeight && classes.fullHeight, compact && classes.compact, className)}>
      {}

      {loading ? (
        renderPrivate()
      ) : (
        <div className={classes.imgContainer}>
          <CircularProgress />
        </div>
      )}
    </section>
  );
};
