import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    label: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      marginBottom: theme.spacing(2),
    },
    infomodal: {
      position: "relative",
      width: 800,
      padding: theme.spacing(2, 6),
      "& h3": {
        textAlign: "center",
      },
      "& .MuiCollapse-container": {
        marginBottom: theme.spacing(2),
      },
    },
    modal: {
      position: "relative",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
