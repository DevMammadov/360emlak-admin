import React, { FC } from "react";
import { useTranslator } from "localization";
import JoditEditor, { JoditProps } from "jodit-react";
import { editorConfig } from "./config";
import { Backdrop, Fade, Modal, Paper } from "@material-ui/core";
import { ActionButton, Alert, Button } from "..";
import { useStyles } from "./jodit.style";
import InfoIcon from "@material-ui/icons/Info";

export interface IJodit extends JoditProps {
  label?: string;
}

export const Jodit: FC<IJodit> = ({ label, ...props }) => {
  const [open, setOpen] = React.useState(false);
  const lang = useTranslator("main");
  const classes = useStyles();

  return (
    <div>
      <div className={classes.label}>
        <label style={{ marginBottom: 8, display: "block" }}> {label} </label>
        <Button icon={InfoIcon} variant="text" onClick={() => setOpen(!open)}>
          {lang.readLaws}
        </Button>
      </div>

      {
        //@ts-ignore
        <JoditEditor config={editorConfig} {...props} />
      }
      <Modal
        className={classes.modal}
        open={open}
        onClose={() => setOpen(!open)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Paper className={classes.infomodal}>
            <ActionButton variant="close" onClick={() => setOpen(!open)} />
            <h3>Editor qaydaları</h3>
            <Alert title="Qeyd" severity="warning">
              <div>1. Editorda verdiyiniz dizayn saytda başqa cür tənzimlənib</div>
              <div>
                2. Əgər Html bilikləriniz yoxdursa editorun kod bölməsinə keçib kodlara toxunmayın. əks halda səhifə
                görsənməyəcək
              </div>
            </Alert>
            <Alert title="Qeyd">
              Yazıların arxasına rəng verilməsinə saytda qadağa qoyulub, yazılara rəng verərkən çalışın saytdakı rəng
              tonlarından istifadə edin, rəng paletindəki ikinci sətir saytda istifadə edilən rənglərdir
            </Alert>
            <Alert title="Qeyd">
              Şəkil yerləşdirmək üçün əvvəlcə FTP bölməsindən şəklinizi bazaya yükləyin sonra, verilən linki şəkil kimi
              istifadə edə bilərsiniz.
            </Alert>
            <Alert icon={false} title="Qeyd">
              <b>Şəkil ölşüsü:</b> <br /> Editora yerləşdirilən şəklin enini verməkçün aşağıdakı açar sözlərdən birini
              Alternative Text yerinə yazmalısız (medium seçilidir)
              <br />
              <div>small: 30%</div>
              <div>medium: 50%</div>
              <div>big: 100%</div>
            </Alert>
          </Paper>
        </Fade>
      </Modal>
    </div>
  );
};
