import anyLang from "./lang/az.json";
type Page = keyof typeof anyLang;

export const translator = (page: Page, other?: Page[]) => {
  const lang = localStorage.getItem("lang") || "az";
  const langObj = require(`./lang/${lang}.json`);

  const _getPageLang = (page?: Page) => {
    if (page && page.toLowerCase() === "main") {
      return langObj["main"];
    } else if (page && page.toLowerCase() !== "main" && Object.keys(langObj).includes(page)) {
      return { ...langObj["main"], ...langObj[page as keyof typeof langObj] };
    } else {
      let allLangs = {};
      Object.keys(langObj).map((key: string) => (allLangs = { ...allLangs, ...langObj[key as keyof typeof langObj] }));
      return allLangs;
    }
  };

  const _getCombinedPages = (page?: Page, other?: Page[] | undefined) => {
    let pageLang = _getPageLang(page);

    if (other && other.length > 0) {
      other.map((pg: Page) => {
        pageLang = { ...pageLang, ...langObj[pg as keyof typeof langObj] };
        return pageLang;
      });
    }
    return pageLang as any;
  };

  return _getCombinedPages(page, other);
};
