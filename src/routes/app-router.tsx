import { PrivateRoute } from "components/shared";
import React, { lazy } from "react";
import { Route, Switch } from "react-router-dom";

const Main = lazy(() => import("views").then((module) => ({ default: module.Main })));
const Announces = lazy(() => import("views").then((module) => ({ default: module.Announces })));
const AddAnnounce = lazy(() => import("views").then((module) => ({ default: module.AddAnnounce })));
const Agencies = lazy(() => import("views").then((module) => ({ default: module.Agencies })));
const AddAgency = lazy(() => import("views").then((module) => ({ default: module.AddAgency })));
const Complexes = lazy(() => import("views").then((module) => ({ default: module.Complexes })));
const AddComplex = lazy(() => import("views").then((module) => ({ default: module.AddComplex })));
const Users = lazy(() => import("views").then((module) => ({ default: module.Users })));
const AddUpdateUsers = lazy(() => import("views").then((module) => ({ default: module.AddUpdateUsers })));
const Info = lazy(() => import("views").then((module) => ({ default: module.Info })));
const Baner = lazy(() => import("views").then((module) => ({ default: module.Baner })));
const AddUpdateBaner = lazy(() => import("views").then((module) => ({ default: module.AddUpdateBaner })));
const SearchHome = lazy(() => import("views").then((module) => ({ default: module.SearchHome })));
const HomeRequest = lazy(() => import("views").then((module) => ({ default: module.Request })));
const Laws = lazy(() => import("views").then((module) => ({ default: module.Laws })));
const Album = lazy(() => import("views").then((module) => ({ default: module.Album })));

const NotFound = () => {
  return <div>Not Found</div>;
};

const AppRouter = () => {
  return (
    <Switch>
      <PrivateRoute exact path="/" component={Main} />
      <PrivateRoute exact path="/album" component={Album} />
      <PrivateRoute exact path="/home-request/:id" component={HomeRequest} />
      <PrivateRoute exact path="/home-requests" component={SearchHome} />
      <PrivateRoute exact path="/baners/edit/:id" component={AddUpdateBaner} />
      <PrivateRoute exact path="/baners/add" component={AddUpdateBaner} />
      <PrivateRoute exact path="/baners" component={Baner} />
      <PrivateRoute exact path="/information" component={Info} />
      <PrivateRoute exact path="/users/edit/:id" component={AddUpdateUsers} />
      <PrivateRoute exact path="/users" component={Users} />
      <PrivateRoute exact path="/complexes/add" component={AddComplex} />
      <PrivateRoute exact path="/complexes/edit/:id" component={AddComplex} />
      <PrivateRoute exact path="/complexes" component={Complexes} />
      <PrivateRoute exact path="/agencies" component={Agencies} />
      <PrivateRoute exact path="/agencies/edit/:id" component={AddAgency} />
      <PrivateRoute exact path="/agencies/add" component={AddAgency} />
      <PrivateRoute exact path="/announces/edit/:id" component={AddAnnounce} />
      <PrivateRoute exact path="/announces/add" component={AddAnnounce} />
      <PrivateRoute exact path="/announces" component={Announces} />
      <PrivateRoute path="/laws/increase-balance" component={Laws} />
      <PrivateRoute path="/laws/announce-law" component={Laws} />
      <PrivateRoute path="/laws/advertisment-law" component={Laws} />
      <PrivateRoute path="/laws/user-agreement" component={Laws} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default AppRouter;
