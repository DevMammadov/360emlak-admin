import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import App from "layout/app";
import { PrivateRoute } from "components/shared";

const Login = lazy(() => import("views").then((module) => ({ default: module.Login })));

const Routes = ({ history }: any) => {
  return (
    <Router>
      <Suspense fallback={<section>loading</section>}>
        <Switch>
          <Route path="/login" component={Login} />
          <PrivateRoute path="/" component={App} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default Routes;
