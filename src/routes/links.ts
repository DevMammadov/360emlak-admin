export const links = {
  login: {
    login: "/login",
  },
  announces: {
    baseUrl: "/announces",
    add: "/announces/add",
    edit: "/announces/edit",
  },
  agencies: {
    baseUrl: "/agencies",
    add: "/agencies/add",
    edit: "/agencies/edit",
  },
  complexes: {
    baseUrl: "/complexes",
    add: "/complexes/add",
    edit: "/complexes/edit",
  },
  users: {
    baseUrl: "/users",
    add: "/users/add",
    edit: "/users/edit",
  },
  executiveBoard: {
    baseUrl: "/admin-users",
    add: "/admin-users/add",
    edit: "/admin-users/edit",
  },
  info: {
    baseUrl: "/information",
  },
  baners: {
    baseUrl: "/baners",
    add: "/baners/add",
    edit: "/baners/edit",
  },
  homeRequests: {
    baseUrl: "/home-requests",
    view: "/home-request",
  },
  album: {
    baseUrl: "/album",
  },
  laws: {
    increaseBalance: "laws/increase-balance",
    announceLaw: "laws/announce-law",
    advertismentLaw: "laws/advertisment-law",
    userAgreement: "laws/user-agreement",
  },
};
