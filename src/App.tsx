import React from "react";
import "./App.css";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import Routes from "routes/routes";
import { ToastContainer } from "react-toastify";
import theme from "theme/theme";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Routes />
      <ToastContainer autoClose={3000} position="top-center" />
    </ThemeProvider>
  );
}

export default App;
