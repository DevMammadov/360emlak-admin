import { axiosAdmin } from "helpers/axios";
import { IUserEdit, IUserRequest } from "views/users/types";

export default class UsersApi {
  static getUserList = (email?: string) => {
    return axiosAdmin.get("user/search", { params: { email } }).then((payload) => payload?.data);
  };

  static getUseById = (id?: string) => {
    return axiosAdmin.get(`user/${id}`).then((payload) => payload?.data);
  };

  static getFullUserById = (id?: string) => {
    return axiosAdmin.get(`user/${id}/full`).then((payload) => payload?.data);
  };

  static blockUser = (id?: string, lockoutTo?: string) => {
    return axiosAdmin.get(`user/ban/${id}`, { params: { lockoutTo } }).then((payload) => payload?.data);
  };

  static getUsers = (data: IUserRequest) => {
    return axiosAdmin.get("user", { params: { ...data } }).then((payload) => payload?.data);
  };

  static edit = (id: string, data: IUserEdit) => {
    return axiosAdmin.put(`user/${id}`, data).then((payload) => payload?.data);
  };

  static delete = (id: string) => {
    return axiosAdmin.delete(`user/${id}`).then((payload) => payload?.data);
  };

  static changeRole = (id: string, role: number) => {
    return axiosAdmin.get(`user/role/${id}`, { params: { role } }).then((payload) => payload?.data);
  };

  static activeUser = (id?: string) => {
    return axiosAdmin.get(`user/activate/${id}`).then((payload) => payload?.data);
  };
}
