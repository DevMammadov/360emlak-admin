import { axiosMain, axiosAdmin } from "helpers/axios";
import { ILimitOfs } from "types";
import { ILoginRequest } from "views/login/types";

export default class LoginApi {
  static login = (data: ILoginRequest) => {
    return axiosAdmin.post("user/login", data).then((payload) => payload?.data);
  };
}
