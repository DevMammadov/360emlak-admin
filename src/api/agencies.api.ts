import { axiosMain, axiosAdmin } from "helpers/axios";
import { ILimitOfs } from "types";
import { AgenciesResponse } from "views/agencies/types";

export default class AgenciesApi {
  static getAgencies = (data: AgenciesResponse) => {
    return axiosAdmin.get("agent", { params: data }).then((payload) => payload?.data);
  };

  static addAgency = (data: FormData) => {
    return axiosAdmin.post("agent", data);
  };

  static getByIdAgency = (id: string) => {
    return axiosAdmin.get(`agent/${id}`);
  };

  static updateAgency = (data: FormData, userId?: string) => {
    return axiosAdmin.put("agent", data, { params: { userId } });
  };

  static removeAgent = (id: string) => {
    return axiosAdmin.delete(`agent/${id}`);
  };

  static searchAgent = (name: string) => {
    return axiosAdmin.get(`agent`, { params: { name } });
  };
}
