import { axiosAdmin } from "helpers/axios";
import { ILawsReq } from "views";

export default class LawsApi {
  static addLaw = (data: ILawsReq) => {
    return axiosAdmin.post("rules", data).then((payload) => payload?.data);
  };

  static updateLaw = (id: string, data: ILawsReq) => {
    return axiosAdmin.put(`rules/${id}`, data).then((payload) => payload?.data);
  };

  static getLaw = (type: string) => {
    return axiosAdmin.get("rules", { params: { type } }).then((payload) => payload?.data);
  };
}
