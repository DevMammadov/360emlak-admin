import { axiosMain, axiosAdmin } from "helpers/axios";
import { ILimitOfs } from "types";
import { AgenciesResponse } from "views/agencies/types";
import { IAlbumRequest } from "views/album/types";

export default class AlbumApi {
  static upload = (photo: FormData) => {
    return axiosAdmin.post("user/upload", photo).then((payload) => payload?.data);
  };

  static getPhotos = (data: IAlbumRequest) => {
    return axiosAdmin.get("user/photos", { params: data }).then((payload) => payload?.data);
  };
}
