import { axiosAdmin, axiosMatterport } from "helpers/axios";
import { IUserEdit, IUserRequest } from "views/users/types";
import axios from "axios";

export default class MatterportApi {
  static getModels = () => {
    return axiosMatterport({
      url: "",
      method: "post",
      data: {
        query: `models(query: "*") {
                totalResults
                 results {
                    id
                     name
                  description
                }
         }`,
      },
    });
  };
}
