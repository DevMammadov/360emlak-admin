import { axiosAdmin, axiosMain } from "helpers/axios";
import { ILimitOfs } from "types";
import { IBaner } from "views";
import { IAddReqModel, ISearchAdd } from "views/announces/types";

export default class BanerApi {
  static getBaners = (data?: ILimitOfs) => {
    return axiosAdmin.get("banner", { params: { ...data } }).then((payload) => payload?.data);
  };

  static getById = (id: string) => {
    return axiosAdmin.get(`banner/${id}`).then((payload) => payload?.data);
  };

  static add = (data: IBaner) => {
    return axiosAdmin.post("banner", data).then((payload) => payload?.data);
  };

  static update = (id: number, data: IBaner) => {
    return axiosAdmin.put(`banner/${id}`, data).then((payload) => payload?.data);
  };

  static remove = (id: number) => {
    return axiosAdmin.delete(`banner/${id}`).then((payload) => payload?.data);
  };
}
