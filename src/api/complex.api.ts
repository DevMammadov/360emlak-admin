import axios from "axios";
import { axiosAdmin, axiosMain } from "helpers/axios";
import { IAddReqModel, ISearchAdd } from "views/announces/types";
import { IComplexResponse } from "views/comlpexes/types";

export default class AddsApi {
  static add = (data: FormData) => {
    return axiosAdmin.post("apartment", data);
  };

  static getById = (id: string) => {
    return axiosAdmin.get(`apartment/${id}`);
  };

  static getAll = (data: IComplexResponse) => {
    return axiosAdmin.get(`apartment`, { params: { ...data } });
  };

  static remove = (id: string) => {
    return axiosAdmin.delete(`apartment/${id}`);
  };

  static update = (id: string, data: FormData) => {
    return axiosAdmin.put(`apartment/${id}`, data);
  };

  static removeSliderPhoto = (id: string, file: string) => {
    return axiosAdmin.delete(`apartment/${id}/${file}`);
  };
}
