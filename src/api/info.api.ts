import { axiosAdmin } from "helpers/axios";

export default class InfoApi {
  static getInfo = () => {
    return axiosAdmin.get("about").then((payload) => payload?.data);
  };

  static add = (data: FormData) => {
    return axiosAdmin.post("about", data).then((payload) => payload?.data);
  };

  static update = (id: number, data: FormData) => {
    return axiosAdmin.put(`about/${id}`, data).then((payload) => payload?.data);
  };
}
