import { axiosAdmin } from "helpers/axios";
import { IRequestTable, ISearchRequest } from "views/search-home/types";
import { IUserEdit, IUserRequest } from "views/users/types";

export default class RequestApi {
  static getRequest = (id: number) => {
    return axiosAdmin.get(`interest/${id}`);
  };

  static getRequests = (data?: IRequestTable) => {
    return axiosAdmin.get(`interest`, { params: { ...data } });
  };

  static removeRequest = (id: string) => {
    return axiosAdmin.delete(`interest/${id}`);
  };
}
