import { axiosMain, axiosAdmin } from "helpers/axios";
import { ILimitOfs } from "types";
import { ILoginRequest } from "views/login/types";

export default class MainApi {
  static getMonthly = () => {
    return axiosAdmin.get("statistics/monthly");
  };

  static getLastUsers = () => {
    return axiosAdmin.get("statistics/last-user");
  };

  static getCounts = () => {
    return axiosAdmin.get("statistics/counts");
  };

  static getAnnounces = () => {
    return axiosAdmin.get("statistics/last-announce");
  };
}
