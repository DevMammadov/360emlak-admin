import { axiosAdmin, axiosMain } from "helpers/axios";
import { IAddReqModel, ISearchAdd } from "views/announces/types";

export default class AddsApi {
  static getAdds = (filter?: ISearchAdd) => {
    const _filter = filter ? { params: filter } : {};
    return axiosAdmin.get("announce", _filter).then((payload) => payload?.data);
  };

  static newAdd = (data?: FormData) => {
    return axiosAdmin.post("announce", data).then((payload) => payload?.data);
  };

  static update = (id: number, data?: FormData) => {
    return axiosAdmin.put(`announce/${id}`, data).then((payload) => payload?.data);
  };

  static getAddById = (id: string) => {
    return axiosAdmin.get(`announce/${id}`);
  };

  static removeAdd = (id?: string) => {
    return axiosAdmin.delete(`announce/${id}`).then((payload) => payload?.data);
  };

  static accept = (id?: number) => {
    return axiosAdmin.get(`announce/accept/${id}`).then((payload) => payload?.data);
  };

  static reject = (id?: number) => {
    return axiosAdmin.get(`announce/reject/${id}`).then((payload) => payload?.data);
  };

  static removePhoto = ({ id, file }: { id: number; file: string }) => {
    return axiosAdmin.delete(`announce/${id}/${file}`);
  };
}
