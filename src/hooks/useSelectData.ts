import Json from "localization/lang/az.json";
import { SelectData } from "mui-rff";
import { useUser } from "./useUser";

export const useSelectData = () => {
  const currentUser = useUser();
  const langObj = require(`../localization/lang/${currentUser.lang}.json`) as typeof Json;

  const getData = (key: keyof typeof langObj) => {
    let data: SelectData[] = [];
    const piece = langObj[key];

    Object.keys(piece).map((key) => {
      data.push({ label: piece[key as keyof typeof piece], value: key });
    });

    return data;
  };

  const getNumberData = (count: number) => {
    let data: SelectData[] = [];
    for (let i = 1; i <= count; i++) {
      data.push({ label: i.toString(), value: i });
    }
    return data;
  };

  const getSettlements = (regionId: string = "0") => {
    const st = langObj["settlements"][regionId as keyof typeof langObj["settlements"]];
    let data: SelectData[] = [];
    Object.keys(st || {}).map((key) => {
      data.push({ label: st[key as keyof typeof st], value: key });
    });
    return data;
  };

  const allSettlements = () => {
    let stl = [];
    for (let region of getData("bakuRegions")) {
      for (let settlement of getSettlements(region.value as string)) {
        stl.push(settlement);
      }
    }
    return stl;
  };

  return {
    propType: getData("propTypes"),
    city: getData("cities"),
    metro: getData("metroStations"),
    weekDays: getData("weekDays"),
    salesType: getData("salesType"),
    mounth: getData("mounth"),
    roles: getData("roles"),
    rooms: getNumberData(20),
    region: getData("bakuRegions"),
    settlements: (regionId: string) => getSettlements(regionId),
    allSettlements: allSettlements(),
    filterFields: getData("filterFields"),
    numberData: (num: number) => getNumberData(num),
    AddFilter: getData("AddFilter"),
    buildingType: getData("buildingType"),
  };
};
