import { FieldState } from "final-form";
import { useTranslator, replace } from "localization";

interface ValidatorProps {
  value: any;
  allValues: object;
  meta?: FieldState<any> | undefined;
}

export const useValidators = () => {
  const lang = useTranslator("formAlerts");

  const required = (value: any) => (value?.toString().trim().length ? undefined : lang.required);
  const requiredIf = (condition: boolean) => (value: any) => {
    return condition ? (value?.toString().trim().length ? undefined : lang.required) : undefined;
  };

  const requiredMessageLess = (value: any) => (value?.toString().trim().length ? undefined : " ");
  const maxLength = (length: number) => (value: any) =>
    value && value.toString().length < length ? undefined : replace(lang.maxLength, length.toString());
  const minLength = (length: number) => (value: any) =>
    value && value.toString().length > length ? undefined : replace(lang.minLength, length.toString());
  const lengthRange = (min: number, max: number) => (value: any) => {
    if (value.toString().length > max) return replace(lang.maxLength, max.toString());
    else if (value.toString().length < min) return replace(lang.minLength, min.toString());
  };
  const email = (value: any) =>
    !value?.toString().trim() || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value?.toString().trim())
      ? undefined
      : lang.invalidEmail;

  const validDate = (value: any) => (isNaN(new Date(value).getDate()) && !!value ? lang.invalidDateMessage : undefined);

  const matches = (regex: RegExp, errorText?: string) => (value: any) =>
    regex.test(value) ? undefined : errorText || `invalid format`;
  const number = (value: any) => (value && isNaN(Number(value)) ? lang.onlyNumber : undefined);
  const test = (condition: boolean, message: string) => (value: any) => condition ? undefined : message;

  // For Project
  const passportNumberRegex = (value: any) =>
    value && !/^(?!^0+$)[a-zA-Z0-9]{5,20}$/.test(value) ? lang.invalidPassportRegex : undefined;
  const passwordRegex = (value: any) =>
    value && !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$/.test(value) ? lang.invalidPasswordRegex : undefined;
  const passwordMatch = (passName?: string) => (value: any, allValues: any) => {
    const password = passName ? allValues[passName] : allValues.password;
    return value && value !== password ? lang.passwordsMustMatch : undefined;
  };

  return {
    required,
    requiredIf,
    requiredMessageLess,
    maxLength,
    minLength,
    lengthRange,
    email,
    validDate,
    passportNumberRegex,
    matches,
    passwordRegex,
    passwordMatch,
    number,
    test,
  };
};
