//import { useSelector, useDispatch } from "react-redux";
import { getStorage, removeStorage, decodedToken } from "helpers/storage";
import { decode } from "jsonwebtoken";
import { useParams } from "react-router";
import { IToken } from "views/login/types";

export interface ICurrentUser {
  online: boolean;
  logout(): void;
  userId?: string;
  token?: string;
  roles?: "admin" | "moderator" | "user";
  lang?: string;
  iAmAgent: boolean;
}

export const useUser = () => {
  const storage = getStorage();
  const params: any = useParams();
  const token = storage?.token || undefined;

  let currentUser: ICurrentUser = {
    online: !!token,
    iAmAgent: Boolean(decodedToken()?.iamAgent),
    userId: decodedToken()?.userId,
    roles: decodedToken()?.roles,
    token,
    lang: "az",
    logout: () => {
      removeStorage();
      sessionStorage.clear();
    },
  };

  return currentUser;
};
