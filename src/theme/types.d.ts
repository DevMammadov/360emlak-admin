declare module "@material-ui/core/styles/createPalette" {
  interface Palette {
    color: IColor;
    gradients: IGradients;
  }

  interface PaletteOptions {
    color?: IColor;
    gradients?: IGradients;
  }
}

export interface IColor {
  pink: string;
  borderColor: string;
  gray: string;
  blue: string;
  green?: string;
  red?: string;
  ping?: string;
  lightGreen?: string;
  black?: string;
  white?: string;
  orange?: string;
  main?: string;
  lightOrange?: string;
}
