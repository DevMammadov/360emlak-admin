import { createMuiTheme } from "@material-ui/core/styles";
import { grey, red } from "@material-ui/core/colors";

// A custom theme for this app
const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      "@global": {},
    },
    MuiPaper: {
      root: {
        boxShadow: "0 1px 3px rgb(43 45 55 / 10%)  !important",
      },
    },
    MuiButton: {
      root: {
        textTransform: "capitalize",
      },
    },
  },
  props: {
    MuiSelect: {},
  },
  palette: {
    warning: {
      main: "#FDB450",
      light: "#FDC477",
      dark: "#FC9305",
    },
    error: {
      main: red[800],
      light: red[600],
    },
    color: {
      pink: "#f78ca0",
      borderColor: grey[400],
      gray: "#dfe9f3",
      blue: "#59ABFF",
      red: red[400],
      ping: "#f78ca0",
      green: "#0ba360",
      lightGreen: "#DFFFCD",
      black: grey[800],
      white: "white",
      orange: "#ffce34",
      main: "#3f51b5",
      lightOrange: "#ffe364",
    },
  },
});

export default theme;
