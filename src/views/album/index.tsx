import { Grid, Paper } from "@material-ui/core";
import AlbumApi from "api/album.api";
import { Button, FileField, Section, SectionHeader } from "components/shared";
import copy from "copy-to-clipboard";
import { toastMessage } from "helpers/functions";
import { toFormData } from "helpers/to-form-data";
import { useTranslator } from "localization";
import React, { FC, useEffect, useState } from "react";
import { Form } from "react-final-form";
import { toast } from "react-toastify";
import { useStyles } from "./album.style";
import { FolderName } from "./types";

export interface IAlbum {}

export const Album: FC<IAlbum> = () => {
  const lang = useTranslator("album", ["alerts"]);
  const [folder, setFolder] = useState<FolderName>("others");
  const [photos, setPhotos] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);
  const [changed, setchanged] = useState(false);
  const classes = useStyles();
  const limit = 10;

  useEffect(() => {
    AlbumApi.getPhotos({ folder, offset: 0, limit }).then((p) => {
      setPhotos(p.data);
      setLoading(false);
    });
  }, [folder, changed]);

  const handleFormSubmit = (data: any) => {
    setLoading(true);
    AlbumApi.upload(toFormData(data)).then((p) => {
      copy(p.data.fileName);
      toastMessage(p, lang.linkCopiedToClipboard);
      setchanged(!changed);
    });
  };

  const sectionContent = () => {
    return (
      <div>
        <Form
          onSubmit={handleFormSubmit}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit} className={classes.form}>
              <FileField name="photo" label={lang.selectPhoto} />
              <Button type="submit"> {lang.upload} </Button>
            </form>
          )}
        />
      </div>
    );
  };

  return (
    <Section loading={true}>
      <SectionHeader
        renderContent={sectionContent}
        filterData={[
          { label: "others", value: "others" },
          { label: "agency", value: "agency" },
          { label: "announce", value: "announce" },
          { label: "banners", value: "banners" },
          { label: "complex", value: "complex" },
        ]}
        initial={{ filter: "others" }}
        onFilter={(f) => setFolder(f)}
        title={lang.photoAlbum}
      />
      <Grid container>
        <Grid item xs={12} component={Paper} className={classes.imageContainer}>
          {photos.map((p) => (
            <Button
              variant="text"
              onClick={() => {
                copy(p, { debug: true, message: "kopyala" });
                toast.success(lang.copied, { autoClose: 1500, toastId: "1" });
              }}
            >
              <img src={p} alt={p} />
            </Button>
          ))}
        </Grid>
      </Grid>
    </Section>
  );
};
