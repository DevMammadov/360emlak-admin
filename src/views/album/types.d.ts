export type FolderName = "others" | "agency" | "announce" | "banners" | "complex";

export interface IAlbumRequest {
  folder: FolderName;
  offset?: number;
  limit?: number;
}
