import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    form: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      width: 300,
    },
    imageContainer: {
      display: "flex",
      flexWrap: "wrap",
      "& img": {
        height: 150,
        width: 150,
        objectFit: "cover",
        background: theme.palette.color.gray,
      },
      "& div": {
        padding: theme.spacing(0.5),
        margin: theme.spacing(1),
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
