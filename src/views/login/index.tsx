import React, { FC, useEffect } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./login.style";
import { Grid, Paper } from "@material-ui/core";
import { Form } from "react-final-form";
import { Button, TextField } from "components/shared";
import { useValidators } from "hooks/useValidators";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import LockIcon from "@material-ui/icons/Lock";
//@ts-ignore
import Typical from "react-typical";
import LoginApi from "api/login.api";
import { ILoginRequest } from "./types";
import { setToStorage } from "helpers/storage";
import { useUser } from "hooks/useUser";
import { useHistory } from "react-router";

export interface ILogin {}

export const Login: FC<ILogin> = ({}) => {
  const lang = useTranslator("login");
  const classes = useStyles();
  const { required } = useValidators();
  const currentUser = useUser();
  const history = useHistory();

  const handleFormSubmit = (data: ILoginRequest) => {
    LoginApi.login(data).then((payload) => {
      if (payload?.statusCode === 200) {
        setToStorage("token", payload?.data?.token);
        window.location.reload();
      }
    });
  };

  useEffect(() => {
    if (currentUser.online) {
      history.push("/");
    }
  }, [currentUser.online]);

  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <Typical steps={[lang.hello, 1000, lang.welcomeText, 2000, lang.welcome, 1000]} loop={Infinity} wrapper="h1" />
      </div>
      <Paper>
        <Form
          onSubmit={handleFormSubmit}
          render={({ handleSubmit, form, values }) => (
            <form onSubmit={handleSubmit} className={classes.form}>
              <Grid container className={classes.formPaper}>
                <Grid item xs={12}>
                  <TextField adornmentIcon={AlternateEmailIcon} validate={required} name="email" label={lang.email} />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    adornmentIcon={LockIcon}
                    type="password"
                    validate={required}
                    name="password"
                    label={lang.password}
                  />
                </Grid>
                <Grid item xs={12} className={classes.buttonContainer}>
                  <Button type="submit"> {lang.send} </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </Paper>
    </div>
  );
};
