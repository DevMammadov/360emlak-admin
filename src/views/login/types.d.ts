export interface IToken {
  iamAgent: string;
  userId: string;
  email: string;
  roles: "admin" | "moderator" | "user";
  loginType: string;
  aud: string;
  exp: string;
  iss: string;
  jti: string;
}

export interface ILoginRequest {
  email: string;
  password: string;
}
