import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    title: {
      "& h1": {
        margin: 0,
        marginBottom: theme.spacing(2),
      },
    },
    root: {
      height: "100%",
      width: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      "& > .MuiPaper-root": {
        width: 800,
        padding: theme.spacing(2),
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        "background-image": "linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%)",
      },
    },
    formPaper: {
      width: 500,
      "& .MuiGrid-root": {
        marginBottom: theme.spacing(3),
      },
    },
    buttonContainer: {
      display: "flex",
      justifyContent: "center",
      "& .MuiButtonBase-root": {},
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
