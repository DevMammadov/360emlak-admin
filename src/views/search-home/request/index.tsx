import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./request.style";
import { List, SectionHeader, ITableWordpair } from "components/shared";
import { ISearchHomeRequest } from "../types";
import { Paper } from "@material-ui/core";
import { useParams } from "react-router-dom";
import RequestApi from "api/search-home";
import { useSelectData } from "hooks/useSelectData";
import { removeUndefined } from "helpers/functions";

export interface IRequest {}

export const Request: FC<IRequest> = ({}) => {
  const lang = useTranslator("findHome");
  const [parameters, setParams] = useState<ISearchHomeRequest>({} as ISearchHomeRequest);
  const classes = useStyles();
  const params: any = useParams();
  const selectData = useSelectData();
  const arrayFields = ["metro", "region", "settlement", "salesType", "buildingType", "propType", "city"];

  useEffect(() => {
    if (params.id) {
      RequestApi.getRequest(params.id).then((payload) => {
        setParams(payload?.data?.data);
      });
    }
  }, [params.id]);

  const renderArrField = (key: keyof typeof arrayFields, val: any) => {
    const dataObj = {
      ...selectData,
      settlement: selectData["allSettlements"],
    };
    const fields = val.split(",");
    const arr = dataObj[key as keyof typeof dataObj] as any[];
    return arr
      ?.filter((e) => fields.includes(e.value))
      ?.map((e) => e.label)
      ?.join(", ");
  };

  const getVal = (key: string, val: any) => {
    if (arrayFields.includes(key)) {
      return renderArrField(key as keyof typeof arrayFields, val);
    } else if (val === true) {
      return "✔️";
    } else if (key.toLocaleLowerCase().includes("price")) {
      return `${val} ${lang.azn}`;
    } else if (key.toLocaleLowerCase().includes("area")) {
      return `${val} ${lang.m2}`;
    } else {
      return val?.toString();
    }
  };

  const getListData = (list: ISearchHomeRequest) => {
    let data: ITableWordpair[] = [];
    for (let key in removeUndefined(list)) {
      const value = list[key as keyof ISearchHomeRequest];
      data.push({ label: lang[key], value: getVal(key, value) });
    }
    return data;
  };

  return (
    <section>
      <SectionHeader title={`${parameters.name} ${lang.searching}`} />
      <Paper className={classes.listContainer}>
        <List checkValue paperRow data={getListData(parameters)} />
      </Paper>
    </section>
  );
};
