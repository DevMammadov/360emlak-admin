import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    listContainer: {
      display: "flex",
      justifyContent: "center",
      padding: theme.spacing(4, 2),
      "& .pair-list-value-row": {
        maxWidth: 500,
        whiteSpace: "break-spaces",
      },
      "& .pair-list-body": {
        "& tr:nth-child(2), & tr:nth-child(3), & tr:nth-child(4)": {
          "& td": {
            backgroundColor: theme.palette.color.gray,
            color: theme.palette.color.main,
          },
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
