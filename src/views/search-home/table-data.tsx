import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Button } from "components/shared";
import { useAddName } from "helpers/for-priject";
import { useTranslator } from "localization";
import React from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "./search-home.style";
import { ISearchHome } from "./types";

export const useColumns = (handleRemove: Function, handleShow: Function) => {
  const lang = useTranslator("findHome");
  const history = useHistory();
  const classes = useStyles();
  const { addName } = useAddName();

  return [
    {
      title: lang.userName,
      field: "name",
    },
    { title: lang.email, field: "email" },
    { title: lang.phone, field: "phone" },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: ISearchHome) => (
        <div className={classes.dataTableButtonsContainer}>
          <Button onClick={() => handleShow(rowData.id)} variant="contained">
            <VisibilityIcon />
          </Button>
          <Button onClick={() => handleRemove(rowData.id)} background="red" variant="contained" text="white">
            <DeleteIcon />
          </Button>
        </div>
      ),
    },
  ];
};
