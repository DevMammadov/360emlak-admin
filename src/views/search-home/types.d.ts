export interface ISearchHomeRequest {
  id?: string;
  name: string;
  email: string;
  phone: string;
  is360?: boolean;
  propType?: string;
  salesType?: string;
  buildingType?: string;
  document?: boolean;
  mortgage?: boolean;
  city?: string;
  region?: string;
  settlement?: string;
  metro?: string;
  rooms?: string;
  areaMin?: number;
  areaMax?: number;
  landAreaMin?: number;
  landAreaMax?: number;
  homeFloorMax?: number;
  homeFloorMin?: number;
  priceMax?: number;
  priceMin?: number;
  notFirstFloor?: boolean;
  notLastFloor?: boolean;
}

export interface ISearchHome {
  id?: string;
  name: string;
  email: string;
  phone: string;
  propType?: string;
  salesType?: string;
  city?: string;
  region?: string;
  settlement?: string;
  metro?: string;
  rooms?: string;
}

export interface IRequests {
  totalCount: number;
  interest: ISearchHomeRequest[];
}

export interface ISearchRequest {
  email?: string;
  limit?: number;
  offset?: number;
  salestype?: string;
  proptype?: string;
  city?: string;
  is360?: boolean;
}

export interface IRequestTable {
  email?: string;
  limit?: number;
  offset?: number;
}
