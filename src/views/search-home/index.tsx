import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./search-home.style";
import { Button, DataTable, SectionHeader, TextField } from "components/shared";
import { IconButton, TableCell } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { Form } from "react-final-form";
import clsx from "clsx";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import { useSwal } from "helpers/swal";
import { useColumns } from "./table-data";
import { IRequests, ISearchHomeRequest } from "./types";
import { getLimitOffset, toastMessage } from "helpers/functions";
import RequestApi from "api/search-home";
import { useHistory, useParams } from "react-router";
import { links } from "routes/links";

export interface ISearchHome {}

export const SearchHome: FC<ISearchHome> = ({}) => {
  const lang = useTranslator("findHome", ["alerts", "toast"]);
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [email, setEmail] = useState("");
  const { Alert, Prompt } = useSwal();
  const [requests, setRequests] = useState<IRequests>({} as IRequests);
  const [changed, setChanged] = useState(false);
  const history = useHistory();
  const limit = 8;

  useEffect(() => {
    RequestApi.getRequests({
      email,
      ...getLimitOffset(page, limit),
    }).then((payload) => setRequests(payload?.data?.data));
  }, [page, changed, email]);

  const handleRemove = (id: string) => {
    Alert.fire({
      text: lang.homeRequestRemove,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        RequestApi.removeRequest(id).then((p) => {
          toastMessage(p, lang.requestRemoved);
          setChanged(!changed);
        });
      }
    });
  };

  const handleSearch = (data: any) => {
    setEmail(data.email);
  };

  const handleShow = (id: number) => {
    history.push(`${links.homeRequests.view}/${id}`);
  };

  const renderContent = () => {
    return (
      <Form
        onSubmit={handleSearch}
        render={({ handleSubmit, form }) => (
          <form onSubmit={handleSubmit} className={classes.searchForm}>
            <TextField
              name="email"
              placeholder={lang.userEmail}
              fullWidth
              InputProps={{
                endAdornment: (
                  <IconButton
                    className={clsx(classes.clearButton, email?.length > 0 && classes.clearButtonActive)}
                    onClick={() => {
                      setEmail("");
                      form.reset();
                    }}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                ),
              }}
            />
            <Button for="search" type="submit" variant="contained">
              {lang.search}
            </Button>
          </form>
        )}
      />
    );
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  console.log(requests?.interest);

  return (
    <section>
      <SectionHeader renderContent={renderContent} title={lang.users} />
      <div>
        <DataTable
          data={requests?.interest || []}
          columns={useColumns(handleRemove, handleShow)}
          options={{ toolbar: false, pageSize: limit }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(requests?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
