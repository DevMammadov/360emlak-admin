import BlockIcon from "@material-ui/icons/Block";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Button, Image } from "components/shared";
import { getPhoto } from "helpers/functions";
import { useUser } from "hooks/useUser";
import { useTranslator } from "localization";
import React from "react";
import { useStyles } from "./baner.style";
import { IBanner } from "./type";

export const useColumns = (handleRemove: Function, handleEdit: Function) => {
  const lang = useTranslator("baner");
  const classes = useStyles();
  const currentUser = useUser();

  return [
    {
      title: lang.images,
      field: "",
      render: (rowData: IBanner) => (
        <div className={classes.images}>
          <Image src={getPhoto(rowData.bannerLeft)} />
          <Image src={getPhoto(rowData.bannerRight)} />
        </div>
      ),
    },
    { title: lang.owner, field: "owner" },
    { title: lang.phone, field: "phone" },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: IBanner) => (
        <div className={classes.dataTableButtonsContainer}>
          <Button onClick={() => handleEdit(rowData.id)} variant="contained">
            <EditIcon />
          </Button>
          {currentUser.roles === "admin" && (
            <Button onClick={() => handleRemove(rowData.id)} background="red" variant="contained" text="white">
              <DeleteIcon />
            </Button>
          )}
        </div>
      ),
    },
  ];
};
