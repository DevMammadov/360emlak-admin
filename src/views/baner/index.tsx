import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./baner.style";
import { DataTable, SectionHeader } from "components/shared";
import { TableCell } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { useColumns } from "./table-data";
import BanerApi from "api/baner.api";
import { IBanner, IBannerList } from "./type";
import { useHistory } from "react-router";
import { links } from "routes/links";
import { useSwal } from "helpers/swal";
import { getLimitOffset } from "helpers/functions";

export interface IBaner {}

export const Baner: FC<IBaner> = ({}) => {
  const lang = useTranslator("baner", ["alerts"]);
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [changed, setChanged] = useState(false);
  const [baner, setBaner] = useState<IBannerList>({} as IBannerList);
  const history = useHistory();
  const { Alert } = useSwal();
  const limit = 10;

  useEffect(() => {
    BanerApi.getBaners({ ...getLimitOffset(page, limit) }).then((payload) => setBaner(payload?.data));
  }, [page, changed]);

  const handleRemove = (id: number) => {
    Alert.fire({
      text: lang.removeBanerAlert,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        BanerApi.remove(id).then(() => setChanged(!changed));
      }
    });
  };
  const handleEdit = (id: number) => {
    history.push(`${links.baners.edit}/${id}`);
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  return (
    <section>
      <SectionHeader title={lang.users} addButton onAddClick={() => history.push(links.baners.add)} />
      <div>
        <DataTable
          data={baner?.banner || []}
          columns={useColumns(handleRemove, handleEdit)}
          options={{ toolbar: false, pageSize: limit }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(baner?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
