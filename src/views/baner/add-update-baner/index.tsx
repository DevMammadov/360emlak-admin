import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./add-update-baner.style";
import { Button, DatePicker, FileField, SectionHeader, TextField } from "components/shared";
import { Form } from "react-final-form";
import { IBanner } from "../type";
import { Grid, Paper } from "@material-ui/core";
import { useValidators } from "hooks/useValidators";
import BanerApi from "api/baner.api";
import { useHistory, useParams } from "react-router";
import { toFormData } from "helpers/to-form-data";
import { links } from "routes/links";
import { toast } from "react-toastify";
import { removeUndefined } from "helpers/functions";

export interface IinitialPhotos {
  bannerLeft: string[] | undefined;
  bannerRight: string[] | undefined;
}

export interface IAddUpdateBaner {}

export const AddUpdateBaner: FC<IAddUpdateBaner> = ({}) => {
  const lang = useTranslator("baner", ["toast"]);
  const classes = useStyles();
  const [initialValues, setInitialValues] = useState<IBanner>({} as IBanner);
  const [initialPhotos, setInitialPhotos] = useState<IinitialPhotos>({} as IinitialPhotos);
  const { required } = useValidators();
  const params: any = useParams();
  const history = useHistory();

  useEffect(() => {
    if (params.id) {
      BanerApi.getById(params.id).then((payload) => {
        setInitialValues(payload.data);
        setInitialPhotos({ bannerLeft: [payload.data?.bannerLeft], bannerRight: [payload.data?.bannerRight] });
      });
    }
  }, [params.id]);

  const handleFormSubmit = (data: any) => {
    const request: IBanner = {
      ...data,
      bannerLeft: typeof data.bannerLeft === "string" ? undefined : data.bannerLeft,
      bannerRight: typeof data.bannerRight === "string" ? undefined : data.bannerRight,
    };

    if (params.id) {
      BanerApi.update(params.id, toFormData(removeUndefined(request))).then((payload) => {
        if (payload?.statusCode == 200) {
          history.push(links.baners.baseUrl);
          toast.success(lang.banerUpdated);
        }
      });
    } else {
      BanerApi.add(toFormData(removeUndefined(request))).then((payload) => {
        if (payload?.statusCode == 200) {
          history.push(links.baners.baseUrl);
          toast.success(lang.bannerAdded);
        }
      });
    }
  };

  return (
    <section>
      <SectionHeader title={params.id ? lang.updateBaner : lang.addBaner} />
      <Form
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        render={({ handleSubmit, form, values }) => (
          <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container component={Paper} className={classes.formPaper}>
              <Grid item xs={12}>
                <TextField label={lang.owner} validate={required} name="owner" />
              </Grid>
              <Grid item xs={12}>
                <TextField label={lang.phone} validate={required} name="phone" />
              </Grid>
              <Grid item xs={12}>
                <TextField label={lang.link} validate={required} name="link" />
              </Grid>
              <Grid item xs={12}>
                <DatePicker label={lang.expireDate} validate={required} name="expireDate" />
              </Grid>
              <Grid item xs={6} className={classes.fileContainer}>
                <FileField
                  validate={required}
                  label={lang.bannerLeft}
                  initialPhotos={initialPhotos.bannerLeft}
                  onInitialPhotoRemove={() => setInitialPhotos({ ...initialPhotos, bannerLeft: undefined })}
                  priview
                  name="bannerLeft"
                />
              </Grid>
              <Grid item xs={6} className={classes.fileContainer}>
                <FileField
                  validate={required}
                  label={lang.bannerRight}
                  initialPhotos={initialPhotos.bannerRight}
                  onInitialPhotoRemove={() => setInitialPhotos({ ...initialPhotos, bannerRight: undefined })}
                  priview
                  name="bannerRight"
                />
              </Grid>
              <Grid item xs={12} className={classes.buttonContainer}>
                <Button type="submit">{lang.send}</Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </section>
  );
};
