import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    images: {
      "& img": {
        width: 70,
        height: 100,
        objectFit: "cover",
      },
      "& img:first-child": {
        marginRight: theme.spacing(1),
      },
    },
    dataTableButtonsContainer: {
      display: "flex",
      justifyContent: "space-between",
      width: "24%",
      margin: "0 auto",
      "& .MuiButtonBase-root": {
        padding: theme.spacing(0.5),
        marginRight: 5,
        minWidth: "unset",
        "& .MuiSvgIcon-root": {
          fontSize: 25,
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
