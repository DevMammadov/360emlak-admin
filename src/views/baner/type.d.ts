export interface IBanner {
  id?: string;
  owner: string;
  phone: string;
  bannerLeft: string;
  bannerRight: string;
  link: string;
  expireDate: string;
}

export interface IBannerList {
  totalCount: number;
  banner: IBanner[];
}
