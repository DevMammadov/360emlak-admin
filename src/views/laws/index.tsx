import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./laws.style";
import { useTranslator } from "localization";
import { Section, SectionHeader, TabPanel, Jodit, Button } from "components/shared";
import { Grid, Paper, Tab, Tabs } from "@material-ui/core";
import { useLocation, useParams } from "react-router";
import LawsApi from "api/laws.api";
import { toastMessage } from "helpers/functions";

export interface ILawsReq {
  type: string;
  contentAz: string;
  contentEn: string;
  contentRu: string;
}

export interface ILaws {}

export const Laws: FC<ILaws> = () => {
  const lang = useTranslator("menu", ["alerts"]);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [contents, setContents] = useState({ contentAz: "", contentEn: "", contentRu: "" });
  const [updateId, setUpdateId] = useState<string | null>(null);
  const [changed, setChanged] = useState(false);
  const loc = useLocation();

  const viewEnum = [
    {
      key: "/laws/increase-balance",
      title: lang.increaseBalance,
      type: "balance",
    },
    {
      key: "/laws/announce-law",
      title: lang.announceAdding,
      type: "announce",
    },
    {
      key: "/laws/advertisment-law",
      title: lang.addAdding,
      type: "advertisment",
    },
    {
      key: "/laws/user-agreement",
      title: lang.userAgreement,
      type: "agreement",
    },
  ];

  const getField = (field: keyof typeof viewEnum[0]) => {
    return viewEnum.filter((v) => v.key === loc.pathname)[0][field];
  };

  useEffect(() => {
    setContents({ contentAz: "", contentEn: "", contentRu: "" });
  }, [loc.pathname]);

  useEffect(() => {
    LawsApi.getLaw(getField("type")).then((p) => {
      const { id, type, ...rest } = p?.data || {};
      if (id) {
        setContents(rest);
        setUpdateId(id);
      } else {
        setContents({ contentAz: "", contentEn: "", contentRu: "" });
        setUpdateId(null);
      }
    });
  }, [loc.pathname, changed]);

  const handleSend = () => {
    if (updateId) {
      LawsApi.updateLaw(updateId, { type: getField("type"), ...contents }).then((p) => {
        setChanged(!changed);
        toastMessage(p, lang.updatedSuccessfuly);
      });
    } else {
      LawsApi.addLaw({ type: getField("type"), ...contents }).then((p) => {
        setChanged(!changed);
        toastMessage(p, lang.updatedSuccessfuly);
      });
    }
  };

  return (
    <Section loading={true}>
      <SectionHeader title={getField("title") || ""} />
      <Grid container>
        <Grid item xs={12}>
          <Paper className={classes.tabContainer}>
            <Tabs value={value} variant="fullWidth" className={classes.tabs} onChange={(e, v) => setValue(v)}>
              <Tab label="Azərbaycan dili" />
              <Tab label="Русский Язык" />
              <Tab label="Enlish language" />
            </Tabs>
            <TabPanel value={value} index={0}>
              <Jodit value={contents.contentAz} onBlur={(contentAz) => setContents({ ...contents, contentAz })} />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Jodit value={contents.contentRu} onBlur={(contentRu) => setContents({ ...contents, contentRu })} />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <Jodit value={contents.contentEn} onBlur={(contentEn) => setContents({ ...contents, contentEn })} />
            </TabPanel>
            <Button className={classes.sendButton} onClick={handleSend}>
              {lang.send}
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </Section>
  );
};
