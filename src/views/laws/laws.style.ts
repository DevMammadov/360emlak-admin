import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    tabs: {
      marginTop: theme.spacing(4),
      "& button": {
        textTransform: "inherit",
      },
    },
    tabContainer: {
      maxWidth: 850,
      margin: "0 auto",
      padding: theme.spacing(3, 10, 2, 10),
    },
    sendButton: {
      display: "block",
      margin: "0 auto",
      padding: theme.spacing(0.7, 8),
    },
    [theme.breakpoints.down("xl")]: {
      tabContainer: {
        maxWidth: 950,
      },
    },
    [theme.breakpoints.down("lg")]: {
      tabContainer: {
        maxWidth: 850,
      },
    },
    [theme.breakpoints.down("md")]: {
      tabContainer: {
        maxWidth: 750,
      },
    },
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
