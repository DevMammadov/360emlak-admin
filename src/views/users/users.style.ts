import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    searchForm: {
      display: "flex",
      "& .MuiButtonBase-root": {
        marginLeft: theme.spacing(2),
      },
    },
    clearButton: {
      padding: 0,
      visibility: "hidden",
    },
    clearButtonActive: {
      visibility: "visible",
    },
    swalForm: {
      "& > span": {
        fontSize: 16,
        display: "inline-block",
        marginBottom: 8,
        color: theme.palette.color.main,
      },
      "& .MuiFormControl-root": {
        "& > label": {
          "& div": {
            fontSize: 14,
          },
        },
      },
    },
    dataTableButtonsContainer: {
      display: "flex",
      justifyContent: "space-between",
      width: "45%",
      margin: "0 auto",
      "& .MuiButtonBase-root": {
        padding: theme.spacing(0.5),
        marginRight: 5,
        minWidth: "unset",
        "& .MuiSvgIcon-root": {
          fontSize: 25,
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
