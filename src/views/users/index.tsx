import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./users.style";
import { Button, DataTable, DatePicker, SectionHeader, TextField } from "components/shared";
import { useHistory } from "react-router-dom";
import { links } from "routes/links";
import { getLimitOffset, toastMessage } from "helpers/functions";
import { useColumns } from "./table-data";
import { useSwal } from "helpers/swal";
import { IconButton, TableCell, useTheme } from "@material-ui/core";
import { IUser, IUserList } from "./types";
import { Pagination } from "@material-ui/lab";
import clsx from "clsx";
import { Form } from "react-final-form";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import UserApi from "api/users.api";
import { toast } from "react-toastify";
import dayjs from "dayjs";

export interface IUsersPage {}

export const Users: FC<IUsersPage> = ({}) => {
  const lang = useTranslator("agencies", ["alerts"]);
  const roleLang = useTranslator("roles");
  const classes = useStyles();
  const history = useHistory();
  const [users, setUsers] = useState<IUserList>({} as IUserList);
  const [page, setPage] = useState(1);
  const [email, setEmail] = useState("");
  const [changed, setChanged] = useState(false);
  const theme = useTheme();
  const { Alert, Prompt } = useSwal();
  const limit = 10;

  useEffect(() => {
    UserApi.getUsers({
      email,
      ...getLimitOffset(page, limit),
    }).then((data) => setUsers(data?.data));
  }, [page, changed, email]);

  const handleRemove = (id: string) => {
    Alert.fire({
      text: lang.removeUserAlert,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        UserApi.delete(id)
          .then(() => setChanged(!changed))
          .then((p) => toastMessage(p, lang.userRemoved));
      }
    });
  };

  const handleSearch = (data: any) => {
    setEmail(data.email);
  };

  const renderContent = () => {
    return (
      <Form
        onSubmit={handleSearch}
        render={({ handleSubmit, form }) => (
          <form onSubmit={handleSubmit} className={classes.searchForm}>
            <TextField
              name="email"
              placeholder={lang.userEmail}
              fullWidth
              InputProps={{
                endAdornment: (
                  <IconButton
                    className={clsx(classes.clearButton, email?.length > 0 && classes.clearButtonActive)}
                    onClick={() => {
                      setEmail("");
                      form.reset();
                    }}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                ),
              }}
            />
            <Button for="search" type="submit" variant="contained">
              {lang.search}
            </Button>
          </form>
        )}
      />
    );
  };

  const handleEdit = (id: string) => {
    history.push(`${links.users.edit}/${id}`);
  };

  const handleBlock = (id: string, blocked: boolean, blockEnd: string) => {
    Prompt.fire({
      html: (
        <Form
          onSubmit={() => {}}
          initialValues={{ date: blocked ? blockEnd : new Date() }}
          render={() => (
            <form className={classes.swalForm}>
              <span> {lang.userWillBlock} </span>
              <DatePicker label={lang.selectBanDate} name="date" id="date" />
            </form>
          )}
        />
      ),
      input: undefined,
      icon: "warning",
      preConfirm: () => {
        //@ts-ignore
        return document.getElementById("date").value;
      },
    }).then((result) => {
      if (result.value) {
        UserApi.blockUser(id, result.value).then((p) => {
          toastMessage(p, "user bloklandi");
          setChanged(!changed);
        });
      }
    });
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  const handleRoleSelect = (data: any) => {
    UserApi.changeRole(data.id, data.role).then((payload) =>
      toastMessage(payload, `${roleLang[data.role]} ${lang.roleAddedToUser}`)
    );
  };

  const handleActivate = (id: string) => {
    UserApi.activeUser(id).then((p) => {
      toastMessage(p, lang.useractivated);
      setChanged(!changed);
    });
  };

  return (
    <section>
      <SectionHeader renderContent={renderContent} title={lang.users} />
      <div>
        <DataTable
          data={users?.users || []}
          columns={useColumns(handleRemove, handleEdit, handleBlock, handleActivate, handleRoleSelect)}
          options={{
            toolbar: false,
            pageSize: limit,
            rowStyle: (rowData: IUser) => ({
              backgroundColor: rowData.lockoutEnabled ? "#ffebeb" : "#FFF",
            }),
          }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(users?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
