import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Button, Select } from "components/shared";
import { useSelectData } from "hooks/useSelectData";
import { useTranslator } from "localization";
import React from "react";
import { Form } from "react-final-form";
import { useHistory } from "react-router-dom";
import { IUser } from "./types";
import { useStyles } from "./users.style";
import BlockIcon from "@material-ui/icons/Block";
import { useUser } from "hooks/useUser";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import dayjs from "dayjs";

export const useColumns = (
  handleRemove: Function,
  handleEdit: Function,
  handleBlock: Function,
  handleActivate: Function,
  onRoleSelect: (data: any) => void
) => {
  const lang = useTranslator("agencies");
  const history = useHistory();
  const classes = useStyles();
  const { roles } = useSelectData();
  const roleLang = useTranslator("roles");
  const currentUser = useUser();

  return [
    {
      title: lang.userName,
      field: "fullName",
      render: (rowData: IUser) => (
        <div>
          <span>{rowData.fullName}</span>
          {rowData.lockoutEnabled && (
            <div>
              <small>
                ( <b>{dayjs(rowData.lockoutEnd).format("DD-MM-YYYY")} </b>
                {lang.blockedUntilDate} )
              </small>
            </div>
          )}
        </div>
      ),
    },
    {
      title: lang.role,
      field: "email",
      render: (rowData: IUser) =>
        currentUser.roles === "admin" ? (
          <Form
            onSubmit={(data) => onRoleSelect({ ...data, id: rowData.id })}
            initialValues={{ role: rowData.role }}
            render={({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Select disabled={rowData.lockoutEnabled} name="role" onBlur={handleSubmit} data={roles} />
              </form>
            )}
          />
        ) : (
          <> {roleLang[rowData.role]} </>
        ),
    },
    { title: lang.email, field: "email" },
    { title: lang.phone, field: "phoneNumber" },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: IUser) => (
        <div className={classes.dataTableButtonsContainer}>
          <Button onClick={() => handleEdit(rowData.id)} variant="contained">
            <EditIcon />
          </Button>
          {currentUser.roles === "admin" && (
            <>
              {rowData.lockoutEnabled ? (
                <Button onClick={() => handleActivate(rowData.id)} variant="contained" background="green" text="white">
                  <VerifiedUserIcon />
                </Button>
              ) : (
                <Button
                  onClick={() => handleBlock(rowData.id, rowData.lockoutEnabled, rowData.lockoutEnd)}
                  variant="contained"
                  background="red"
                  text="white"
                >
                  <BlockIcon />
                </Button>
              )}

              <Button onClick={() => handleRemove(rowData.id)} variant="contained" background="red" text="white">
                <DeleteIcon />
              </Button>
            </>
          )}
        </div>
      ),
    },
  ];
};
