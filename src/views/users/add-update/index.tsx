import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./add-update.style";
import { Button, CapsLock, SectionHeader, TextField } from "components/shared";
import { Form } from "react-final-form";
import { Box, Grid, Paper } from "@material-ui/core";
import { useValidators } from "hooks/useValidators";
import UserApi from "api/users.api";
import { useHistory, useParams } from "react-router";
import { IUserEdit } from "../types";
import { links } from "routes/links";
import { toast } from "react-toastify";
import { Checkboxes } from "mui-rff";

export interface IAddUpdate {}

export const AddUpdateUsers: FC<IAddUpdate> = ({}) => {
  const lang = useTranslator("agencies", ["alerts"]);
  const classes = useStyles();
  const { required, email } = useValidators();
  const params: any = useParams();
  const [user, setUser] = useState<IUserEdit>({} as IUserEdit);
  const history = useHistory();

  useEffect(() => {
    if (params.id) {
      UserApi.getFullUserById(params.id).then((payload) => setUser(payload?.data));
    }
  }, []);

  const handleFormSubmit = (data: any) => {
    const { id, info, ...rest } = data;

    const reqModel: IUserEdit = {
      ...rest,
      phoneNumberConfirmed: info?.includes("phoneNumberConfirmed"),
      emailConfirmed: info?.includes("emailConfirmed"),
    };

    UserApi.edit(params.id, reqModel).then((payload) => {
      if (payload?.statusCode === 200) {
        history.push(links.users.baseUrl);
        toast.success(lang.userUpdated);
      }
    });
  };

  const getInitialVals = (initial: IUserEdit) => {
    return {
      ...initial,
      info: [initial?.phoneNumberConfirmed && "phoneNumberConfirmed", initial?.emailConfirmed && "emailConfirmed"],
    };
  };

  return (
    <section>
      <SectionHeader title={lang.userAdding} />
      <Form
        onSubmit={handleFormSubmit}
        initialValues={getInitialVals(user)}
        render={({ handleSubmit, form, values }) => (
          <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container component={Paper} className={classes.formPaper}>
              <Grid item xs={12} md={6}>
                <TextField label={lang.nameOf} validate={required} name="name" className={classes.groupField} />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField label={lang.surnameOf} validate={required} name="surname" />
              </Grid>
              <Grid item xs={12}>
                <TextField label={lang.phoneNumber} validate={required} name="phoneNumber" />
              </Grid>
              <Grid item xs={12}>
                <TextField label={lang.email} validate={required} type="email" name="email" />
              </Grid>
              <Grid item xs={12}>
                <Checkboxes
                  name="info"
                  color="primary"
                  formGroupProps={{ row: true }}
                  data={[
                    { label: lang.emailConfirmed, value: "emailConfirmed" },
                    { label: lang.phoneConfirmed, value: "phoneNumberConfirmed" },
                  ]}
                />
              </Grid>
              <Grid item xs={12} className={classes.buttonContainer}>
                <Button type="submit">{lang.send}</Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </section>
  );
};
