export interface IUserRequest {
  email?: string;
  limit: number;
  offset: number;
}

export interface IUser {
  email: string;
  fullName: string;
  iamAgent: boolean;
  id: string;
  lockoutEnabled: boolean;
  lockoutEnd: string;
  phoneNumber: string;
  role: number;
}

export interface IUserList {
  totalCount: number;
  users: IUser[];
}

export interface IUserEdit {
  id: string;
  name: string;
  surname: string;
  username: string;
  email: string;
  phoneNumber: string;
  emailConfirmed: boolean;
  phoneNumberConfirmed: boolean;
  lockoutEnabled: boolean;
  lockoutEnd: string;
  accessFailedCount: number;
}

export interface IUserBasic {
  id: string;
  fullName: string;
  iamAgent: boolean;
  email: string;
  photo?: string;
}
