import { Box, Grid, Paper, Tab, Tabs } from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import InfoApi from "api/info.api";
import clsx from "clsx";
import {
  Button,
  FileField,
  GoogleMap,
  Jodit,
  Section,
  SectionHeader,
  StaticMap,
  TabPanel,
  TextField,
} from "components/shared";
import { ILngLat } from "components/shared/google-map/types";
import { toastMessage } from "helpers/functions";
import { useTranslator } from "localization";
import React, { FC, useEffect, useState } from "react";
import { Form } from "react-final-form";
import { useStyles } from "./info.style";
import { IInfo } from "./types";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import YouTubeIcon from "@material-ui/icons/YouTube";
import { Instagram } from "@material-ui/icons";
import { toFormData } from "helpers/to-form-data";

export interface IInfoPage {}

interface IContents {
  contentAz: string;
  contentRu: string;
  contentEn: string;
}

export const Info: FC<IInfoPage> = () => {
  const lang = useTranslator("info", ["toast"]);
  const classes = useStyles();
  const [contents, setContents] = useState<IContents>({ contentAz: "", contentEn: "", contentRu: "" });
  const [mapOpen, setMapOpen] = useState(false);
  const [markerPos, setMarkerPos] = useState<ILngLat | undefined>(undefined);
  const [initialValues, setInitialValues] = useState<IInfo | undefined>(undefined);
  const [cover, setCover] = useState<string[] | undefined>(undefined);
  const [logo, setLogo] = useState<string[] | undefined>(undefined);
  const [value, setValue] = React.useState(0);

  useEffect(() => {
    InfoApi.getInfo().then((payload) => {
      setInitialValues(payload?.data || undefined);
      setCover([payload?.data?.cover]);
      setLogo([payload?.data?.logo]);
      setContents({
        contentAz: payload?.data?.contentAz,
        contentRu: payload?.data?.contentRu,
        contentEn: payload?.data?.contentEn,
      });
      setMarkerPos({ lat: Number(payload?.data?.lat), lng: Number(payload?.data?.lng) });
    });
  }, []);

  const handleFormSubmit = (data: IInfo) => {
    const reqModel: IInfo = {
      ...data,
      lat: markerPos?.lat.toString(),
      lng: markerPos?.lng.toString(),
      ...contents,
    };

    console.log(toFormData(reqModel));

    if (initialValues?.id) {
      InfoApi.update(initialValues?.id, toFormData(reqModel)).then((payload) =>
        toastMessage(payload, lang.infoUpdated)
      );
    } else {
      InfoApi.add(toFormData(reqModel)).then((payload) => toastMessage(payload, lang.infoUpdated));
    }
  };

  return (
    <Section loading={true}>
      <SectionHeader title={lang.generalInfo} />
      <Form
        onSubmit={handleFormSubmit}
        initialValues={initialValues}
        render={({ handleSubmit, values }) => (
          <form onSubmit={handleSubmit}>
            <Grid component={Paper} container className={clsx(classes.formPaper, classes.searchContainer)}>
              <Grid item xs={6}>
                <FileField
                  name="cover"
                  label={lang.cover}
                  onInitialPhotoRemove={() => setCover(undefined)}
                  initialPhotos={cover}
                  priview
                />
              </Grid>
              <Grid item xs={6}>
                <FileField
                  name="logo"
                  label={lang.logo}
                  onInitialPhotoRemove={() => setLogo(undefined)}
                  initialPhotos={logo}
                  priview
                />
              </Grid>
              <Grid item xs={12}>
                <TextField name="name" label={lang.websiteName} />
              </Grid>
              <Grid item xs={12}>
                <TextField name="defis" label="Defis" />
              </Grid>
              <Grid item xs={12}>
                <TextField name="email" label={lang.email} />
              </Grid>
              <Grid item xs={6}>
                <TextField name="phone1" label={`${lang.phone} (1)`} />
              </Grid>
              <Grid item xs={6}>
                <TextField name="phone2" label={`${lang.phone} (2)`} />
              </Grid>
              <Grid item xs={6}>
                <TextField adornmentIcon={FacebookIcon} name="facebook" label="Facebook" />
              </Grid>
              <Grid item xs={6}>
                <TextField adornmentIcon={InstagramIcon} name="instagram" label="İnstagram" />
              </Grid>
              <Grid item xs={6}>
                <TextField adornmentIcon={YouTubeIcon} name="youtube" label="Youtube" />
              </Grid>
              <Grid item xs={6}>
                <TextField adornmentIcon={LinkedInIcon} name="linkedin" label="Linkedin" />
              </Grid>
              <Grid item xs={12} style={{ display: "block" }}>
                <Box mb={1} display="flex" justifyContent="space-between" alignItems="center">
                  {lang.translateable}
                </Box>
                <div className={classes.tabContainer}>
                  <Tabs
                    value={value}
                    variant="fullWidth"
                    className={classes.tabs}
                    onChange={(e, v) => setValue(v)}
                    aria-label="simple tabs example"
                  >
                    <Tab label="Azərbaycan dili" />
                    <Tab label="Русский Язык" />
                    <Tab label="Enlish language" />
                  </Tabs>
                  <TabPanel value={value} index={0}>
                    <TextField name="addressAz" label={lang.address} />
                    <Jodit
                      label={`${lang.aboutPageContent} (Az)`}
                      value={contents.contentAz}
                      onBlur={(contentAz) => setContents({ ...contents, contentAz })}
                    />
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <TextField name="addressRu" label={lang.address} />
                    <Jodit
                      label={`${lang.aboutPageContent} (Ru)`}
                      value={contents.contentRu}
                      onBlur={(contentRu) => setContents({ ...contents, contentRu })}
                    />
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <TextField name="addressEn" label={lang.address} />
                    <Jodit
                      label={`${lang.aboutPageContent} (En)`}
                      value={contents.contentEn}
                      onBlur={(contentEn) => setContents({ ...contents, contentEn })}
                    />
                  </TabPanel>
                </div>
              </Grid>
              <Grid item xs={12}>
                <Button
                  text="white"
                  icon={LocationOnIcon}
                  className={classes.showMapButton}
                  background="main"
                  onClick={() => setMapOpen(!mapOpen)}
                >
                  {lang.showAddressOnMap}
                </Button>
                <GoogleMap
                  initial={markerPos}
                  open={mapOpen}
                  onClose={() => setMapOpen(!mapOpen)}
                  onSubmit={(data) => setMarkerPos(data)}
                />
              </Grid>
              <Grid item xs={12}>
                {markerPos && (
                  <Grid item xs={12}>
                    <StaticMap position={markerPos} width={850} className={classes.staticMap} />
                  </Grid>
                )}
              </Grid>
              <Grid item xs={12}>
                <Button className={classes.sendButton} type="submit">
                  {lang.send}
                </Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </Section>
  );
};
