export interface IInfo {
  id?: number;
  name: string;
  email: string;
  phone1: string;
  phone2: string;
  contentAz: string;
  contentRu: string;
  contentEn: string;
  addressAz: string;
  addressRu: string;
  addressEn: string;
  lat?: string;
  lng?: string;
  facebook: string;
  instagram: string;
  youtube: string;
  linkedin: string;
  defis: string;
  cover: string;
}
