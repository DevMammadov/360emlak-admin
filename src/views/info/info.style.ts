import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    formPaper: {
      maxWidth: 850,
      margin: "0 auto",
      padding: theme.spacing(3, 10, 6, 10),
      "& .MuiGrid-item": {
        marginBottom: theme.spacing(2),
        padding: theme.spacing(0, 1),
        display: "flex",
        alignItems: "flex-end",
      },
    },
    staticMap: {
      width: "100%",
      height: 150,
      objectFit: "contain",
    },
    sendButton: {
      padding: theme.spacing(1, 5),
      margin: "0 auto",
    },
    tabs: {
      marginTop: theme.spacing(4),
      "& button": {
        textTransform: "inherit",
      },
    },
    tabContainer: {
      "& .MuiTypography-root": {
        "& .MuiFormControl-root": {
          marginBottom: theme.spacing(2),
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
