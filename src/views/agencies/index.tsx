import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./agencies.style";
import { useTranslator } from "localization";
import { Autocomplete, Button, DataTable, Select, TextField } from "components/shared";
import { SectionHeader } from "components/shared/section-header";
import { Form } from "react-final-form";
import { useSelectData } from "hooks/useSelectData";
import AgencyApi from "api/agencies.api";
import { IconButton, TableCell } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { IAgencyCard } from "./types";
import { getLimitOffset } from "helpers/functions";
import { useColumns } from "./table-data";
import { useHistory } from "react-router";
import { links } from "routes/links";
import { useSwal } from "helpers/swal";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import clsx from "clsx";

export interface IAgencies {}

export const Agencies: FC<IAgencies> = () => {
  const lang = useTranslator("main", ["filter", "alerts", "agencies"]);
  const { AddFilter } = useSelectData();
  const [agencies, setAgencies] = useState<{ totalCount: number; agentCards: IAgencyCard[] }>({
    totalCount: 0,
    agentCards: [],
  });
  const [page, setPage] = useState(1);
  const [changed, setChanged] = useState(false);
  const classes = useStyles();
  const history = useHistory();
  const { Alert } = useSwal();
  const [name, setName] = useState("");
  const limit = 10;

  useEffect(() => {
    AgencyApi.getAgencies({
      name,
      ...getLimitOffset(page, limit),
    }).then((data) => setAgencies(data?.data));
  }, [page, changed, name]);

  const handleSearch = (data: any) => {
    setName(data.name);
  };

  const renderContent = () => {
    return (
      <Form
        onSubmit={handleSearch}
        initialValues={{ filter: 0 }}
        render={({ handleSubmit, form }) => (
          <form onSubmit={handleSubmit} className={classes.searchForm}>
            <TextField
              name="name"
              placeholder={lang.agencyName}
              fullWidth
              InputProps={{
                endAdornment: (
                  <IconButton
                    className={clsx(classes.clearButton, name.length > 0 && classes.clearButtonActive)}
                    onClick={() => {
                      setName("");
                      form.reset();
                    }}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                ),
              }}
            />
            <Button for="search" type="submit" variant="contained">
              {lang.search}
            </Button>
          </form>
        )}
      />
    );
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  const handleRemove = (id: string) => {
    Alert.fire({
      text: lang.removeAgencyAlert,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        AgencyApi.removeAgent(id).then(() => setChanged(!changed));
      }
    });
  };

  const handleEdit = (id: string) => {
    history.push(`${links.agencies.edit}/${id}`);
  };

  return (
    <section>
      <SectionHeader renderContent={renderContent} onAddClick={() => history.push(links.agencies.add)} addButton />
      <div>
        <DataTable
          data={agencies?.agentCards || []}
          columns={useColumns(handleRemove, handleEdit)}
          options={{ toolbar: false, pageSize: limit }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(agencies?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
