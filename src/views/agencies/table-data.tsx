import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Button } from "components/shared";
import { getPhoto } from "helpers/functions";
import { useUser } from "hooks/useUser";
import { useTranslator } from "localization";
import React from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "./agencies.style";
import { IAgency, IAgencyCard } from "./types";

export const useColumns = (handleRemove: Function, handleEdit: Function) => {
  const lang = useTranslator("agencies", ["publishStatus"]);
  const history = useHistory();
  const classes = useStyles();
  const currentUser = useUser();

  return [
    {
      title: lang.no,
      field: "logo",
      render: (rowData: IAgencyCard) => (
        <img src={getPhoto(rowData.logo)} className={classes.agentLogo} alt={rowData.name} />
      ),
    },
    {
      title: lang.senderName,
      field: "name",
    },
    { title: lang.announceCount, field: "announceCount" },
    { title: lang.phone, field: "phone1" },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: IAgencyCard) => (
        <div className={classes.dataTableButtonsContainer}>
          <Button onClick={() => handleEdit(rowData.id)} variant="contained">
            <EditIcon />
          </Button>
          {currentUser.roles === "admin" && (
            <Button onClick={() => handleRemove(rowData.id)} variant="contained" background="red" text="white">
              <DeleteIcon />
            </Button>
          )}
        </div>
      ),
    },
  ];
};
