import { Box, Grid, Paper, Tab, Tabs } from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import UsersApi from "api/users.api";
import userNoImage from "assets/userNoImage.png";
import clsx from "clsx";
import {
  Autocomplete,
  Button,
  FileField,
  GoogleMap,
  SectionHeader,
  Select,
  StaticMap,
  TabPanel,
  TextField,
  TimePicker,
  Jodit,
} from "components/shared";
import { ILngLat } from "components/shared/google-map/types";
import { getPhoto } from "helpers/functions";
import { useSelectData } from "hooks/useSelectData";
import { useValidators } from "hooks/useValidators";
import { useTranslator } from "localization";
import { Checkboxes } from "mui-rff";
import React, { FC, useEffect, useState } from "react";
import { Form } from "react-final-form";
import { IAgency } from "../types";
import { useStyles } from "./add-agency.style";
import AgencyApi from "api/agencies.api";
import { useHistory, useParams } from "react-router";
import { links } from "routes/links";

export interface IAddAgency {}

interface IContents {
  contentAz: string;
  contentRu: string;
  contentEn: string;
}

export const AddAgency: FC<IAddAgency> = () => {
  const lang = useTranslator("toast", ["profile"]);
  const classes = useStyles();
  const { required, email, requiredIf } = useValidators();
  const { weekDays } = useSelectData();
  const params: any = useParams();
  const [initialValues, setInitialValues] = useState<Partial<IAgency>>({} as IAgency);
  const [cover, setCover] = useState<string | undefined>(undefined);
  const [logo, setLogo] = useState<string | undefined>(undefined);
  const history = useHistory();
  const [value, setValue] = React.useState(0);
  const [contents, setContents] = useState<IContents>({ contentAz: "", contentEn: "", contentRu: "" });

  useEffect(() => {
    if (params.id) {
      AgencyApi.getByIdAgency(params.id).then((payload: any) => {
        const { contentAz, contentEn, contentRu } = payload?.data?.data || {};
        setInitialValues(payload?.data?.data);
        setCover(payload?.data?.data?.cover);
        setLogo(payload?.data?.data?.logo);
        setContents({ contentAz, contentEn, contentRu });
      });
    }
  }, [params.id]);

  const handleFormSubmit = (data: any) => {
    const formData = new FormData();

    const response: IAgency = {
      weeklyWorkTime: data.everyDay ? [1, 7] : [Number(data.dayFrom), Number(data.dayTo)],
      workTime: [data.timeFrom, data.timeTo],
      ...data,
      ...contents,
    };

    for (let key of Object.keys(response)) {
      let item: any = response[key as keyof typeof response];
      if (item) {
        if (Array.isArray(item)) {
          item.map((i: any) => {
            formData.append(key, i);
          });
        } else {
          formData.append(key, item || "");
        }
      }
    }

    if (params.id) {
      AgencyApi.updateAgency(formData, initialValues.id).then((payload) => {
        if (payload?.data?.statusCode === 200) {
          history.push(links.agencies.baseUrl);
        }
      });
    } else {
      AgencyApi.addAgency(formData).then((payload) => {
        if (payload?.data?.statusCode === 200) {
          history.push(links.agencies.baseUrl);
        }
      });
    }
  };

  const isEveryDay = (times: number[]) => {
    return times && times[0] === 1 && times[1] === 7;
  };

  const getInitialVals = (initial: Partial<IAgency>) => {
    const { weeklyWorkTime, workTime } = initial;

    return {
      ...initial,
      timeFrom: workTime && workTime[0],
      timeTo: workTime && workTime[1],
      dayFrom: weeklyWorkTime && weeklyWorkTime[0],
      dayTo: weeklyWorkTime && weeklyWorkTime[1],
      everyDay: isEveryDay(weeklyWorkTime as number[]),
    };
  };

  return (
    <section>
      <SectionHeader title={!params.id ? lang.agencyAdding : `${initialValues.nameAz || ""} ${lang.agencyUpdating}`} />
      <Form
        onSubmit={handleFormSubmit}
        initialValues={getInitialVals(initialValues)}
        render={({ handleSubmit, form, values }) => (
          <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container component={Paper} className={classes.formPaper}>
              <Grid item xs={12}>
                <TextField name="email" validate={[required, email]} label={lang.email} />
              </Grid>
              <Grid item xs={12}>
                <TextField name="password" validate={requiredIf(params.id)} label={lang.password} />
              </Grid>
              <Grid item xs={6}>
                <TextField name="phone1" validate={required} label={`${lang.phone} (1)`} />
              </Grid>
              <Grid item xs={6}>
                <TextField name="phone2" label={`${lang.phone} (2)`} />
              </Grid>
              <Grid item xs={6}>
                <TimePicker variant="inline" ampm={false} name="timeFrom" label={lang.workTime} />
              </Grid>
              <Grid item xs={6}>
                <TimePicker variant="inline" ampm={false} name="timeTo" />
              </Grid>
              <Grid item xs={4}>
                <Select
                  name="dayFrom"
                  validate={requiredIf(values.everyDay?.length === 0)}
                  disabled={values.everyDay ? values.everyDay[0] : false}
                  data={weekDays}
                  label={lang.workDays}
                />
              </Grid>
              <Grid item xs={4}>
                <Select
                  name="dayTo"
                  validate={requiredIf(values.everyDay?.length === 0)}
                  disabled={values.everyDay ? values.everyDay[0] : false}
                  data={weekDays}
                  label={lang.workDays}
                />
              </Grid>
              <Grid item xs={4}>
                <Checkboxes name="everyDay" data={[{ label: lang.everyDay, value: true }]} />
              </Grid>
              <Grid item xs={12} style={{ display: "block", padding: 0 }}>
                <Box mb={0} display="flex" justifyContent="space-between" alignItems="center">
                  {lang.translateable}
                </Box>
                <div className={classes.tabContainer}>
                  <Tabs
                    value={value}
                    variant="fullWidth"
                    className={classes.tabs}
                    onChange={(e, v) => setValue(v)}
                    aria-label="simple tabs example"
                  >
                    <Tab label="Azərbaycan dili" />
                    <Tab label="Русский Язык" />
                    <Tab label="Enlish language" />
                  </Tabs>
                  <TabPanel value={value} index={0}>
                    <Grid container>
                      <Grid item xs={6}>
                        <TextField validate={required} name="nameAz" label={`${lang.name} (Az)`} />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField name="addressAz" label={`${lang.address} (Az)`} />
                      </Grid>
                      <Grid item xs={12} style={{ display: "block" }}>
                        <Jodit
                          label={`${lang.agencyPageContent} (Az)`}
                          value={contents.contentAz}
                          onBlur={(contentAz) => setContents({ ...contents, contentAz })}
                        />
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <Grid container>
                      <Grid item xs={6}>
                        <TextField validate={required} name="nameRu" label={`${lang.name} (Ru)`} />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField name="addressRu" label={`${lang.address} (Ru)`} />
                      </Grid>
                      <Grid item xs={12} style={{ display: "block" }}>
                        <Jodit
                          label={`${lang.agencyPageContent} (Ru)`}
                          value={contents.contentRu}
                          onBlur={(contentRu) => setContents({ ...contents, contentRu })}
                        />
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <Grid container>
                      <Grid item xs={6}>
                        <TextField validate={required} name="nameEn" label={`${lang.name} (En)`} />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField name="addressEn" label={`${lang.address} (En)`} />
                      </Grid>
                      <Grid item xs={12} style={{ display: "block" }}>
                        <Jodit
                          label={`${lang.agencyPageContent} (En)`}
                          value={contents.contentEn}
                          onBlur={(contentEn) => setContents({ ...contents, contentEn })}
                        />
                      </Grid>
                    </Grid>
                  </TabPanel>
                </div>
              </Grid>
              <Grid item xs={6} className={classes.fileContainer}>
                <FileField
                  validate={required}
                  name="cover"
                  initialPhotos={cover ? [cover] : undefined}
                  onInitialPhotoRemove={() => setCover(undefined)}
                  priview
                  label={lang.coverPhoto}
                  classes={{ image: classes.coverImg }}
                />
              </Grid>
              <Grid item xs={6} className={classes.fileContainer}>
                <FileField
                  validate={required}
                  name="logo"
                  initialPhotos={logo ? [logo] : undefined}
                  onInitialPhotoRemove={() => setLogo(undefined)}
                  priview
                  label={lang.profilePhoto}
                />
              </Grid>
              <Grid item xs={12} className={classes.buttonContainer}>
                <Button type="submit">{lang.send}</Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </section>
  );
};
