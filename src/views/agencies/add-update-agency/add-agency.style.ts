import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    formPaper: {
      maxWidth: 750,
      margin: "0 auto",
      padding: theme.spacing(3, 10, 6, 10),
      "&  .MuiGrid-item": {
        marginBottom: theme.spacing(2),
        padding: theme.spacing(0, 1),
        display: "flex",
        alignItems: "flex-end",
      },
    },
    buttonContainer: {
      display: "flex",
      justifyContent: "center",
      "& .MuiButtonBase-root": {
        margin: "0 auto",
        padding: theme.spacing(1, 5),
      },
    },
    fileContainer: {
      alignItems: "flex-start !important",
    },
    coverImg: {
      width: "100%",
    },
    tabs: {
      marginTop: theme.spacing(1),
      "& button": {
        textTransform: "inherit",
      },
    },
    tabContainer: {
      "& .MuiTypography-root": {
        "& .MuiFormControl-root": {
          marginBottom: theme.spacing(2),
        },
      },
      "& .MuiGrid-item": {
        marginBottom: theme.spacing(0),
        padding: theme.spacing(0, 1),
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
