export interface IAgencyCard {
  id: number;
  address: string;
  announceCount?: number;
  logo: string;
  name: string;
  phone1: string;
}

export interface IAgency {
  id?: string;
  userId?: number;
  about: string;
  cover: string;
  email: string;
  expireDate?: string;
  logo: string;
  phone1: string;
  phone2?: string;
  views?: number;
  weeklyWorkTime: number[];
  workTime: string[];
  announceCount?: number;
  password?: string;
  contentAz: string;
  contentEn: string;
  contentRu: string;
  nameAz: string;
  nameRu: string;
  nameEn: string;
  addressAz: string;
  addressRu: string;
  addressEn: string;
}

export interface AgenciesResponse {
  name?: string;
  limit?: number;
  offset?: number;
}
