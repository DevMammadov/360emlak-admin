import React, { FC } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./editable.style";
import { Button } from "components/shared";
import CreateIcon from "@material-ui/icons/Create";
import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

export interface IEditable {
  onEdit?(): void;
  onRemove?(): void;
}

export const Editable: FC<IEditable> = ({ children, onEdit = () => {}, onRemove = () => {} }) => {
  const lang = useTranslator();
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.editContainer}>
        <IconButton onClick={() => onEdit()}>
          <CreateIcon />
        </IconButton>
        <IconButton onClick={() => onRemove()}>
          <DeleteIcon />
        </IconButton>
      </div>

      <div>{children}</div>
    </div>
  );
};
