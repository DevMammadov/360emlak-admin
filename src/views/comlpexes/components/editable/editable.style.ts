import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    root: {
      position: "relative",
    },
    editContainer: {
      position: "absolute",
      right: -40,
      top: 0,
      zIndex: 2,
      "& .MuiSvgIcon-root": {
        fontSize: 20,
        color: theme.palette.color.main,
      },
      "& .MuiButtonBase-root": {
        height: 40,
        minWidth: 20,
      },
      "& .MuiButtonBase-root:nth-child(2)": {
        "& .MuiSvgIcon-root": {
          fontSize: 20,
          color: theme.palette.color.red,
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
