import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./info-list.style";
import { Fade, Modal, Paper } from "@material-ui/core";
import { Form } from "react-final-form";
import { Button, TextField } from "components/shared";
import InfoIcon from "@material-ui/icons/Info";
import { useValidators } from "hooks/useValidators";
import { IApartmentInfo } from "views/comlpexes/types";
import { isEmpty } from "helpers/functions";

export interface IInfoForm {
  onSubmit?(data: IApartmentInfo): void;
  className?: string;
  initialValues?: IApartmentInfo;
  onClose?(): void;
  onEdit?(data: IApartmentInfo): void;
}

export const InfoForm: FC<IInfoForm> = ({
  onSubmit = () => {},
  className,
  initialValues,
  onClose = () => {},
  onEdit = () => {},
}) => {
  const lang = useTranslator("complex");
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { required, number } = useValidators();

  useEffect(() => {
    if (initialValues) {
      setOpen(true);
    }
  }, [initialValues]);

  const handleFormSubmit = (data: IApartmentInfo) => {
    setOpen(false);
    onClose();

    if (isEmpty(initialValues)) {
      onSubmit(data);
    } else {
      onEdit(data);
    }
  };

  const handleClose = () => {
    setOpen(false);
    onClose();
  };

  return (
    <>
      <Button icon={InfoIcon} className={className} onClick={() => setOpen(true)} background="gray" text="black">
        {lang.info}
      </Button>

      <Modal
        open={open}
        onClose={() => handleClose()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        className={classes.modal}
      >
        <Fade in={open}>
          <Paper className={classes.paper}>
            <Form
              onSubmit={handleFormSubmit}
              initialValues={initialValues}
              render={({ handleSubmit, form, values }) => (
                <form onSubmit={handleSubmit} className={classes.form}>
                  <h2>
                    <InfoIcon /> {lang.complexInfo}
                  </h2>
                  <TextField type="number" validate={[required, number]} name="corpusNum" label={lang.corpus} />
                  <TextField type="number" validate={[required, number]} name="blokNum" label={lang.blok} />
                  <TextField type="number" validate={[required, number]} name="lifts" label={lang.lift} />
                  <TextField type="number" validate={[required, number]} name="apartmentNum" label={lang.apartment} />
                  <TextField type="number" validate={[required, number]} name="floorNum" label={lang.floor} />
                  <TextField
                    type="number"
                    validate={[required, number]}
                    name="apartmentInFloor"
                    label={lang.apartmentInFloor}
                  />
                  <Button type="submit"> {lang.add} </Button>
                </form>
              )}
            />
          </Paper>
        </Fade>
      </Modal>
    </>
  );
};
