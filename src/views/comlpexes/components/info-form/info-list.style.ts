import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    modal: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    paper: {
      width: 400,
      padding: theme.spacing(4),
    },
    form: {
      "& h2": {
        margin: 0,
        marginBottom: theme.spacing(1),
        "& .MuiSvgIcon-root": {
          position: "relative",
          top: 5,
          color: theme.palette.color.main,
        },
      },
      "& > div": {
        marginBottom: theme.spacing(1),
      },
      "& .MuiButtonBase-root": {
        padding: theme.spacing(0.8, 4),
        margin: "0 auto",
        display: "block",
        marginTop: theme.spacing(2),
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
