import React, { FC } from "react";
import { useStyles } from "./project-card.style";
import { useTranslator } from "localization";
import { List } from "components/shared";
import { Grid, Paper } from "@material-ui/core";
import { getPhoto } from "helpers/functions";

export interface IProjectCard {
  room: number;
  size: number;
  roomPrice: number;
  text: string;
  image?: string;
}

export const ProjectCard: FC<IProjectCard> = ({ image, room, roomPrice, size, text }) => {
  const lang = useTranslator("main");
  const classes = useStyles();

  return (
    <Grid container className={classes.project} component={Paper}>
      <Grid item xs={12} md={2}>
        <img src={image ? (typeof image === "object" ? URL.createObjectURL(image) : getPhoto(image)) : undefined} />
      </Grid>
      <Grid item xs={12} md={2}>
        <span>
          {room} {lang.withRooms}
        </span>
      </Grid>
      <Grid item xs={12} md={2}>
        {size} {lang.m2}
      </Grid>
      <Grid item xs={12} md={4}>
        {text}
      </Grid>
      <Grid item xs={12} md={2}>
        {roomPrice} {lang.azn}
      </Grid>
    </Grid>
  );
};
