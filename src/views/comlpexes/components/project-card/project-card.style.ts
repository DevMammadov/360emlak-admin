import { grey } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    project: {
      cursor: "pointer",
      marginBottom: theme.spacing(1),
      color: theme.palette.color.black,
      "&:hover": {
        background: grey[100],
      },
      "& img": {
        height: 100,
        width: 100,
        objectFit: "cover",
      },
      "& .MuiGrid-item": {
        position: "relative",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      },
      "& span": {
        fontWeight: "bold",
        display: "inline-block",
        marginLeft: theme.spacing(1),
        fontSize: 18,
      },
      "& .MuiGrid-item:first-child": {
        justifyContent: "flex-start",
      },
      "& .MuiSvgIcon-root": {
        position: "absolute",
        top: "50%",
        right: 8,
        transform: "translateY(-50%)",
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
