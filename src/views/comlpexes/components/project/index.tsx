import React, { FC, useEffect, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./project.style";
import { Button, FileField, TextField } from "components/shared";
import { useValidators } from "hooks/useValidators";
import { IAppartmentProject } from "views/comlpexes/types";
import { Fade, Modal, Paper } from "@material-ui/core";
import { Form } from "react-final-form";
import DashboardIcon from "@material-ui/icons/Dashboard";
import { isEmpty } from "helpers/functions";

export interface IProject {
  onSubmit(data: IAppartmentProject): void;
  className?: string;
  initialValues?: IAppartmentProject;
  onClose?(): void;
  onEdit?(data: IAppartmentProject): void;
}

export const Project: FC<IProject> = ({
  className,
  onSubmit,
  initialValues,
  onClose = () => {},
  onEdit = () => {},
}) => {
  const lang = useTranslator();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { required } = useValidators();
  const [initial, setInitial] = useState<IAppartmentProject>({} as IAppartmentProject);

  useEffect(() => {
    if (initialValues) {
      setInitial(initialValues);
      setOpen(true);
    }
  }, [initialValues]);

  const handleFormSubmit = (data: IAppartmentProject) => {
    if (isEmpty(initialValues)) {
      onSubmit(data);
    } else {
      onEdit(data);
    }

    setOpen(false);
    onClose();
    setInitial({} as IAppartmentProject);
  };

  const handleClose = () => {
    setOpen(false);
    setInitial({} as IAppartmentProject);
    onClose();
  };

  return (
    <>
      <Button icon={DashboardIcon} className={className} onClick={() => setOpen(true)} background="gray" text="black">
        {lang.project}
      </Button>
      <Modal
        open={open}
        onClose={() => handleClose()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        className={classes.modal}
      >
        <Fade in={open}>
          <Paper className={classes.paper}>
            <Form
              onSubmit={handleFormSubmit}
              initialValues={initial}
              render={({ handleSubmit, form, values }) => (
                <form onSubmit={handleSubmit} className={classes.form}>
                  <h2>
                    <DashboardIcon /> {lang.project}
                  </h2>
                  <TextField validate={required} name="room" label={lang.roomCount} />
                  <TextField validate={required} name="size" label={lang.size} />
                  <TextField validate={required} name="roomPrice" label={lang.price} />
                  <TextField validate={required} multiline rows={4} name="text" label={lang.about} />
                  <FileField
                    initialPhotos={initial?.image ? [initial?.image] : undefined}
                    priview
                    name="image"
                    validate={required}
                    label={lang.addPhoto}
                    onInitialPhotoRemove={() => setInitial({ ...initial, image: undefined })}
                  />
                  <Button type="submit" className={classes.sendButton}>
                    {lang.add}
                  </Button>
                </form>
              )}
            />
          </Paper>
        </Fade>
      </Modal>
    </>
  );
};
