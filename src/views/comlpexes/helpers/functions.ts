export const addIndex = (data: any, arr: any[]) => {
  const index = arr.length > 0 ? Math.max(...arr.map((i) => i.index)) : 0;
  return { ...data, index: index + 1 };
};

export const insert = (data: any, arr: any[]) => {
  let listsCopy = [...arr];
  let item = listsCopy.filter((i) => i.index === data.index)[0];
  let indx = listsCopy.indexOf(item);
  listsCopy[indx] = data;
  return listsCopy;
};

export const compareByIndex = (a: any, b: any) => {
  if (a.index > b.index) {
    return 1;
  } else if (a.index < b.index) {
    return -1;
  } else {
    return 0;
  }
};
