import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    dataTableButtonsContainer: {
      display: "flex",
      justifyContent: "space-between",
      width: "35%",
      margin: "0 auto",
      "& .MuiButtonBase-root": {
        padding: theme.spacing(0.5),
        marginRight: 5,
        minWidth: "unset",
        "& .MuiSvgIcon-root": {
          fontSize: 25,
        },
      },
    },
    searchForm: {
      display: "flex",
      "& .MuiButtonBase-root": {
        marginLeft: theme.spacing(2),
      },
    },
    clearButton: {
      padding: 0,
      visibility: "hidden",
    },
    clearButtonActive: {
      visibility: "visible",
    },
    logo: {
      height: 70,
      width: 70,
      objectFit: "cover",
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
