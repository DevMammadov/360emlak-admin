export interface IComplexProject {
  complex: string; // complex name
  project: IAppartmentProject[];
}

export interface IApartmentInfo {
  corpusNum: number;
  blokNum: number;
  floorNum: number;
  apartmentNum: number;
  apartmentInFloor: number;
  lifts: number;
}

export interface IAppartmentProject {
  id?: number;
  apartmentId?: number;
  room: number;
  size: number;
  roomPrice: number;
  text: string;
  image?: string;
  index?: number;
}

export interface IComplex {
  id: number;
  nameAz: string;
  nameRu: string;
  nameEn: string;
  phone: string;
  addressAz: string;
  addressRu: string;
  addressEn: string;
  workTime: string[];
  email: string;
  website?: string;
  construct?: string;
  handover: number; // tehvil verilme ili
  views: number;
  images: string[];
  logo: string;
  corpusNum: number;
  blokNum: number;
  floorNum: number;
  apartmentNum: number;
  apartmentInFloor: number;
  lifts: number;
  apartmentProjects: IAppartmentProject[];
  contentAz: string;
  contentEn: string;
  contentRu: string;
}

export interface _IComplex {
  id: number;
  logo: string;
  name: string;
  email: string;
  phone: string;
  address: string;
}

export interface IComplexResponse {
  name?: string;
  limit?: number;
  offset?: number;
}
