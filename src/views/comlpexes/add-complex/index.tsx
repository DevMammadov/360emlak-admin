import React, { FC, Fragment, useCallback, useEffect, useRef, useState } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./add-complex-style";
import {
  SectionHeader,
  List as ListComponent,
  FileField,
  TextField,
  TimePicker,
  DatePicker,
  Button,
  Select,
  Jodit,
  GoogleMap,
  StaticMap,
  TabPanel,
} from "components/shared";
import { Form } from "react-final-form";
import { InfoForm, Project, Editable, ProjectCard } from "../components";
import { IApartmentInfo, IAppartmentProject, IComplex } from "../types";
import { Box, Grid, Paper, Tab, Tabs } from "@material-ui/core";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { isEmpty } from "helpers/functions";
import { useSelectData } from "hooks/useSelectData";
import { useValidators } from "hooks/useValidators";
import ComplexApi from "api/complex.api";
import { useHistory, useParams } from "react-router";
import { addIndex, insert } from "../helpers/functions";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { ILngLat } from "components/shared/google-map/types";
import { links } from "routes/links";
import { useSwal } from "helpers/swal";
import { toast } from "react-toastify";

interface InitialValues {
  info?: IApartmentInfo;
  project?: IAppartmentProject;
}

interface IContents {
  contentAz: string;
  contentRu: string;
  contentEn: string;
}

export interface IAddComplex {}

export const AddComplex: FC<IAddComplex> = ({}) => {
  const lang = useTranslator("complex", ["alerts", "toast"]);
  const classes = useStyles();
  //
  const [info, setInfo] = useState<IApartmentInfo>({} as IApartmentInfo);
  const [projects, setProject] = useState<IAppartmentProject[]>([]);
  const [initialValues, setInitialValues] = useState();
  const [initialLogo, setInitialLogo] = useState<string | undefined>();
  const [initialImages, setInitialImages] = useState();
  const [contents, setContents] = useState<IContents>({ contentAz: "", contentEn: "", contentRu: "" });
  const [mapOpen, setMapOpen] = useState(false);
  const [markerPos, setMarkerPos] = useState<ILngLat | undefined>(undefined);
  const [value, setValue] = React.useState(0);
  const history = useHistory();
  //
  const [initial, setInitial] = useState<InitialValues>({});
  const [photoToRemove, setPhotoToRemove] = useState<string | undefined>();
  const { weekDays, metro } = useSelectData();
  const { required } = useValidators();
  const params: any = useParams();
  const { Alert } = useSwal();

  useEffect(() => {
    if (params.id) {
      ComplexApi.getById(params.id).then((payload) => {
        const {
          apartmentProjects,
          corpusNum,
          blokNum,
          floorNum,
          apartmentNum,
          apartmentInFloor,
          lifts,
          resizedImage,
          ...rest
        } = payload?.data?.data || {};
        setInitialLogo(payload?.data?.data?.logo);
        setInitialImages(payload?.data?.data?.images);
        setContents({
          contentAz: payload?.data?.data?.contentAz,
          contentRu: payload?.data?.data?.contentRu,
          contentEn: payload?.data?.data?.contentEn,
        });
        setInfo({ corpusNum, blokNum, floorNum, apartmentNum, apartmentInFloor, lifts });
        setProject(apartmentProjects);
        setMarkerPos({ lat: Number(payload?.data?.data?.lat), lng: Number(payload?.data?.data?.lng) });
        setInitialValues(rest);
      });
    }
  }, [params.id]);

  const renderContent = () => {
    return (
      <div className={classes.contentForm}>
        <InfoForm
          onSubmit={(data) => setInfo(data)}
          onEdit={(data) => setInfo(data)}
          initialValues={initial.info}
          onClose={() => setInitial({})}
        />
        <Project
          initialValues={initial.project}
          onClose={() => setInitial({})}
          onSubmit={(data) => setProject([...projects, addIndex(data, projects)])}
          onEdit={(data) => setProject(insert(data, projects))}
        />
      </div>
    );
  };

  const toFormData = (response: any) => {
    let formData = new FormData();

    for (let key of Object.keys(response)) {
      let item: any = response[key as keyof typeof response];
      if (item) {
        if (Array.isArray(item)) {
          for (let itemIndex in item) {
            if (typeof item[itemIndex] === "object" && !item[itemIndex]?.stream) {
              for (let subKey in item[itemIndex]) {
                formData.append(`${key}[${itemIndex}].${subKey}`, item[itemIndex][subKey]);
              }
            } else {
              formData.append(key, item[itemIndex]);
            }
          }
        } else if (typeof response[key] === "object" && !response[key]?.stream) {
          for (let subKey in response[key]) {
            formData.append(`${key}.${subKey}`, response[key][subKey]);
          }
        } else {
          formData.append(key, item || "");
        }
      }
    }

    return formData;
  };

  const handleFormSubmit = (data: any) => {
    const { dayFrom, dayTo, timeFrom, timeTo, ...rest } = data;

    let response: IComplex = {
      ...rest,
      workTime: [timeFrom.toString(), timeTo.toString()],
      handover: data.handover?.toString(),
      weeklyWorkTime: [Number(dayFrom), Number(dayTo)],
      apartmentProjects: projects,
      images: data.images?.filter((i: any) => typeof i === "object"),
      logo: typeof data?.logo === "object" ? data?.logo : undefined,
      lat: markerPos?.lat.toString(),
      lng: markerPos?.lng.toString(),
      ...contents,
      ...info,
    };
    if (!params.id) {
      ComplexApi.add(toFormData(response)).then((payload) => {
        if (payload.status === 201) {
          toast.success(lang.complexAdded);
          history.push(links.complexes.baseUrl);
        }
      });
    } else {
      ComplexApi.update(params.id, toFormData(response)).then((payload) => {
        if (payload.status === 200) {
          toast.success(lang.infoUpdated);
          history.push(links.complexes.baseUrl);
        }
      });
    }
  };

  const getInitialVals = (initial: any) => {
    return {
      ...initial,
      timeFrom: initial?.workTime && initial?.workTime[0],
      timeTo: initial?.workTime && initial?.workTime[1],
      dayFrom: initial?.weeklyWorkTime && initial?.weeklyWorkTime[0],
      dayTo: initial?.weeklyWorkTime && initial?.weeklyWorkTime[1],
    };
  };

  const handleSliderPhotoremove = (file: string) => {
    Alert.fire({
      text: lang.removePhotoAlert,
    }).then((result) => {
      if (result.value) {
        setPhotoToRemove(file);
        ComplexApi.removeSliderPhoto(params.id, file)
          .then(() => setPhotoToRemove(undefined))
          .then(() => {
            ComplexApi.getById(params.id).then((payload) => {
              if (payload?.data?.data) {
                setInitialImages(payload?.data?.data?.images);
              }
            });
          });
      }
    });
  };

  return (
    <>
      <SectionHeader renderContent={renderContent} title={lang.addingComplex} />
      <Form
        onSubmit={handleFormSubmit}
        initialValues={getInitialVals(initialValues)}
        render={({ handleSubmit, form, values }) => (
          <form onSubmit={handleSubmit}>
            <Grid container component={Paper} className={classes.container}>
              <Grid item xs={12} lg={4} xl={3} className={classes.fieldsContainer}>
                <FileField
                  validate={required}
                  initialPhotos={initialLogo ? [initialLogo] : undefined}
                  onInitialPhotoRemove={() => setInitialLogo(undefined)}
                  name="logo"
                  priview
                  label={lang.uploadLogo}
                />
                <TextField validate={required} name="phone" label={lang.phone} />
                <Select name="metro" data={metro} label={lang.nearMetro} />
                <div className={classes.inputContainer}>
                  <Select
                    validate={required}
                    name="dayFrom"
                    data={weekDays}
                    label={`${lang.workDays} (${lang.from})`}
                  />
                  <Select validate={required} name="dayTo" data={weekDays} label={`${lang.workDays} (${lang.to})`} />
                </div>
                <div className={classes.inputContainer}>
                  <TimePicker name="timeFrom" label={`${lang.workTime} (${lang.from})`} />
                  <TimePicker name="timeTo" label={`${lang.workTime} (${lang.to})`} />
                </div>
                <TextField validate={required} name="email" label={lang.email} />
                <TextField validate={required} name="website" label={lang.website} />
                <TextField validate={required} name="construct" label={lang.construct} />
                <DatePicker validate={required} name="handover" label={lang.handover} />
                <div className={classes.inputContainer}>
                  <TextField validate={required} name="minPrice" type="number" label={lang.minPrice} />
                  <TextField validate={required} name="maxPrice" type="number" label={lang.maxPrice} />
                </div>
                <Button
                  text="white"
                  icon={LocationOnIcon}
                  className={classes.showMapButton}
                  background="main"
                  onClick={() => setMapOpen(!mapOpen)}
                >
                  {lang.showOnMap}
                </Button>
                <GoogleMap
                  initial={markerPos}
                  open={mapOpen}
                  onClose={() => setMapOpen(!mapOpen)}
                  onSubmit={(data) => setMarkerPos(data)}
                />
                {markerPos && (
                  <Grid item xs={12}>
                    <StaticMap position={markerPos} className={classes.staticMap} />
                  </Grid>
                )}
              </Grid>
              <Grid item xs={12} lg={8} xl={9} className={classes.infoContainer}>
                <Grid container>
                  <Grid item xs={12}>
                    <FileField
                      name="images"
                      initialPhotos={initialImages}
                      validate={required}
                      multiple
                      priview
                      onInitialPhotoRemove={handleSliderPhotoremove}
                      label={lang.uploadSliderImages}
                    />
                    <div className={classes.tabContainer}>
                      <Box mb={1} mt={2} pl={2} display="flex" justifyContent="space-between" alignItems="center">
                        {lang.translateable}
                      </Box>
                      <Tabs
                        value={value}
                        centered
                        variant="standard"
                        className={classes.tabs}
                        onChange={(e, v) => setValue(v)}
                        aria-label="simple tabs example"
                      >
                        <Tab label="Azərbaycan dili" />
                        <Tab label="Русский Язык" />
                        <Tab label="Enlish language" />
                      </Tabs>
                      <TabPanel value={value} index={0}>
                        <Grid container>
                          <Grid item xs={6}>
                            <TextField validate={required} name="nameAz" label={`${lang.name} (Az)`} />
                          </Grid>
                          <Grid item xs={6}>
                            <TextField name="addressAz" label={`${lang.address} (Az)`} />
                          </Grid>
                          <Grid item xs={12}>
                            <Jodit
                              label={`${lang.complexPageContent} (Az)`}
                              value={contents.contentAz}
                              onBlur={(contentAz) => setContents({ ...contents, contentAz })}
                            />
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={1}>
                        <Grid container>
                          <Grid item xs={6}>
                            <TextField validate={required} name="nameRu" label={`${lang.name} (Ru)`} />
                          </Grid>
                          <Grid item xs={6}>
                            <TextField name="addressRu" label={`${lang.address} (Ru)`} />
                          </Grid>
                          <Grid item xs={12}>
                            <Jodit
                              label={`${lang.complexPageContent} (Ru)`}
                              value={contents.contentRu}
                              onBlur={(contentRu) => setContents({ ...contents, contentRu })}
                            />
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={2}>
                        <Grid container>
                          <Grid item xs={6}>
                            <TextField validate={required} name="nameEn" label={`${lang.name} (En)`} />
                          </Grid>
                          <Grid item xs={6}>
                            <TextField name="addressEn" label={`${lang.address} (En)`} />
                          </Grid>
                          <Grid item xs={12}>
                            <Jodit
                              label={`${lang.complexPageContent} (En)`}
                              value={contents.contentEn}
                              onBlur={(contentEn) => setContents({ ...contents, contentEn })}
                            />
                          </Grid>
                        </Grid>
                      </TabPanel>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    {!isEmpty(info) && (
                      <Editable
                        onEdit={() => setInitial({ ...initial, info })}
                        onRemove={() => setInfo({} as IApartmentInfo)}
                      >
                        <ListComponent
                          checkLabel
                          checkValue
                          valueAlign="left"
                          data={[
                            { label: info?.corpusNum, value: lang.corpus },
                            { label: info?.blokNum, value: lang.blok },
                            { label: info?.lifts, value: lang.lift },
                            { label: info?.apartmentNum, value: lang.apartment },
                            { label: info?.floorNum, value: lang.floor },
                            { label: info?.apartmentInFloor, value: lang.apartmentInFloor },
                          ]}
                          icon={ArrowRightIcon}
                          iconColor="main"
                          className={classes.appartmentInfoList}
                        />
                      </Editable>
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    {projects?.map((project, i) => (
                      <Editable
                        key={i}
                        onEdit={() => setInitial({ ...initial, project })}
                        onRemove={() => setProject(projects.filter((p) => p !== project))}
                      >
                        <ProjectCard {...project} />
                      </Editable>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.sendButton}>
                <Button type="submit"> {lang.send} </Button>
              </Grid>
            </Grid>
          </form>
        )}
      />
    </>
  );
};
