import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    contentForm: {
      "& > .MuiButtonBase-root": {
        marginLeft: theme.spacing(2),
      },
    },
    tabContainer: {
      "& .MuiTypography-root": {
        "& .MuiFormControl-root": {
          marginBottom: theme.spacing(2),
        },
      },
      "& .MuiGrid-item": {
        padding: theme.spacing(1),
      },
    },
    tabs: {
      "& button": {
        textTransform: "inherit",
      },
    },
    paper: {
      display: "flex",
      justifyContent: "center",
    },
    container: {
      padding: theme.spacing(4, 0),
    },
    appartmentInfoList: {
      "& .pair-list-body": {
        display: "flex",
      },
      "& .pair-list-row": {
        paddingRight: theme.spacing(2),
        fontWeight: "bold",
      },
      "& .pair-list-icon": {
        fontSize: 30,
      },
      marginBottom: theme.spacing(2),
    },
    videoSection: {
      "& h3": {
        marginBottom: theme.spacing(2),
      },
      "& > div": {},
    },
    fieldsContainer: {
      borderRight: `1px solid ${theme.palette.color.borderColor}`,
      padding: theme.spacing(0, 2),
      "& .imgContainer": {
        "& div": {
          width: "100%",
          "& img": {
            width: "100%",
            height: 140,
          },
        },
      },
      "& > div": {
        marginBottom: theme.spacing(2),
      },
    },
    infoContainer: {
      padding: theme.spacing(0, 4),
    },
    sendButton: {
      display: "flex",
      justifyContent: "center",
      "& .MuiButtonBase-root": {
        padding: theme.spacing(1, 10),
      },
    },
    inputContainer: {
      display: "flex",
      "& > div:first-child": {
        marginRight: theme.spacing(1),
      },
    },
    showMapButton: {
      margin: "0 auto",
      display: "flex",
      marginBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
