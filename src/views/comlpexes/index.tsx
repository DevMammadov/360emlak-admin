import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./complexes.style";
import { useTranslator } from "localization";
import { Autocomplete, Button, DataTable, Select, TextField } from "components/shared";
import { SectionHeader } from "components/shared/section-header";
import { Form } from "react-final-form";
import { useSelectData } from "hooks/useSelectData";
import { IconButton, TableCell } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { _IComplex } from "./types";
import { getLimitOffset } from "helpers/functions";
import { useColumns } from "./table-data";
import { useHistory } from "react-router";
import { links } from "routes/links";
import { useSwal } from "helpers/swal";
import ComplexApi from "api/complex.api";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import clsx from "clsx";

export interface IComplexes {}

export const Complexes: FC<IComplexes> = () => {
  const lang = useTranslator("main", ["filter", "alerts"]);
  const { AddFilter } = useSelectData();
  const [complexes, setComplexes] = useState<{ totalCount: number; apartmentCards: _IComplex[] }>({
    totalCount: 0,
    apartmentCards: [],
  });
  const [page, setPage] = useState(1);
  const [changed, setChanged] = useState(false);
  const [name, setName] = useState("");
  const classes = useStyles();
  const history = useHistory();
  const { Alert } = useSwal();
  const limit = 5;

  useEffect(() => {
    ComplexApi.getAll({
      name,
      ...getLimitOffset(page, limit),
    }).then((payload) => setComplexes(payload?.data?.data));
  }, [name, page, changed]);

  const handleSearch = (data: any) => {
    setName(data.name.trim());
  };

  const renderContent = () => {
    return (
      <Form
        onSubmit={handleSearch}
        initialValues={{ filter: 0 }}
        render={({ handleSubmit, form }) => (
          <form onSubmit={handleSubmit} className={classes.searchForm}>
            <TextField
              name="name"
              InputProps={{
                endAdornment: (
                  <IconButton
                    className={clsx(classes.clearButton, name.length > 0 && classes.clearButtonActive)}
                    onClick={() => {
                      setName("");
                      form.reset();
                    }}
                  >
                    <HighlightOffIcon />
                  </IconButton>
                ),
              }}
              placeholder={lang.complexName}
              fullWidth
            />
            <Button for="search" type="submit" variant="contained">
              {lang.search}
            </Button>
          </form>
        )}
      />
    );
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  const handleRemove = (id: string) => {
    Alert.fire({
      text: lang.removeComplexAlert,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        ComplexApi.remove(id).then(() => setChanged(!changed));
      }
    });
  };

  const handleEdit = (id: string) => {
    history.push(`${links.complexes.edit}/${id}`);
  };

  return (
    <section>
      <SectionHeader
        renderContent={renderContent}
        onAddClick={() => history.push(links.complexes.add)}
        title={lang.announces}
        addButton
      />
      <div>
        <DataTable
          data={complexes?.apartmentCards || []}
          columns={useColumns(handleRemove, handleEdit)}
          options={{ toolbar: false, pageSize: limit }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(complexes?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
