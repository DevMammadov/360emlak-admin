import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Button } from "components/shared";
import { getPhoto } from "helpers/functions";
import { useUser } from "hooks/useUser";
import { useTranslator } from "localization";
import React from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "./complexes.style";
import { _IComplex } from "./types";

export const useColumns = (handleRemove: Function, handleEdit: Function) => {
  const lang = useTranslator("agencies", ["publishStatus"]);
  const history = useHistory();
  const classes = useStyles();
  const currentUser = useUser();

  return [
    {
      title: lang.logoOf,
      field: "logo",
      render: (rowData: _IComplex) => <img src={getPhoto(rowData.logo)} className={classes.logo} alt={rowData.name} />,
    },
    {
      title: lang.nameOf,
      field: "name",
    },
    { title: lang.email, field: "email" },
    { title: lang.phone, field: "phone" },
    { title: lang.address, field: "address" },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: _IComplex) => (
        <div className={classes.dataTableButtonsContainer}>
          <Button onClick={() => handleEdit(rowData.id)} variant="contained">
            <EditIcon />
          </Button>
          {currentUser.roles === "admin" && (
            <Button onClick={() => handleRemove(rowData.id)} variant="contained" background="red" text="white">
              <DeleteIcon />
            </Button>
          )}
        </div>
      ),
    },
  ];
};
