import React, { FC } from "react";
import { useStyles } from "./user-list.style";
import { useTranslator } from "localization";
import { Avatar, List, ListItem, ListItemAvatar, ListItemText, Paper } from "@material-ui/core";
import noImg from "assets/userNoImage.png";
import { useHistory } from "react-router";
import { links } from "routes/links";
import { ILastUsers } from "views/main/types";
import { getPhoto } from "helpers/functions";

export interface IUserList {
  data: ILastUsers[];
}

export const UserList: FC<IUserList> = ({ data }) => {
  const lang = useTranslator("main");
  const classes = useStyles();
  const history = useHistory();

  return (
    <Paper className={classes.root}>
      <h3>{lang.lastRegisteredUsers}</h3>
      <List className={classes.root}>
        {data?.map((user) => (
          <ListItem
            key={user.id}
            button
            onClick={() => history.push(`${user.iamAgent ? links.agencies.edit : links.users.edit}/${user.id}`)}
          >
            <ListItemAvatar>
              <Avatar>
                <img
                  src={getPhoto(user.photo) || noImg}
                  alt="user"
                  className={classes.profileImg}
                  onError={(e: any) => (e.target.src = noImg)}
                />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={`${user.name} ${user.surname || ""}`}
              secondary={`${user.email} - ${user.iamAgent ? "agent" : "user"}`}
            />
          </ListItem>
        ))}
      </List>
    </Paper>
  );
};
