import React, { FC } from "react";
import { useTranslator } from "localization";
import { useStyles } from "./announce-list.style";
import { IAnnounce } from "views/main/types";
import { Avatar, List, ListItem, ListItemAvatar, ListItemText, Paper } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { links } from "routes/links";
import noImg from "assets/noimg.png";
import { useAddName } from "helpers/for-priject";
import { IAdd } from "views/announces/types";
import { getPhoto } from "helpers/functions";

export interface IAnnounceList {
  data: IAnnounce[];
}

export const AnnounceList: FC<IAnnounceList> = ({ data }) => {
  const lang = useTranslator("main");
  const classes = useStyles();
  const history = useHistory();
  const { addName } = useAddName();

  return (
    <Paper className={classes.root}>
      <h3>{lang.lastRegisteredAnnounces}</h3>
      <List className={classes.root}>
        {data?.map((add) => (
          <ListItem key={add.id} button onClick={() => history.push(`${links.announces.edit}/${add.id}`)}>
            <ListItemAvatar>
              <Avatar className={classes.profileImg}>
                <img src={getPhoto(add.image) || noImg} onError={(e: any) => (e.target.src = noImg)} alt="user" />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={addName(add as any, ["area"])} secondary={add.insertDate} />
          </ListItem>
        ))}
      </List>
    </Paper>
  );
};
