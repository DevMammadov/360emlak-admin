import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    root: {
      padding: theme.spacing(1, 0, 0, 0),
      "& h3": {
        margin: 0,
        paddingLeft: theme.spacing(2),
        color: theme.palette.color.main,
      },
    },
    profileImg: {
      "& img": {
        //width: 65,
        width: "100%",
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
