import React, { FC } from "react";
import { useStyles } from "./statistic-card.style";
import { useTranslator } from "localization";
import { Line, Bar } from "react-chartjs-2";
import { Paper, useTheme } from "@material-ui/core";
import { useSelectData } from "hooks/useSelectData";
import { IColor } from "theme/types";

export interface IStatisticCard {
  title?: string;
  data: number[];
  backgroundColor?: keyof IColor;
  borderColor?: keyof IColor;
  count?: number;
}

export const StatisticCard: FC<IStatisticCard> = ({ title, data, backgroundColor, borderColor, count = 0 }) => {
  const lang = useTranslator();
  const classes = useStyles();
  const { mounth } = useSelectData();
  const theme = useTheme();

  const Carddata = {
    datasets: [
      {
        data: mounth.map((m, i) => ({ x: m.label, y: data ? data[i] : 0 })),
        label: `${title} (${count})`,
        backgroundColor: backgroundColor || theme.palette.color.main,
        borderColor: borderColor || theme.palette.color.borderColor,
      },
    ],
  };

  const options = {
    parsing: false,
    scales: {
      y: {
        ticks: {
          stepSize: 1,
        },
      },
    },
  };

  return (
    <Paper>
      <Bar data={Carddata} options={options} type="line" />
    </Paper>
  );
};
