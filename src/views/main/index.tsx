import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./main.style";
import { useTranslator } from "localization";
import { Grid, Paper } from "@material-ui/core";
import { AnnounceList, StatisticCard, UserList } from "./components";
import { useUser } from "hooks/useUser";
import MainApi from "api/main.api";
import MatterportApi from "api/matterport.api";
import { ILastUsers, IMonthly, IStatistics } from "./types";

export interface IMain {}

export const Main: FC<IMain> = () => {
  const lang = useTranslator("main");
  const classes = useStyles();
  const currentUser = useUser();
  const [statistics, setStatistics] = useState<IStatistics>({} as IStatistics);

  useEffect(() => {
    MainApi.getMonthly().then((payload) => setStatistics((prev) => ({ ...prev, monthly: payload?.data?.data })));
    MainApi.getLastUsers().then((payload) => setStatistics((prev) => ({ ...prev, lastUsers: payload?.data?.data })));
    MainApi.getCounts().then((payload) => setStatistics((prev) => ({ ...prev, counts: payload?.data?.data })));
    MainApi.getAnnounces().then((payload) => setStatistics((prev) => ({ ...prev, announces: payload?.data?.data })));
    MatterportApi.getModels();
  }, []);

  return (
    <Grid container className={classes.container}>
      <Grid item xs={4}>
        <StatisticCard
          data={statistics.monthly?.announce}
          count={statistics.counts?.countAnnounce}
          title={lang.announces}
        />
      </Grid>
      <Grid item xs={4}>
        <StatisticCard
          backgroundColor="orange"
          count={statistics.counts?.countAgent}
          data={statistics.monthly?.agent}
          title={lang.agencies}
        />
      </Grid>
      <Grid item xs={4}>
        <StatisticCard
          data={statistics.monthly?.apartment}
          count={statistics.counts?.countApartment}
          title={lang.complexes}
        />
      </Grid>
      <Grid item xs={4}>
        <UserList data={statistics.lastUsers} />
      </Grid>
      <Grid item xs={4}>
        <AnnounceList data={statistics.announces} />
      </Grid>
    </Grid>
  );
};
