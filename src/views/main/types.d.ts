export interface IMonthly {
  announce: number[];
  agent: number[];
  apartment: number[];
}

export interface ILastUsers {
  registrationDate: string;
  name: string;
  surname: string;
  iamAgent: boolean;
  id: string;
  email: string;
  photo: string;
}

export interface ICounts {
  countUser: number;
  countAnnounce: number;
  countAgent: number;
  countApartment: number;
}

export interface IAnnounce {
  id: string;
  city: string;
  region: string;
  settlement: string;
  propType: string;
  salesType: string;
  metro: string;
  rooms: string;
  image: string;
  address: string;
  insertDate: string;
}

export interface IStatistics {
  counts: ICounts;
  lastUsers: ILastUsers[];
  monthly: IMonthly;
  announces: IAnnounce[];
}
