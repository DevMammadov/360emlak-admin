import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    formPaper: {
      maxWidth: 750,
      margin: "0 auto",
      padding: theme.spacing(3, 10, 6, 10),
      "& > .MuiGrid-item": {
        marginBottom: theme.spacing(2),
        padding: theme.spacing(0, 1),
        display: "flex",
        alignItems: "flex-end",
      },
    },
    searchContainer: {
      padding: theme.spacing(3, 10, 2, 10),
      marginBottom: theme.spacing(2),
      "& .MuiGrid-item": {
        marginBottom: theme.spacing(0),
        padding: theme.spacing(0, 1),
        alignItems: "center",
        "& img": {
          height: 80,
          width: 80,
          marginRight: 16,
        },
      },
    },
    showMapButton: {
      maxWidth: "unset",
    },
    sendButton: {
      padding: theme.spacing(1, 5),
      margin: "0 auto",
    },
    autocompleteOption: {
      display: "flex",
      alignItems: "center",
      "& img": {
        height: 40,
        width: 40,
        marginRight: 5,
      },
    },
    staticMap: {
      margin: "0 auto",
      display: "block",
    },
    tabs: {
      marginTop: theme.spacing(1),
      "& button": {
        textTransform: "inherit",
      },
    },
    tabContainer: {
      "& .MuiTypography-root": {
        "& .MuiFormControl-root": {
          marginBottom: theme.spacing(2),
        },
      },
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {
      staticMap: {
        width: "100%",
      },
    },
    [theme.breakpoints.down("xs")]: {},
  };
});
