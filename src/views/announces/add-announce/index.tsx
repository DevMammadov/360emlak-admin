import { Box, Grid, ListItemText, Paper, Tab, Tabs } from "@material-ui/core";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { createFilterOptions } from "@material-ui/lab";
import AddApi from "api/announces.api";
import { default as UserApi, default as UsersApi } from "api/users.api";
import { default as userNoImage, default as userNoImg } from "assets/userNoImage.png";
import clsx from "clsx";
import {
  Autocomplete,
  Button,
  FileField,
  GoogleMap,
  Radios,
  SectionHeader,
  Select,
  StaticMap,
  TabPanel,
  TextField,
} from "components/shared";
import { ILngLat } from "components/shared/google-map/types";
import { checkValue, getPhoto, removeUndefined, toNomber } from "helpers/functions";
import { useSwal } from "helpers/swal";
import { useSelectData } from "hooks/useSelectData";
import { useValidators } from "hooks/useValidators";
import { useTranslator } from "localization";
import { Checkboxes } from "mui-rff";
import React, { FC, useEffect, useState } from "react";
import { Form } from "react-final-form";
import { useHistory, useParams } from "react-router";
import { toast } from "react-toastify";
import { links } from "routes/links";
import { IAdd, IAddReqModel } from "../types";
import { useStyles } from "./add.style";
import { RenderField } from "./helper";

export interface IAddAnnounce {}

export interface IUser {
  id: string;
  fullName: string;
  iamAgent: boolean;
  email: string;
  photo: string;
}

export const AddAnnounce: FC<IAddAnnounce> = () => {
  const lang = useTranslator("toast", ["filter", "announces"]);
  const classes = useStyles();
  const data = useSelectData();
  const { required } = useValidators();
  const [mapOpen, setMapOpen] = useState(false);
  const [markerPos, setMarkerPos] = useState<ILngLat | undefined>(undefined);
  const [initialPhotos, setInitialPhotos] = useState<string[] | undefined>();
  const [initialValues, setinitialValues] = useState<IAdd>({} as IAdd);
  const [sender, setSender] = useState<IUser>({} as IUser);
  const [users, setUsers] = useState<IUser[]>([]);
  const [value, setValue] = React.useState(0);
  const [photoToRemove, setPhotoToRemove] = useState<string | undefined>();
  const [loading, setLoading] = useState(false);
  const params: any = useParams();
  const history = useHistory();
  const { Alert } = useSwal();

  useEffect(() => {
    if (params.id) {
      setLoading(true);
      AddApi.getAddById(params.id)
        .then((payload) => {
          const response = payload?.data?.data;
          if (response) {
            const images = response.is360 ? (response.images[1] ? [response.images[1]] : undefined) : response.images;
            setinitialValues({ ...response, images, link: images ? response?.images[0] : "" });
            setInitialPhotos(images);
            setMarkerPos({ lat: Number(response?.lat), lng: Number(response.lng) });
          }
          if (response?.userId) {
            UserApi.getUseById(response?.userId).then((p) => {
              if (p?.data) setSender(p?.data);
            });
          }
          setLoading(false);
        })
        .catch(() => setLoading(false));
    }
  }, [params]);

  const handleFormSubmit = (data: any) => {
    const formData = new FormData();
    const { docInfo, images, ...rest } = data;

    const reqModel: IAddReqModel = {
      ...rest,
      mortgage: data.docInfo && data.docInfo.includes("mortgage"),
      document: data.docInfo && data.docInfo.includes("document"),
      isPremium: data.docInfo && data.docInfo.includes("isPremium"),
      is360: data.is360 && !!data.is360[0],
      price: toNomber(data.price),
      area: toNomber(data.area),
      rooms: toNomber(data.rooms),
      landArea: toNomber(rest.landArea),
      homeFloor: toNomber(data.homeFloor),
      buildingFloor: toNomber(data.buildingFloor),
      lat: markerPos?.lat.toString(),
      lng: markerPos?.lng.toString(),
    };

    for (let key of Object.keys(reqModel)) {
      if (reqModel[key as keyof IAddReqModel]) {
        formData.append(key, checkValue(reqModel[key as keyof IAddReqModel]));
      }
    }

    if (Array.isArray(images)) {
      for (let image of images || []) {
        formData.append("images", image);
      }
    } else {
      formData.append("images", images);
    }

    if (params.id) {
      AddApi.update(params.id, formData).then((data) => {
        if (data?.statusCode === 200) {
          toast.success(lang.announceUpdated);
          history.push(links.announces.baseUrl);
        } else {
          toast.error(lang.announceNotAdded);
        }
      });
    } else {
      AddApi.newAdd(formData).then((data) => {
        if (data?.statusCode === 200) {
          toast.success(lang.announceAdded);
          history.push(links.announces.baseUrl);
        } else {
          toast.error(lang.announceNotAdded);
        }
      });
    }
  };

  const getUser = (name: string) => {
    if (name.length > 2) {
      UsersApi.getUserList(name).then((data) => setUsers(data?.data || []));
    } else {
      setUsers([]);
    }
  };

  const filterOptions = createFilterOptions({
    matchFrom: "start",
    stringify: (option: IUser) => option.email,
  });

  const getInitialVals = (initial: any) => {
    const { images, ...rest } = initial;
    return {
      ...removeUndefined(rest),
      docInfo: [initial.mortgage && "mortgage", initial.document && "document", initial.isPremium && "isPremium"],
      is360: [initial.is360 && "is360"],
    };
  };

  const removePhoto = (file: string) => {
    Alert.fire({
      text: lang.removePhotoAlert,
    }).then((result) => {
      if (result.value) {
        setPhotoToRemove(file);
        AddApi.removePhoto({ id: initialValues.id, file })
          .then(() => setPhotoToRemove(undefined))
          .then(() => {
            AddApi.getAddById(params.id).then((payload) => {
              const response = payload?.data?.data;
              if (response) {
                setInitialPhotos(response.is360 ? undefined : payload?.data?.data?.images);
              }
            });
          });
      }
    });
  };

  return (
    <section>
      <SectionHeader title={lang.announceAdding} />
      <Form
        onSubmit={handleFormSubmit}
        keepDirtyOnReinitialize
        initialValues={params.id ? getInitialVals(initialValues) : { propType: "1", salesType: "1", city: "8" }}
        render={({ handleSubmit, values, form }) => (
          <form onSubmit={handleSubmit}>
            <Grid component={Paper} container className={clsx(classes.formPaper, classes.searchContainer)}>
              <Grid item xs={12}>
                {params.id ? (
                  <>
                    <img src={sender?.photo ? getPhoto(sender.photo) : userNoImg} alt={sender?.fullName} />
                    <ListItemText
                      primary={sender?.fullName || initialValues.name}
                      secondary={`${sender?.email || initialValues.email} - ${
                        !!sender.iamAgent ? (sender?.iamAgent ? "Agent" : "User") : "Qeydiyyatsız user"
                      }`}
                    />
                  </>
                ) : (
                  <Autocomplete
                    options={users || []}
                    name="userId"
                    validate={required}
                    getOptionValue={(data) => data.id}
                    getOptionLabel={(data) => data.fullName}
                    textFieldProps={{ onChange: (e) => getUser(e.target.value) }}
                    filterOptions={filterOptions}
                    renderOption={(data) => (
                      <div className={classes.autocompleteOption}>
                        <img src={data?.iamAgent ? getPhoto(data?.photo) : userNoImage} />
                        <ListItemText
                          primary={data.fullName}
                          secondary={`${data.email} - ${data.iamAgent ? "agent" : "user"}`}
                        />
                      </div>
                    )}
                    label={lang.selectAddingUser}
                    fullWidth
                  />
                )}
              </Grid>
            </Grid>
            <Grid component={Paper} container className={classes.formPaper}>
              <Grid item xs={12} md={6}>
                <Select native name="propType" validate={required} label={lang.typeOfProperty} data={data.propType} />
              </Grid>
              <Grid item xs={12} md={6}>
                <Select native name="salesType" label={lang.announceType} validate={required} data={data.salesType} />
              </Grid>
              <Grid item xs={12}>
                <Select native name="city" validate={required} data={data.city} label={lang.cities} />
              </Grid>

              <RenderField form={form} name="region" range={[8]} value={values.city}>
                <Grid item xs={12} md={6}>
                  <Select native name="region" emptyLabel=" " label={lang.regions} data={data.region} />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Select
                    native
                    name="settlement"
                    emptyLabel=" "
                    label={lang.settlements}
                    data={data.settlements(values.region)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Select native name="metro" emptyLabel=" " label={lang.metroStations} data={data.metro} />
                </Grid>
              </RenderField>
              <RenderField form={form} name="buildingType" range={[4]} value={values.propType}>
                <Grid item xs={12}>
                  <Radios
                    name="buildingType"
                    label={lang.buildingType}
                    color="primary"
                    radioGroupProps={{ row: true }}
                    data={[
                      { label: lang["b1"], value: "1" },
                      { label: lang["b2"], value: "2" },
                      { label: lang["b3"], value: "3" },
                    ]}
                  />
                </Grid>
              </RenderField>
              <RenderField form={form} name="rooms" range={[1, 2, 3, 4]} value={values.propType}>
                <Grid item xs={12}>
                  <Select
                    native
                    name="rooms"
                    emptyLabel=" "
                    validate={required}
                    label={lang.numberOfRooms}
                    data={data.rooms}
                  />
                </Grid>
              </RenderField>
              <Grid item xs={12}>
                <TextField name="area" validate={required} type="number" label={lang.area} />
              </Grid>

              <RenderField form={form} name="landArea" range={[3, 5]} value={values.propType}>
                <Grid item xs={12}>
                  <TextField name="landArea" type="number" label={lang.landArea} />
                </Grid>
              </RenderField>
              <RenderField form={form} name="homeFloor" range={[1, 2]} value={values.propType}>
                <Grid item xs={12} md={6}>
                  <TextField name="homeFloor" validate={required} type="number" label={lang.floor} />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField name="buildingFloor" validate={required} type="number" label={lang.numberOfFloors} />
                </Grid>
              </RenderField>
              <Grid item xs={12} md={12}>
                <TextField name="price" validate={required} type="number" label={lang.price} />
              </Grid>
              <Grid item xs={12} style={{ display: "block" }}>
                <Box mb={0} display="flex" justifyContent="space-between" alignItems="center">
                  {lang.translateable}
                </Box>
                <div className={classes.tabContainer}>
                  <Tabs value={value} variant="fullWidth" className={classes.tabs} onChange={(e, v) => setValue(v)}>
                    <Tab label="Azərbaycan dili *" />
                    <Tab label="Русский Язык" />
                    <Tab label="Enlish language" />
                  </Tabs>
                  <TabPanel boxProps={{ pb: 0 }} value={value} index={0}>
                    <Grid container>
                      <Grid item xs={12}>
                        <TextField name="addressAz" validate={required} label={`${lang.address} (Az)`} />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField
                          multiline
                          rows={4}
                          rowsMax={15}
                          validate={required}
                          name="aboutAz"
                          label={`${lang.info} (Az)`}
                        />
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel boxProps={{ pb: 0 }} value={value} index={1}>
                    <Grid container>
                      <Grid item xs={12}>
                        <TextField name="addressRu" label={`${lang.address} (Ru)`} />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField multiline rows={4} rowsMax={15} name="aboutRu" label={`${lang.info} (Ru)`} />
                      </Grid>
                    </Grid>
                  </TabPanel>
                  <TabPanel boxProps={{ pb: 0 }} value={value} index={2}>
                    <Grid container>
                      <Grid item xs={12}>
                        <TextField name="addressEn" label={`${lang.address} (En)`} />
                      </Grid>
                      <Grid item xs={12}>
                        <TextField multiline rows={4} rowsMax={15} name="aboutEn" label={`${lang.info} (En)`} />
                      </Grid>
                    </Grid>
                  </TabPanel>
                </div>
              </Grid>
              <Grid item xs={12} md={12}>
                <Button
                  text="white"
                  icon={LocationOnIcon}
                  className={classes.showMapButton}
                  background="main"
                  onClick={() => setMapOpen(!mapOpen)}
                >
                  {lang.showOnMap}
                </Button>
                <GoogleMap
                  initial={markerPos}
                  open={mapOpen}
                  onClose={() => setMapOpen(!mapOpen)}
                  onSubmit={(data) => setMarkerPos(data)}
                />
              </Grid>
              {markerPos && (
                <Grid item xs={12}>
                  <StaticMap position={markerPos} className={classes.staticMap} />
                </Grid>
              )}
              <Grid item xs={12} md={12}>
                <Checkboxes
                  name="docInfo"
                  color="primary"
                  formGroupProps={{ row: true }}
                  data={[
                    { label: lang.hasMortgage, value: "mortgage" },
                    { label: lang.hasDocument, value: "document" },
                    { label: lang.premium, value: "isPremium" },
                  ]}
                />
                <Checkboxes
                  name="is360"
                  color="primary"
                  formGroupProps={{ row: true }}
                  data={[{ label: lang.add360, value: "is360" }]}
                />
              </Grid>
              <RenderField form={form} name="link" condition={values?.is360?.includes("is360")}>
                <Grid item xs={12}>
                  <TextField name="link" validate={required} label={lang.matterportLink} />
                </Grid>
              </RenderField>
              <Grid item xs={12}>
                <FileField
                  name="images"
                  priview
                  multiple={!values?.is360?.includes("is360")}
                  accept=".jpg, .png, .jfif"
                  label={lang.addPhoto}
                  loaderFor={photoToRemove}
                  initialPhotos={initialPhotos || undefined}
                  onInitialPhotoRemove={removePhoto}
                />
              </Grid>
              <Button type="submit" className={classes.sendButton}>
                {lang.send}
              </Button>
            </Grid>
          </form>
        )}
      />
    </section>
  );
};
