import { FormApi } from "final-form";
import React, { FC } from "react";

export const RenderField: FC<{
  form: FormApi<any, Partial<any>>;
  name: string;
  condition?: boolean;
  range?: any;
  value?: string;
}> = ({ form, name, condition = false, range, value, children }) => {
  if (condition || range?.includes(Number(value))) {
    return <>{children}</>;
  } else {
    form.change(name, undefined);
    return <></>;
  }
};
