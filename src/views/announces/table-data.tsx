import { useTranslator } from "localization";
import React from "react";
import { Badge, Chip, IconButton } from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";
import { useStyles } from "./announces.style";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { IAdd, IAddTable } from "./types";
import { useAddName } from "helpers/for-priject";
import { getPhoto } from "helpers/functions";
import { Button } from "components/shared";
import clsx from "clsx";
import { links } from "routes/links";
import { useUser } from "hooks/useUser";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";

export const useColumns = (handleRemove: Function, handleEdit: Function, onAccept: Function, onReject: Function) => {
  const lang = useTranslator("announces", ["publishStatus"]);
  const history = useHistory();
  const classes = useStyles();
  const { addName } = useAddName();
  const currentUser = useUser();

  return [
    { title: lang.no, field: "no", cellStyle: { fontWeight: "bold" } },
    { title: lang.code, field: "code", cellStyle: { fontWeight: "bold" }, hidden: currentUser.roles !== "admin" },
    {
      title: lang.senderName,
      field: "userName",
      render: (rowData: IAddTable) => rowData.userName,
    },
    { title: lang.announceName, field: "salesType", render: (rowData: IAddTable) => addName(rowData as any) },
    {
      title: lang.status,
      field: "publishStatus",
      render: (rowData: IAddTable) => (
        <Chip
          className={clsx(classes[rowData.publishStatus as any], classes.tableBadge)}
          label={lang[rowData.publishStatus as keyof typeof lang]}
        />
      ),
    },
    {
      title: lang.edit,
      field: "action",
      render: (rowData: IAddTable) => (
        <div className={classes.dataTableButtonsContainer}>
          {(rowData.publishStatus === "pending" || rowData.publishStatus === "rejected") && (
            <Button onClick={() => onAccept(rowData.id)} variant="contained" background="green" text="white">
              <CheckIcon />
            </Button>
          )}
          {rowData.publishStatus === "accepted" && (
            <Button onClick={() => onReject(rowData.id)} variant="contained" background="red" text="white">
              <CloseIcon />
            </Button>
          )}
          <Button onClick={() => handleEdit(rowData.id)} variant="contained">
            <EditIcon />
          </Button>
          {currentUser.roles === "admin" && (
            <Button onClick={() => handleRemove(rowData.id)} variant="contained" background="red" text="white">
              <DeleteIcon />
            </Button>
          )}
        </div>
      ),
    },
  ];
};
