import React, { FC, useEffect, useState } from "react";
import { useStyles } from "./announces.style";
import { useTranslator } from "localization";
import { Autocomplete, Button, DataTable, Select, TextField } from "components/shared";
import { SectionHeader } from "components/shared/section-header";
import { Form } from "react-final-form";
import { useSelectData } from "hooks/useSelectData";
import AddsApi from "api/announces.api";
import { TableCell } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import { IAdd } from "./types";
import { getLimitOffset, toastMessage } from "helpers/functions";
import { useColumns } from "./table-data";
import { useHistory } from "react-router";
import { links } from "routes/links";
import { useSwal } from "helpers/swal";

export interface IAnnounces {}

export const Announces: FC<IAnnounces> = () => {
  const lang = useTranslator("main", ["filter", "alerts"]);
  const { AddFilter } = useSelectData();
  const [announces, setAnnounces] = useState<{ totalCount: number; announces: IAdd[] }>({
    totalCount: 0,
    announces: [],
  });
  const [page, setPage] = useState(1);
  const [filter, setFilter] = useState(0);
  const [changed, setChanged] = useState(false);
  const classes = useStyles();
  const history = useHistory();
  const { Alert } = useSwal();
  const limit = 10;

  useEffect(() => {
    AddsApi.getAdds({
      isPremium: Number(filter) === 1 || undefined,
      is360: Number(filter) === 2 || undefined,
      publishStatus: Number(filter) === 3 ? "pending" : undefined,
      ...getLimitOffset(page, limit),
    }).then((data) => setAnnounces(data?.data));
  }, [filter, page, changed]);

  const handleSearch = (data: any) => {
    AddsApi.getAdds({
      no: data?.no,
      limit: limit,
    }).then((data) => setAnnounces(data?.data));
  };

  const renderContent = () => {
    return (
      <Form
        onSubmit={handleSearch}
        initialValues={{ filter: 0 }}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit} className={classes.searchForm}>
            <TextField name="no" placeholder={lang.addNumber} fullWidth />
            <Button for="search" type="submit" variant="contained">
              {lang.search}
            </Button>
          </form>
        )}
      />
    );
  };

  const handlePagination = (currPage: number) => {
    if (currPage !== page) {
      setPage(currPage);
    }
  };

  const handleRemove = (id: string) => {
    Alert.fire({
      text: lang.removeAlert,
      icon: "question",
    }).then((result) => {
      if (result.value) {
        AddsApi.removeAdd(id).then(() => setChanged(!changed));
      }
    });
  };

  const handleEdit = (id: string) => {
    history.push(`${links.announces.edit}/${id}`);
  };

  const handleAccept = (id?: number) => {
    AddsApi.accept(id).then((p) => {
      toastMessage(p, "Elan qəbul edildi !");
      setChanged(!changed);
    });
  };

  const handleReject = (id: number) => {
    AddsApi.reject(id).then((p) => {
      toastMessage(p, "Elan geri çevrildi !", "warn");
      setChanged(!changed);
    });
  };

  return (
    <section>
      <SectionHeader
        filterData={AddFilter}
        renderContent={renderContent}
        onAddClick={() => history.push(links.announces.add)}
        title={lang.announces}
        onFilter={(data) => setFilter(data)}
        addButton
      />
      <div>
        <DataTable
          data={announces?.announces || []}
          //@ts-ignore
          columns={useColumns(handleRemove, handleEdit, handleAccept, handleReject)}
          options={{ toolbar: false, pageSize: limit }}
          components={{
            Pagination: (props: any) => (
              <TableCell className={classes.pagination}>
                <Pagination
                  onChange={(event: object, page: number) => handlePagination(page)}
                  count={Math.ceil(announces?.totalCount / limit)}
                  shape="rounded"
                  page={page}
                />
              </TableCell>
            ),
          }}
        />
      </div>
    </section>
  );
};
