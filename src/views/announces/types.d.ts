export interface ISearchAdd {
  userId?: string;
  isPremium?: boolean;
  is360?: boolean;
  propType?: string;
  salesType?: string;
  buildingType?: string;
  document?: boolean;
  mortgage?: boolean;
  city?: string;
  region?: string;
  settlement?: string;
  metro?: string;
  rooms?: string;
  areaMin?: number;
  areaMax?: number;
  landAreaMin?: number;
  landAreaMax?: number;
  homeFloorMax?: number;
  homeFloorMin?: number;
  priceMax?: number;
  priceMin?: number;
  notFirstFloor?: boolean;
  notLastFloor?: boolean;
  no?: string;
  orderCase?: "date" | "expensive" | "cheap"; // default date
  offset?: number;
  limit?: number;
  publishStatus?: "accepted" | "pending";
}

export interface IAdd {
  about: string;
  address: string;
  agent?: IAgency;
  agentId?: number;
  area: number;
  buildingFloor?: number;
  buildingType?: string;
  city: string;
  document?: boolean;
  email?: string;
  expireDate?: string;
  expired?: boolean;
  homeFloor?: number;
  iamAgent: boolean;
  id: number;
  images: string[];
  insertDate: string;
  is360: boolean;
  isPremium: boolean;
  landArea?: number;
  metro?: string;
  mortgage?: boolean;
  name?: string;
  phone?: string;
  price: number;
  propType: string;
  publishDate: string;
  publishStatus?: string;
  region?: string;
  rooms?: number;
  salesType: string;
  settlement?: string;
  user?: IUser | null;
  userId?: string;
  views: number;
  no: string;
  code: number;
  lat: string;
  lng: string;
}

export interface IAddReqModel {
  //user info
  userId?: string;
  // add info
  propType: string; // yeni tikili , bag evu ..
  salesType: string; // satis ya kiraye
  city: string;
  region: string;
  settlement: string;
  rooms: number;
  area: number;
  homeFloor: number;
  buildingFloor: number;
  about: string;
  price: number;
  document: boolean;
  mortgage: boolean;
  landArea: number | null;
  metro: string;
  buildingType: string; // bina, ev, villa - for office
  address: string;
  is360: boolean;
  images: string[];
  no: string;
  code: string;
  lat: string;
  lng: string;
  link: string;
}

interface IAddTable {
  id: number;
  no: number;
  salesType: string;
  propType: string;
  settlement?: string;
  city: string;
  metro?: string;
  region?: string;
  userName?: string;
  userId?: string;
  publishStatus?: string;
  rooms: number;
  area: number;
}
