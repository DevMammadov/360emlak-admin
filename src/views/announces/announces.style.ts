import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    dataTableButtonsContainer: {
      display: "flex",
      justifyContent: "space-between",
      width: "45%",
      margin: "0 auto",
      "& .MuiButtonBase-root": {
        padding: theme.spacing(0.5),
        marginRight: 5,
        minWidth: "unset",
        "& .MuiSvgIcon-root": {
          fontSize: 25,
        },
      },
    },
    searchForm: {
      display: "flex",
      "& .MuiButtonBase-root": {
        marginLeft: theme.spacing(2),
      },
    },
    tableBadge: {
      color: theme.palette.color.white,
      padding: 0,
      height: 25,
      fontWeight: "bold",
    },
    accepted: {
      backgroundColor: theme.palette.color.green,
    },
    rejected: {
      backgroundColor: theme.palette.color.red,
    },
    pending: {
      backgroundColor: theme.palette.color.orange,
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
