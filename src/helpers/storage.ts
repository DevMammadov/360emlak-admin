import { decode } from "jsonwebtoken";
import { IToken } from "views/login/types";

export interface IStorage {
  token: string;
  lang: string;
}

export const setToStorage = (key: string, value: any) => {
  try {
    let store = localStorage.getItem("__store__");
    if (store) {
      let parsedStore = JSON.parse(store);
      localStorage.setItem("__store__", JSON.stringify({ ...parsedStore, [key]: value }));
    } else {
      let storeObj = { [key]: value };
      localStorage.setItem("__store__", JSON.stringify(storeObj));
    }
  } catch (e) {
    // console.log(e);
  }
};

export const getStorage = () => {
  try {
    let store = localStorage.getItem("__store__");
    if (store) {
      return JSON.parse(store) as IStorage;
    }
    return undefined;
  } catch (e) {
    // console.log(e);
  }
};

export const removeStorage = () => {
  localStorage.removeItem("__store__");
};

export const emptyStorage = () => {
  localStorage.removeItem("__store__");
};

export const decodedToken = () => {
  const store = getStorage();
  if (store && store?.token) {
    const decodedLocal =
      decode(store.token) ||
      ({
        email: "",
        aud: "",
        exp: "",
        iss: "",
        jti: "",
        userId: "",
        iamAgent: "False",
      } as IToken);
    return decodedLocal as IToken;
  }
};
