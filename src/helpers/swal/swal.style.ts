import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => {
  return {
    swalTitle: {
      fontSize: 21,
      color: `${theme.palette.color.main} !important`,
    },
    swalContent: {
      fontSize: 17,
      color: `${theme.palette.color.black} !important`,
    },
    swalInput: {
      padding: 8,
      height: 40,
      outline: "none",
      margin: 0,
      marginTop: theme.spacing(2),
      border: `1px solid ${theme.palette.color.main}`,
      "&::focus": {
        outline: 0,
        boxShadow: "none",
      },
    },
    swalConfirmButton: {
      backgroundImage: `${theme.palette.color.main} !important`,
      color: `${theme.palette.color.white} !important`,
      background: "unset",
    },
    swalIcon: {
      color: `${theme.palette.color.main} !important`,
      borderColor: `${theme.palette.color.main} !important`,
    },
    [theme.breakpoints.down("xl")]: {},
    [theme.breakpoints.down("lg")]: {},
    [theme.breakpoints.down("md")]: {},
    [theme.breakpoints.down("sm")]: {},
    [theme.breakpoints.down("xs")]: {},
  };
});
