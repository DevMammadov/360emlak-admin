import { useTranslator } from "localization";
import Swalert from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
//import "@sweetalert2/theme-material-ui/material-ui.scss";
import { useStyles } from "./swal.style";

interface ISwalParams {
  type: "alert" | "prompt";
}

export const useSwal = () => {
  const MySwal = withReactContent(Swalert);
  const lang = useTranslator("main");
  const classes = useStyles();

  const Alert = MySwal.mixin({
    customClass: {
      title: classes.swalTitle,
      content: classes.swalContent,
      input: classes.swalInput,
      confirmButton: classes.swalConfirmButton,
      cancelButton: classes.swalDenyButton,
      icon: classes.swalIcon,
    },
    showCloseButton: true,
    showCancelButton: true,
    confirmButtonText: lang.approve,
    cancelButtonText: lang.deny,
    icon: "error",
  });

  const Prompt = Alert.mixin({
    icon: "question",
    input: "text",
    showLoaderOnConfirm: true,
    showCancelButton: true,
  });

  return { Alert, Prompt };
};
