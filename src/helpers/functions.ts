import { useLocation } from "react-router-dom";
import { toast } from "react-toastify";

export const isEmpty = (obj: any) => {
  if (obj) {
    return Object.keys(obj).length === 0;
  }
  return true;
};

export const serialize = (obj: any) => {
  let str = [];
  for (let p in obj)
    if (obj.hasOwnProperty(p) && obj[p]) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
};

export const equal = (obj1: {}, obj2: {}) => {
  return JSON.stringify(obj1) === JSON.stringify(obj2);
};

export const getLimitOffset = (page: number, lmt: number) => {
  let offset = (page - 1) * lmt;
  let limit = lmt;
  return { offset, limit };
};

export const lngLtd = (lng: string, lat: string) => {
  return lng && lat ? { lng: Number(lng), lat: Number(lat) } : undefined;
};

export const getPhoto = (url: string) => {
  return `${url}`;
};

export const checkValue = (val: any) => {
  if (val === "undefined") {
    return "";
  } else {
    return val;
  }
};

export const toastMessage = (payload: any, message: string, type?: "success" | "error" | "warn" | "info") => {
  if (payload?.statusCode === 200) {
    toast[type || "success"](message);
  }
};

export const toNomber = (val?: number) => (val ? Number(val) : undefined);

export const removeUndefined = (obj: {}) => {
  Object.keys(obj).forEach((key) =>
    obj[key as keyof typeof obj] === undefined || obj[key as keyof typeof obj] === null
      ? delete obj[key as keyof typeof obj]
      : {}
  );
  return obj;
};
