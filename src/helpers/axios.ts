import axiosBase from "axios";
import { toast } from "react-toastify";
import { getStorage, removeStorage } from "./storage";

export const axiosMain = axiosBase.create({
  baseURL: `${process.env.REACT_APP_BASE_URL}/api/v1/`,
});

axiosMain.interceptors.request.use((config) => {
  config.headers["accept-language"] = window.location.pathname.split("/")[1] || "az";
  return config;
});

axiosMain.interceptors.response.use(
  (response) => response,
  (error) => {
    const messages = error.response?.data?.message || error.response?.data?.data;
    if (messages && Array.isArray(messages)) {
      for (let err of messages) {
        toast.error(err);
      }
    }

    if (error.response?.status === 401) {
      removeStorage();
    }
  }
);

export const axiosAdmin = axiosBase.create({
  baseURL: `${process.env.REACT_APP_BASE_URL}/admin/v1/`,
});

axiosAdmin.interceptors.request.use((config) => {
  const storage = getStorage();
  const token = storage?.token;
  if (token) {
    config.headers["Authorization"] = "Bearer " + token;
    config.headers["accept-language"] = window.location.pathname.split("/")[1] || "az";
  }

  return config;
});

axiosAdmin.interceptors.response.use(
  (response) => response,
  (error) => {
    const messages = error.response?.data?.message || error.response?.data?.data;
    if (messages && Array.isArray(messages)) {
      for (let err of messages) {
        toast.error(err);
      }
    }

    if (error.response?.status === 401) {
      removeStorage();
    }
  }
);

export const axiosMatterport = axiosBase.create({
  baseURL: `https://api.matterport.com/api/models/graph`,
});

axiosMatterport.interceptors.request.use((config) => {
  config.headers[
    "Authorization"
  ] = `Basic <base64_encode(${process.env.REACT_APP_MATTERPORT_TOKEN_ID}:${process.env.REACT_APP_MATTERPORT_TOKEN_SECRET})`;
  return config;
});
