import { useUser } from "hooks/useUser";
import { useTranslator } from "localization";
import React, { FC } from "react";
import { useLocation } from "react-router-dom";
import { IAdd } from "views/announces/types";
import dayjs from "dayjs";
import { OverridableComponent } from "@material-ui/core/OverridableComponent";
import { SvgIconTypeMap } from "@material-ui/core";

type IMaterialIcon = OverridableComponent<SvgIconTypeMap<{}, "svg">>;

export const idFromLink = (link: string) => {
  let video_id = link.split("v=")[1] || link;
  let ampersandPosition = video_id.indexOf("&");
  if (ampersandPosition != -1) {
    video_id = video_id.substring(0, ampersandPosition);
  }
  return video_id;
};

export const useQuery = () => {
  return new URLSearchParams(useLocation().search);
};

export const renderIcon = (icon?: IMaterialIcon, className?: string) => {
  const IconElement: IMaterialIcon = icon as IMaterialIcon;
  if (icon) {
    return <IconElement className={className} />;
  } else {
    return null;
  }
};

export const From: FC<{ type?: "dan" | "den" }> = ({ children, type = "dan" }) => {
  const currentUser = useUser();
  const lang = useTranslator();

  if (currentUser.lang === "az") {
    return (
      <>
        {children} - {type === "den" ? "dən" : lang.from}
      </>
    );
  } else {
    return (
      <>
        {lang.from} - {children}
      </>
    );
  }
};

export const round = (number: number) => {
  let result = Math.round((number + Number.EPSILON) * 100) / 100;
  if (isNaN(result)) {
    return 0;
  }
  return result;
};

type NameParams = "salesType" | "rooms" | "propTypes" | "area" | "address";

export const useAddName = () => {
  const langPropTypes = useTranslator("propTypes");
  const langSalesTypes = useTranslator("salesType");
  const langRegion = useTranslator("bakuRegions");
  const langSettle = useTranslator("settlements");
  const langCity = useTranslator("cities");
  const langFilter = useTranslator("filter");
  const lang = useTranslator("weekDays");

  const getDay = (days: number[], times: string[]) => {
    if (days) {
      const day1 = days[0] ? lang[days[0]] : "";
      const day2 = days[1] ? `- ${lang[days[1]]}` : "";
      const time1 = times && times[0] ? dayjs(times[0]).format("HH:MM") : "";
      const time2 = times && times[1] ? `- ${dayjs(times[1]).format("HH:MM")}` : "";
      const time = `${time1} ${time2}`;

      return days.length > 1 ? `${day1} ${day2}: ${time}` : `${lang.everyDay}: ${time}`;
    }
  };

  const getAddress = (add: IAdd) => {
    if (add.region && add.settlement) {
      return `${langSettle[add.region][add.settlement]} ${langFilter.settlements[0].toLowerCase()}`;
    } else if (add.region) {
      return `${langRegion[add.region]} ${langFilter.regions[0].toLowerCase()}`;
    } else if (add.city) {
      return `${langCity[add.city]} ${langFilter.cities[0].toLowerCase()}`;
    } else {
      return "";
    }
  };

  const getArea = (add: IAdd) => `${add.area} ${add.propType === "8" ? langCity.sot : langCity.m2}`;

  const addName = (add: IAdd, exclude?: NameParams[]) => {
    const values: NameParams[] = ["salesType", "rooms", "propTypes", "area", "address"];
    const labels: string[] = [
      langSalesTypes[add.salesType],
      `${add.rooms} ${langPropTypes.withRoom}`,
      langPropTypes[add.propType],
      getArea(add),
      `, ${getAddress(add)}`,
    ];
    const name: string[] = [];
    for (let i in values) {
      if (!exclude?.includes(values[i])) {
        name.push(labels[i]);
      }
    }
    return name.join(" ");
  };

  return { addName, getAddress, getArea, getDay };
};

export const getPhoto = (url: string) => {
  //console.log(axios.get(`${process.env.REACT_APP_BASE_URL}/photos/${url}`));

  return `${url}`;
};
